@extends('user.layouts.master')

@section('contentSec')
<!--=================================
inner banner -->
<div class="header-inner bg-light text-center">
    <div class="container">
      <div class="row">
        <div class="col-12">
        <h2 class="text-primary">Apply to {{ $data->name }}</h2>
          <ol class="breadcrumb mb-0 p-0">
            <li class="breadcrumb-item"><a href="{{ route('home') }}"> Home </a></li>
            <li class="breadcrumb-item active"> <i class="fas fa-chevron-right"></i> <span> Apply </span></li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <!--=================================
  inner banner -->

  <!--=================================
Millions of jobs -->
<section class="my-5">
  <div class="container">
      <div class="row mt-4 text-center">
        {{-- <div class="col-md-6">
          @if (!empty($dataUser))
            @if( pathinfo($dataUser->user_application_document()->first()->document, PATHINFO_EXTENSION) == "pdf" )
              <embed src="{{ asset('assets/user/documents/' . $dataUser->user_application_document()->first()->document) }}" type="application/pdf" width="100%" height="600px" />
              @elseif ( pathinfo($dataUser->user_application_document()->first()->document, PATHINFO_EXTENSION) == "docx" )
              <img src="{{ asset('assets/user/images/file.svg') }}" alt="" width="100%">
              @elseif ( pathinfo($dataUser->user_application_document()->first()->document, PATHINFO_EXTENSION) == "PNG" || pathinfo($dataUser->user_application_document()->first()->document, PATHINFO_EXTENSION) == "png")
              <img src="{{ asset('assets/user/documents/' . $dataUser->user_application_document()->first()->document) }}" alt="" width="100%">
            @endif
            @else
            <img src="{{ asset('assets/user/images/file.svg') }}" alt="" width="100%">
          @endif
        </div> --}}
        <div class="col-md-12">
          @if (session('message'))
          <div class="alert alert-success mt-3">{{session('message')}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
        </div>
        @endif
          @if (count($data->company_vacancy))
          <form action="{{ route('apply', $data->id) }}" method="POST" enctype="multipart/form-data">
            @csrf

            <select name="vacancy" class="form-control" id="vacancy-option">
              <option value="">Select Vacancy</option>
              @foreach ($data->company_vacancy as $item)
                <option value="{{ $item->id }}" data-description="{{ $item->description }}">
                  {{ $item->name }}
                </option>
              @endforeach
            </select>

            <p id="description" class="text-left mt-3"></p>

            <label for="document" @error('document')
                    class="text-danger"
                    @enderror>@error('document')  
                    {{$message}}
                    @enderror</label>
            <input type="file" class="form-control my-3" name="document" id="document">
            <button class="btn btn-primary" type="submit">Submit Your Resume</button>
          </form>
          @else
            <h5 class="text-muted">
              There are no vacancies available, or maybe you have applied for all of these company vacancies
            </h5>
            <h5 class="text-muted">
              Please check your profile
            </h5>
          {{-- @else
          <form action="{{ route('applyDelete', $dataUser->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method("DELETE")
            <input type="file" class="form-control my-3" name="document" disabled>
            <button class="btn btn-primary" type="submit">Delete Resume</button>
          </form> --}}
          @endif
        </div>
      </div>
  </div>
  </section>
@endsection

@push('addJs')
  <script>
    const vacany_option = document.getElementById('vacancy-option');

    vacany_option.addEventListener('change', function () {
      const description = this.options[this.selectedIndex].getAttribute('data-description');
      const elDesc = document.getElementById('description');

      elDesc.innerHTML = description;
    });
  </script>
@endpush