@extends('user.layouts.master')

@section('contentSec')
<!--=================================
inner banner -->
<div class="header-inner bg-light text-center">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2 class="text-primary">FAQ</h2>
          <ol class="breadcrumb mb-0 p-0">
            <li class="breadcrumb-item"><a href="{{ route('home') }}"> Home </a></li>
            <li class="breadcrumb-item active"> <i class="fas fa-chevron-right"></i> <span> FAQ </span></li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <!--=================================
  inner banner -->

  <!--=================================
Millions of jobs -->
<section class="my-5">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="section-title text-center">
            <h2>Frequently Asked Questions</h2>
          </div>
          <div class="accordion-style" id="accordion-Two">
            <div class="card">
              <div class="card-header" id="headingOne">
                <h5 class="accordion-title mb-0">
                <button class="btn btn-link d-flex align-items-center ml-auto" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Apakah ECIF 2021 dilaksanakan secara daring? <i class="fas fa-chevron-down fa-xs"></i></button>
                </h5>
              </div>
              <div id="collapseOne" class="collapse show accordion-content" aria-labelledby="headingOne" data-parent="#accordion-Two">
                <div class="card-body">
                  <p>Karena adanya pandemic COVID-19, ECIF 2021 dilaksanakan secara daring.
                  </p>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="headingTwo">
                <h5 class="accordion-title mb-0">
                <button class="btn btn-link d-flex align-items-center ml-auto collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Bagaimana jika ingin mengikuti ECIF 2021? <i class="fas fa-chevron-down fa-xs"></i></button>
                </h5>
              </div>
              <div id="collapseTwo" class="collapse accordion-content" aria-labelledby="headingTwo" data-parent="#accordion-Two">
                <div class="card-body">
                  <ol class="pl-3 mb-0">
                    <li>Membuat akun di ecif.eng.ui.ac.id. Isi data diri</li>
                    <li>Setelah membuat akun, silakan login.</li>
                    <li>Lihat Jadwal Webinar pada <a href="{{ route('schedule') }}">Schedule</a></li>
                    <li>Kamu bisa melihat Perusahaan atau Lembaga yang ada di ECIF 2021 pada <a href="{{ route('map') }}">Main Hall</a></li>
                    <li>Jika ingin mengikuti webinar klik <a href="{{ route('map') }}">Main Hall</a> -> Pilih Booth Perusahaan yang ingin dikunjungi -> Klik Join Webinar -> Isi Nama, WA, dan Email -> tunggu email dari admin mengenai Link Zoom Meeting </li>
                    <li>Jika masih belum jelas, yukk nonton video tutorial <a href="https://bit.ly/TUTORIALIKUTECIF21" target="_blank">di sini</a> </li>
                  </ol>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="headingthree">
                <h5 class="accordion-title mb-0">
                <button class="btn btn-link d-flex align-items-center ml-auto collapsed" data-toggle="collapse" data-target="#collapsethree" aria-expanded="false" aria-controls="collapsethree">Apakah terbuka untuk umum? <i class="fas fa-chevron-down fa-xs"></i></button>
                </h5>
              </div>
              <div id="collapsethree" class="collapse accordion-content" aria-labelledby="headingthree" data-parent="#accordion-Two">
                <div class="card-body">
                  <p>ECIF 2021 terbuka untuk UMUM (mahasiswa FTUI, NON FTUI di lingkukan kampus UI,
                    dan Universitas / Institut di luar UI)</p>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="headingfour">
                <h5 class="accordion-title mb-0">
                <button class="btn btn-link d-flex align-items-center ml-auto collapsed" data-toggle="collapse" data-target="#collapsefour" aria-expanded="false" aria-controls="collapsefour">Apakah acara ini berbayar atau gratis?  <i class="fas fa-chevron-down fa-xs"></i></button>
                </h5>
              </div>
              <div id="collapsefour" class="collapse accordion-content" aria-labelledby="headingfour" data-parent="#accordion-Two">
                <div class="card-body">
                  <p>ECIF 2021 GRATIS! Yukkkk join ;)</p>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="headingfive">
                <h5 class="accordion-title mb-0">
                <button class="btn btn-link d-flex align-items-center ml-auto collapsed" data-toggle="collapse" data-target="#collapsefive" aria-expanded="false" aria-controls="collapsefive">Bagaimana cara login?  <i class="fas fa-chevron-down fa-xs"></i></button>
                </h5>
              </div>
              <div id="collapsefive" class="collapse accordion-content" aria-labelledby="headingfive" data-parent="#accordion-Two">
                <div class="card-body">
                  <ol class="pl-3 mb-0">
                    <li>Akses website ecif.eng.ui.ac.id</li>
                    <li>Klik menu <a href="{{ route('view.login') }}">Sign In</a></li>
                    <li>Isi username dan password. Lalu klik Sign In.</li>
                  </ol>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="headingsix">
                <h5 class="accordion-title mb-0">
                <button class="btn btn-link d-flex align-items-center ml-auto collapsed" data-toggle="collapse" data-target="#collapsesix" aria-expanded="false" aria-controls="collapsesix">Bagaimana cara saya melihat dan memilih perusahaan yang bergabung dalam ECIF
                  2021? <i class="fas fa-chevron-down fa-xs"></i></button>
                </h5>
              </div>
              <div id="collapsesix" class="collapse accordion-content" aria-labelledby="headingsix" data-parent="#accordion-Two">
                <div class="card-body">
                  <ol class="pl-3 mb-0">
                    <li>Semua dapat dilihat di halaman website ECIF 2021 (ecif.eng.ui.ac.id)</li>
                    <li>Pada homepage pilih <a href="{{ route('map') }}">GO TO MAIN HALL</a></li>
                    <li>Maka akan muncul peta perusahaan</li>
                    <li>Klik "BOOTH" Untuk melihat Company Profile, Career/Internship/Scholarship Opportunities, dan join webinar di ECIF 2021</li>
                  </ol>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="headingseven">
                <h5 class="accordion-title mb-0">
                <button class="btn btn-link d-flex align-items-center ml-auto collapsed" data-toggle="collapse" data-target="#collapseseven" aria-expanded="false" aria-controls="collapseseven">Bagaimana cara melamar salah satu pekerjaan/ internship ? <i class="fas fa-chevron-down fa-xs"></i></button>
                </h5>
              </div>
              <div id="collapseseven" class="collapse accordion-content" aria-labelledby="headingseven" data-parent="#accordion-Two">
                <div class="card-body">
                  <ol class="pl-3 mb-0">
                    <li>Klik <a href="{{ route('map') }}">Main Hall</a>, kemudian pilih salah satu booth perusahaan yang membuka lowongan kerja atau internship</li>
                    <li>Pada booth klik Career</li>
                    <li>Jika diarahkan ke website perusahaan maka kamu apply sesuai dengan arahan website tersebut. Namun jika diarahkan untuk mendaftar di ECIF 2021, maka kamu upload berkas-berkas seperti CV/ Resume, atau Portofolio</li>
                    <li>Jika masih belum jelas, yuukk nonton video tutorial melamar pekerjaan <a href="https://bit.ly/TUTORIALLAMARKERJAECIF21">di sini</a></li>
                  </ol>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="headingeight">
                <h5 class="accordion-title mb-0">
                <button class="btn btn-link d-flex align-items-center ml-auto collapsed" data-toggle="collapse" data-target="#collapseeight" aria-expanded="false" aria-controls="collapseeight">Bagaimana cara menjadi member CDC FTUI? <i class="fas fa-chevron-down fa-xs"></i></button>
                </h5>
              </div>
              <div id="collapseeight" class="collapse accordion-content" aria-labelledby="headingeight" data-parent="#accordion-Two">
                <div class="card-body">
                  <p>Nonton yuuk video tutorial pendaftaran keanggotan CDC FTUI disini:
                  </p>
                  <ol class="pl-3 mb-0">
                    <li>Tanpa kode voucher : <a href="https://bit.ly/REGISTRASICDCFTUI">https://bit.ly/REGISTRASICDCFTUI</a></li>
                    <li>Dengan kode voucher: <a href="https://bit.ly/REGISTRASIKODECDCFTUI">https://bit.ly/REGISTRASIKODECDCFTUI</a></li>
                  </ol>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="headingnine">
                <h5 class="accordion-title mb-0">
                <button class="btn btn-link d-flex align-items-center ml-auto collapsed" data-toggle="collapse" data-target="#collapsenine" aria-expanded="false" aria-controls="collapsenine">Apakah saya bisa memilih lebih dari satu perusahaan untuk dilamar? <i class="fas fa-chevron-down fa-xs"></i></button>
                </h5>
              </div>
              <div id="collapsenine" class="collapse accordion-content" aria-labelledby="headingnine" data-parent="#accordion-Two">
                <div class="card-body">
                  <p>Bisa dong, silakan melamar ke perusahaan yang sesuai dengan minat dan kompetisi dirimu yaa. Perhatikan requirements yang telah ditentukan!</p>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="headingten">
                <h5 class="accordion-title mb-0">
                <button class="btn btn-link d-flex align-items-center ml-auto collapsed" data-toggle="collapse" data-target="#collapseten" aria-expanded="false" aria-controls="collapseten">Apakah dapat mengakses website ecif.eng.ui.ac.id dengan smartphone? <i class="fas fa-chevron-down fa-xs"></i></button>
                </h5>
              </div>
              <div id="collapseten" class="collapse accordion-content" aria-labelledby="headingten" data-parent="#accordion-Two">
                <div class="card-body">
                  <p>Untuk mengakses website ECIF disarankan melalui PC/ Laptop untuk tampilan yang
                    optimal dan jangan lupa device mu harus tersambung internet dengan jaringan yang
                    stabil yaa
                  </p>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="headingeleven">
                <h5 class="accordion-title mb-0">
                <button class="btn btn-link d-flex align-items-center ml-auto collapsed" data-toggle="collapse" data-target="#collapseeleven" aria-expanded="false" aria-controls="collapseeleven">Apa saja yang harus dipersiapkan untuk mengikuti ECIF 2021? <i class="fas fa-chevron-down fa-xs"></i></button>
                </h5>
              </div>
              <div id="collapseeleven" class="collapse accordion-content" aria-labelledby="headingeleven" data-parent="#accordion-Two">
                <div class="card-body">
                  <ol class="pl-3 mb-0">
                    <li>Pastikan jaringan internet stabil</li>
                    <li>Siapkan dirimu dengan baik untuk berinteraksi dengan perusahaan atau
                      pekerjaan impian mu</li>
                    <li>Siapkan CV/ Resume/ Portofolio untuk diserahkan ke perusahaan;)</li>
                    <li>GOODLUCK!</li>
                  </ol>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="headingthirteen">
                <h5 class="accordion-title mb-0">
                <button class="btn btn-link d-flex align-items-center ml-auto collapsed" data-toggle="collapse" data-target="#collapsethirteen" aria-expanded="false" aria-controls="collapsethirteen">Apakah saya bisa berkomunikasi langsung dengan perusahaan? <i class="fas fa-chevron-down fa-xs"></i></button>
                </h5>
              </div>
              <div id="collapsethirteen" class="collapse accordion-content" aria-labelledby="headingthirteen" data-parent="#accordion-Two">
                <div class="card-body">
                  <p>Bisa. Kamu bisa berkomunikasi dan berdiskusi langsung dengan perusahaan saat sesi Q&A jika kamu mengikuti sesi webinar melalui zoom. </p>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="headingthirteen1">
                <h5 class="accordion-title mb-0">
                <button class="btn btn-link d-flex align-items-center ml-auto collapsed" data-toggle="collapse" data-target="#collapsethirteen1" aria-expanded="false" aria-controls="collapsethirteen1">Apakah ada sesi interview secara online di acara ECIF? <i class="fas fa-chevron-down fa-xs"></i></button>
                </h5>
              </div>
              <div id="collapsethirteen1" class="collapse accordion-content" aria-labelledby="headingthirteen1" data-parent="#accordion-Two">
                <div class="card-body">
                  <p>Informasi mengenai sesi interview akan dilaksanakan sesuai perusahaan masing-masing yaa</p>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="headingthirteen2">
                <h5 class="accordion-title mb-0">
                <button class="btn btn-link d-flex align-items-center ml-auto collapsed" data-toggle="collapse" data-target="#collapsethirteen2" aria-expanded="false" aria-controls="collapsethirteen2">Apakah sesi webinar dan persentasi perusahaan dilaksanakan dalam waktu yg bersamaan? <i class="fas fa-chevron-down fa-xs"></i></button>
                </h5>
              </div>
              <div id="collapsethirteen2" class="collapse accordion-content" aria-labelledby="headingthirteen2" data-parent="#accordion-Two">
                <div class="card-body">
                  <p>Haii, di sesi webinar akan membahas mengenai profil perusahaan atau lembaga beasiswa, bagaimana cara bergabung dan mengikuti proses rekrutmen di perusahaan lembaga beasiswa. Penasaran dan tertarik kaann?? YUK JOIN WEBINAR SEKARANG JUGAA;)</p>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="headingfourteen">
                <h5 class="accordion-title mb-0">
                <button class="btn btn-link d-flex align-items-center ml-auto collapsed" data-toggle="collapse" data-target="#collapsefourteen" aria-expanded="false" aria-controls="collapsefourteen">Bagaimana jika koneksi saya hilang saat sedang mengikuti job fair? <i class="fas fa-chevron-down fa-xs"></i></button>
                </h5>
              </div>
              <div id="collapsefourteen" class="collapse accordion-content" aria-labelledby="headingfourteen" data-parent="#accordion-Two">
                <div class="card-body">
                  <p>Silakan login kembali</p>
                </div>
              </div>
            </div>
            <div class="card">
              <div class="card-header" id="headingfiveteen">
                <h5 class="accordion-title mb-0">
                <button class="btn btn-link d-flex align-items-center ml-auto collapsed" data-toggle="collapse" data-target="#collapsefiveteen" aria-expanded="false" aria-controls="collapsefiveteen">Bagaimana jika saya ingin menghubungi CDC FTUI? <i class="fas fa-chevron-down fa-xs"></i></button>
                </h5>
              </div>
              <div id="collapsefiveteen" class="collapse accordion-content" aria-labelledby="headingfiveteen" data-parent="#accordion-Two">
                <div class="card-body">
                  <p>Haii kamu dapat menghubungi via DM ke Instagram: cdc_ftui atau </p>
                  <ol class="pl-3 mb-0">
                    <li>No. Telepon: (021) 786 3506</li>
                    <li>No. HP: 085776523987</li>
                  </ol>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection

@push('page-script')
    
@endpush