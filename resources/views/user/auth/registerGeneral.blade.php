@extends('user.layouts.master')

@push('page-style')
    {{-- <link rel="stylesheet" href="{{ asset('assets/user/css/select2/select2.css') }}" /> --}}
@endpush

@section('contentSec')
<!--=================================
inner banner -->
<div class="header-inner bg-light text-center">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="text-primary">Register Account</h2>
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"> Home </a></li>
                    <li class="breadcrumb-item active"> <i class="fas fa-chevron-right"></i> <span> Register </span>
                    </li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!--=================================
inner banner -->

<!--=================================
Millions of jobs -->
<section class=space-ptb>
    <div class=container>
        <div class="row justify-content-center">
            <div class="col-xl-8 col-lg-10 col-md-12">
                <div class=login-register>
                    <fieldset>
                        <ul class="nav nav-tabs nav-tabs-border d-flex" role=tablist>
                            <li class="nav-item mr-4">
                                <a class="nav-link" href="{{ route('view.register') }}">
                                    <div class=d-flex>
                                        <div class=tab-icon>
                                            <i class=flaticon-users></i>
                                        </div>
                                        <div class=ml-3>
                                            <h6 class=mb-0>FT UI</h6>
                                            <p class=mb-0>Mahasiswa dan Alumni FTUI</p>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="nav-item ml-auto">
                                <a class="nav-link active" href="{{ route('view.register.general') }}">
                                    <div class=d-flex>
                                        <div class=tab-icon>
                                            <i class=flaticon-suitcase></i>
                                        </div>
                                        <div class=ml-3>
                                            <h6 class=mb-0>Non FT UI</h6>
                                            <p class=mb-0>Umum</p>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </fieldset>
                    <div class=tab-content>
                        <div class="tab-pane active" id=non-ftui role=tabpanel>
                            <form id="form-register" action={{ route('register', ['type' => 'non']) }} class=mt-4 method=POST enctype="multipart/form-data">
                                
                                @if (Session::has('success'))
                                    <div class="alert alert-success text-center" role="alert">
                                        {{ Session::get('success') }}
                                    </div>
                                @endif

                                @if (Session::has('error'))
                                    <div class="alert alert-danger text-center" role="alert">
                                        {{ Session::get('error') }}
                                    </div>
                                @endif

                                @if ($errors->any())
                                    <div class="alert alert-danger text-center" role="alert">
                                        {{ $errors->first() }}
                                    </div>
                                @endif

                                @csrf

                                <div class=form-row>
                                    <div class="form-group col-12">
                                        <label for=full-name>Full Name*</label>
                                        <input type=text class=form-control id=full-name name=full_name value="{{ old('full_name') }}">
                                    </div>
                                    <div class="form-group col-12">
                                        <label for=address>Address*</label>
                                        <input type=text class=form-control id=address name=address value="{{ old('address') }}">
                                    </div>
                                    <div class="form-group col-6">
                                        <label for=email>Email* </label>
                                        <input type=email class=form-control id=email name=email value="{{ old('email') }}">
                                    </div>
                                    {{-- <div class="form-group col-3">
                                        <label>&nbsp;</label>
                                        <button id="btn-verify" class="btn btn-primary btn-block" type=button style="color:white;">Send OTP</button>
                                    </div>
                                    <div class="form-group col-4">
                                        <label for=otp>OTP* </label>
                                        <input type=otp class=form-control id=otp name=otp value="{{ old('otp') }}">
                                    </div> --}}
                                    {{-- <div class="form-group col-4">
                                        <label for=studentNumber>Student ID Number* </label>
                                        <input type=number class=form-control id=studentNumber name=student_number value="{{ old('student_number') }}">
                                    </div> --}}
                                    <div class="form-group col-md-6 select-border">
                                        <label>Gender *</label>
                                        <select class="form-control basic-select" name="gender">
                                            <option selected="selected">Select Gender</option>
                                            <option value="M" @if(old('gender') === 'M') selected @endif>Male</option>
                                            <option value="F" @if(old('gender') === 'F') selected @endif>Female</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-6">
                                        <label for=birth>Birth* </label>
                                        <input type=date class=form-control id=birth name=birth value="{{ old('birth') }}">
                                    </div>
                                    <div class="form-group col-6">
                                        <label for=phoneNumber>Phone Number* </label>
                                        <input type=text class=form-control id=phoneNumber name=phone_number value="{{ old('phone_number') }}">
                                    </div>
                                    <div class="form-group col-6">
                                        <label for=graduateYear>Graduate Year* </label>
                                        <input type=string class=form-control id=graduateYear name=graduate_year value="{{ old('graduate_year') }}">
                                    </div>
                                    <div class="form-group col-6 select-border">
                                        <label for=studyLevel>Study Level* </label>
                                        <select class="form-control basic-select" name="study_level">
                                            <option value="" selected>Select Study Level</option>
                                            <option
                                                value="Program Diploma"
                                                @if (old('study_level') === "Program Diploma")
                                                    selected
                                                @endif
                                            >
                                                Program Diploma
                                            </option>
                                            <option
                                                value="Program Sarjana"
                                                @if (old('study_level') === "Program Sarjana")
                                                    selected
                                                @endif
                                            >
                                                Program Sarjana
                                            </option>
                                            <option
                                                value="Program Profesi"
                                                @if (old('study_level') === "Program Profesi")
                                                    selected
                                                @endif
                                            >
                                                Program Profesi
                                            </option>
                                            <option
                                                value="Program Magister"
                                                @if (old('study_level') === "Program Magister")
                                                    selected
                                                @endif
                                            >
                                                Program Magister
                                            </option>
                                            <option
                                                value="Program Doktor"
                                                @if (old('study_level') === "Program Doktor")
                                                    selected
                                                @endif
                                            >
                                                Program Doktor
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group col-4">
                                        <label for=nameUniversity>University* </label>
                                        <input type=string class=form-control id=nameUniversity name=university value="{{ old('university') }}">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for=nameUniversity>Faculty* </label>
                                        <input type=string class=form-control id=faculty name=faculty value="{{ old('faculty') }}">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for=nameUniversity>Study Program* </label>
                                        <input type=string class=form-control id="faculty_type" name="faculty_type" value="{{ old('faculty_type') }}">
                                    </div>
                                    {{-- <div class="form-group col-md-4 select-border">
                                        <label>Faculty *</label>
                                        @if(old('faculty') === "Fakultas Ekonomi & Bisnis")
                                            {{ old('faculty') }}
                                        @endif
                                        <select class="form-control basic-select" name="faculty">
                                            <option value="" selected>Select Faculty</option>
                                            <option
                                                @if(old('faculty') === "Fakultas Kedokteran")
                                                    selected
                                                @endif
                                                value="Fakultas Kedokteran"
                                            >
                                                Fakultas Kedokteran
                                            </option>
                                            <option
                                                @if(old('faculty') === "Fakultas Kedokteran Gigi")
                                                    selected
                                                @endif
                                                value="Fakultas Kedokteran Gigi"
                                            >
                                                Fakultas Kedokteran Gigi
                                            </option>
                                            <option
                                                @if(old('faculty') === "Fakultas Matematika & IPAMIPA")
                                                    selected
                                                @endif
                                                value="Fakultas MIPA"
                                            >
                                                Fakultas Matematika & IPA
                                            </option>
                                            <option
                                                @if(old('faculty') === "Fakultas Hukum")
                                                    selected
                                                @endif
                                                value="Fakultas Hukum"
                                            >
                                                Fakultas Hukum
                                            </option>
                                            <option
                                                @if(old('faculty') === "Fakultas Ekonomi & Bisnis")
                                                    selected
                                                @endif
                                                value="Fakultas Ekonomi & Bisnis"
                                            >
                                                Fakultas Ekonomi & Bisnis
                                            </option>
                                            <option
                                                @if(old('faculty') === "Fakultas Ilmu Pengetahuan dan Budaya")
                                                    selected
                                                @endif
                                                value="Fakultas Ilmu Pengetahuan dan Budaya"
                                            >
                                                Fakultas Ilmu Pengetahuan dan Budaya
                                            </option>
                                            <option
                                                @if(old('faculty') === "Fakultas Psikologi")
                                                    selected
                                                @endif
                                                value="Fakultas Psikologi"
                                            >
                                                Fakultas Psikologi
                                            </option>
                                            <option
                                                @if(old('faculty') === "Fakultas Ilmu Sosial dan Ilmu Politik")
                                                    selected
                                                @endif
                                                value="Fakultas Ilmu Sosial dan Ilmu Politik"
                                            >
                                                Fakultas Ilmu Sosial dan Ilmu Politik
                                            </option>
                                            <option
                                                @if(old('faculty') === "Fakultas Kesehatan Masyarakat")
                                                    selected
                                                @endif
                                                value="Fakultas Kesehatan Masyarakat"
                                            >
                                                Fakultas Kesehatan Masyarakat
                                            </option>
                                            <option
                                                @if(old('faculty') === "Fakultas Ilmu Komputer")
                                                    selected
                                                @endif
                                                value="Fakultas Ilmu Komputer"
                                            >
                                                Fakultas Ilmu Komputer
                                            </option>
                                            <option
                                                @if(old('faculty') === "Fakultas Ilmu Keperawatan")
                                                    selected
                                                @endif
                                                value="Fakultas Ilmu Keperawatan"
                                            >
                                                Fakultas Ilmu Keperawatan
                                            </option>
                                            <option
                                                @if(old('faculty') === "Fakultas Farmasi")
                                                    selected
                                                @endif
                                                value="Fakultas Farmasi"
                                            >
                                                Fakultas Farmasi
                                            </option>
                                            <option
                                                @if(old('faculty') === "Fakultas Ilmu Administrasi")
                                                    selected
                                                @endif
                                                value="Fakultas Ilmu Administrasi"
                                            >
                                                Fakultas Ilmu Administrasi
                                            </option>
                                            <option
                                                @if(old('faculty') === "Fakultas Ilmu Lingkungan")
                                                    selected
                                                @endif
                                                value="Fakultas Ilmu Lingkungan"
                                            >
                                                Fakultas Ilmu Lingkungan
                                            </option>
                                            <option
                                                @if(old('faculty') === "Fakultas Kajian Stratejik & Global")
                                                    selected
                                                @endif
                                                value="Fakultas Kajian Stratejik & Global"
                                            >
                                                Fakultas Kajian Stratejik & Global
                                            </option>
                                            <option
                                                @if(old('faculty') === "Program Vokasi")
                                                    selected
                                                @endif
                                                value="Program Vokasi"
                                            >
                                                Program Vokasi
                                            </option>
                                        </select>
                                    </div> --}}
                                    {{-- <div class="form-group col-md-4 select-border">
                                        <label>Faculty Type *</label>
                                        <select class="form-control basic-select" name="faculty_type">
                                            <option value="" selected>Select Faculty Type</option>
                                            <option value="FT" @if(old('faculty_type') === 'FT') selected @endif>FT</option>
                                            <option value="NON" @if(old('faculty_type') === 'NON') selected @endif>NON</option>
                                        </select>
                                    </div> --}}
                                    <div class="form-group col-12">
                                        <label for=password>Password*</label>
                                        <input type=password class=form-control id=password name=password>
                                    </div>

                                    <div class="form-group col-md-12 my-4">
                                        <button class="btn btn-primary btn-block" type=submit style="color:white;">Register</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@push('addJs')
<script>
    const formRegister = document.getElementById('form-register');
    const urlVerify = "{{ route('verify.email') }}";
    const btnVerify = document.getElementById('btn-verify');

    btnVerify.addEventListener('click', function (e) {
        e.preventDefault();

        formRegister.setAttribute('action', urlVerify);
        
        formRegister.submit();
    });
</script>
@endpush