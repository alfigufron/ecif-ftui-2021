@extends('user.layouts.master')

@section('contentSec')
<!--=================================
inner banner -->
<div class="header-inner bg-light text-center">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="text-primary">Forgot Password</h2>
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"> Home </a></li>
                    <li class="breadcrumb-item active"> <i class="fas fa-chevron-right"></i> <span> Forgot Password </span></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!--=================================
  inner banner -->

<!--=================================
Millions of jobs -->
<section class="mt-5">
    <div class=container>
        <div class="row justify-content-center">
            <div class="col-xl-8 col-lg-10 col-md-12">
                <div class=login-register>
    
                    <div class="tab-pane active" id=candidate role=tabpanel>
                        <form id="form-forgot-password" method=POST action={{ route('forgot.password.verification') }} class=mt-4>

                            @if (Session::has('success'))
                                <div class="alert alert-success text-center" role="alert">
                                    {{ Session::get('success') }}
                                </div>
                            @endif

                            @if (Session::has('error'))
                                <div class="alert alert-danger text-center" role="alert">
                                    {{ Session::get('error') }}
                                </div>
                            @endif

                            @if ($errors->any())
                                <div class="alert alert-danger text-center" role="alert">
                                    {{ $errors->first() }}
                                </div>
                            @endif

                            @csrf
                            <div class=form-row>
                                <div class="form-group col-8">
                                    <label for=Email>Email Address</label>
                                    <input type=email class=form-control id=Email name=email value="{{ old('email') }}">
                                </div>
                                <div class="form-group col-4">
                                  <label>&nbsp;</label>
                                  <button id="btn-otp" class="btn btn-primary btn-block" type=button style="color:white;">Send OTP</button>
                                </div>
                                <div class="form-group col-12">
                                    <label for=otp>OTP</label>
                                    <input type=text class=form-control id=otp name=otp>
                                </div>
                            </div>
                            <div class=form-row>
                                <div class=col-md-12>
                                    <button class="btn btn-primary btn-block" type=submit style="color:white;">Verification</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('addJs')
<script>
  const formForgotPassword = document.getElementById('form-forgot-password');
  const urlSendOtp = "{{ route('forgot.password.send.otp') }}";
  const btnOtp = document.getElementById('btn-otp');

  btnOtp.addEventListener('click', function (e) {
    e.preventDefault();

    formForgotPassword.setAttribute('action', urlSendOtp);
    formForgotPassword.submit();
  });
</script>
@endpush