@extends('user.layouts.master')

@section('contentSec')
<!--=================================
inner banner -->
<div class="header-inner bg-light text-center">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="text-primary">Login</h2>
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"> Home </a></li>
                    <li class="breadcrumb-item active"> <i class="fas fa-chevron-right"></i> <span> Login </span></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!--=================================
  inner banner -->

<!--=================================
Millions of jobs -->
<section class="mt-5">
    <div class=container>
        <div class="row justify-content-center">
            <div class="col-xl-8 col-lg-10 col-md-12">
                <div class=login-register>
    
                    <div class="tab-pane active" id=candidate role=tabpanel>
                        <form method=POST action={{ route('login') }} class=mt-4>

                            @if (Session::has('error'))
                                <div class="alert alert-danger text-center" role="alert">
                                    {{ Session::get('error') }}
                                </div>
                            @endif

                            @if ($errors->any())
                                <div class="alert alert-danger text-center" role="alert">
                                    {{ $errors->first() }}
                                </div>
                            @endif

                            @csrf
                            <div class=form-row>
                                <div class="form-group col-12">
                                    <label for=Email2>Email Address</label>
                                    <input type=email class=form-control id=Email22 name=email value="{{ old('email') }}">
                                </div>
                                <div class="form-group col-12">
                                    <label for=password2>Password</label>
                                    <input type=password class=form-control id=password32 name=password>
                                </div>
                            </div>
                            <div class=form-row>
                                <div class=col-md-6>
                                    <button class="btn btn-primary btn-block" type=submit style="color:white;">Sign
                                        In</button>
                                </div>
                                <div class=col-md-6>
                                    <div class="ml-md-3 mt-3 mt-md-0 forgot-pass">
                                        {{-- <a href="{{ route('view.forgot.password') }}">Forgot Password</a> --}}
                                        <a href="https://mail.google.com/mail/?view=cm&fs=1&to=cdc.ft@ui.ac.id" target="_blank">Forgot Password</a>
                                        <p class=mt-1>Don't have an account? <a href="{{ route('view.register') }}">Register</a></p>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('page-script')

@endpush