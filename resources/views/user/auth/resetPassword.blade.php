@extends('user.layouts.master')

@section('contentSec')
<!--=================================
inner banner -->
<div class="header-inner bg-light text-center">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="text-primary">Reset Passwor</h2>
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"> Home </a></li>
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"> Forgot Password </a></li>
                    <li class="breadcrumb-item active"> <i class="fas fa-chevron-right"></i> <span> Reset Password </span></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!--=================================
  inner banner -->

<!--=================================
Millions of jobs -->
<section class="mt-5">
    <div class=container>
        <div class="row justify-content-center">
            <div class="col-xl-8 col-lg-10 col-md-12">
                <div class=login-register>
    
                    <div class="tab-pane active" id=candidate role=tabpanel>
                        <form id="form-forgot-password" method=POST action={{ route('forgot.password.reset') }} class=mt-4>

                            @if (Session::has('error'))
                                <div class="alert alert-danger text-center" role="alert">
                                    {{ Session::get('error') }}
                                </div>
                            @endif

                            @if ($errors->any())
                                <div class="alert alert-danger text-center" role="alert">
                                    {{ $errors->first() }}
                                </div>
                            @endif

                            @csrf
                            <div class=form-row>
                                <input type="hidden" name="email" value="{{ $email }}">

                                <div class="form-group col-12">
                                    <label for=password>Password</label>
                                    <input type=password class=form-control id=password name=password value="{{ old('password') }}">
                                </div>
                                <div class="form-group col-12">
                                    <label for=password_confirmation>Confirm Password</label>
                                    <input type=password class=form-control id=password_confirmation name=password_confirmation value="{{ old('password_confirmation') }}">
                                </div>
                            </div>
                            <div class=form-row>
                                <div class=col-md-12>
                                    <button class="btn btn-primary btn-block" type=submit style="color:white;">Reset Password</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection