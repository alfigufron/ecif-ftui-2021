@extends('user.layouts.master')

@push('page-style')
    {{-- <link rel="stylesheet" href="{{ asset('assets/user/css/select2/select2.css') }}" /> --}}
@endpush

@section('contentSec')
<!--=================================
inner banner -->
<div class="header-inner bg-light text-center">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="text-primary">Register Account</h2>
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"> Home </a></li>
                    <li class="breadcrumb-item active"> <i class="fas fa-chevron-right"></i> <span> Register </span>
                    </li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!--=================================
inner banner -->

<!--=================================
Millions of jobs -->
<section class=space-ptb>
    <div class=container>
        <div class="row justify-content-center">
            <div class="col-xl-8 col-lg-10 col-md-12">
                <div class=login-register>
                    <fieldset>
                        <ul class="nav nav-tabs nav-tabs-border d-flex" role=tablist>
                            <li class="nav-item mr-4">
                                <a class="nav-link active" href="{{ route('view.register') }}">
                                    <div class=d-flex>
                                        <div class=tab-icon>
                                            <i class=flaticon-users></i>
                                        </div>
                                        <div class=ml-3>
                                            <h6 class=mb-0>FT UI</h6>
                                            <p class=mb-0>Mahasiswa dan Alumni FTUI</p>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="nav-item ml-auto">
                                <a class="nav-link" href="{{ route('view.register.general') }}">
                                    <div class=d-flex>
                                        <div class=tab-icon>
                                            <i class=flaticon-suitcase></i>
                                        </div>
                                        <div class=ml-3>
                                            <h6 class=mb-0>Non FT UI</h6>
                                            <p class=mb-0>Umum</p>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </fieldset>
                    <div class=tab-content>
                        <div class="tab-pane active" id=ftui role=tabpanel>
                            <form id="form-register" action={{ route('register', ['type' => 'ftui']) }} class=mt-4 method=POST enctype="multipart/form-data">

                                @if (Session::has('success'))
                                    <div class="alert alert-success text-center" role="alert">
                                        {{ Session::get('success') }}
                                    </div>
                                @endif

                                @if (Session::has('error'))
                                    <div class="alert alert-danger text-center" role="alert">
                                        {{ Session::get('error') }}
                                    </div>
                                @endif

                                @if ($errors->any())
                                    <div class="alert alert-danger text-center" role="alert">
                                        {{ $errors->first() }}
                                    </div>
                                @endif

                                @csrf
                                <div class=form-row>
                                    <div class="form-group col-12">
                                        <label for=full-name>Full Name*</label>
                                        <input type=text class=form-control id=full-name name=full_name value="{{ old('full_name') }}">
                                    </div>
                                    <div class="form-group col-12">
                                        <label for=address>Address*</label>
                                        <input type=text class=form-control id=address name=address value="{{ old('address') }}">
                                    </div>
                                    <div class="form-group col-6">
                                        <label for=email>Email* </label>
                                        <input type=email class=form-control id=email name=email value="{{ old('email') }}">
                                    </div>
                                    {{-- <div class="form-group col-3">
                                        <label>&nbsp;</label>
                                        <button id="btn-verify" class="btn btn-primary btn-block" type=button style="color:white;">Send OTP</button>
                                    </div>
                                    <div class="form-group col-4">
                                        <label for=otp>OTP* </label>
                                        <input type=otp class=form-control id=otp name=otp value="{{ old('otp') }}">
                                    </div> --}}
                                    <div class="form-group col-6">
                                        <label for=studentNumber>Student ID Number* </label>
                                        <input type=text class=form-control id=studentNumber name=student_number value="{{ old('student_number') }}">
                                    </div>
                                    <div class="form-group col-md-6 select-border">
                                        <label>Gender *</label>
                                        <select class="form-control basic-select" name="gender">
                                            <option selected="selected">Select Gender</option>
                                            <option value="M" @if(old('gender') === 'M') selected @endif>Male</option>
                                            <option value="F" @if(old('gender') === 'F') selected @endif>Female</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-6">
                                        <label for=birth>Birth* </label>
                                        <input type=date class=form-control id=birth name=birth value="{{ old('birth') }}">
                                    </div>
                                    <div class="form-group col-12">
                                        <label for=phoneNumber>Phone Number* </label>
                                        <input type=text class=form-control id=phoneNumber name=phone_number value="{{ old('phone_number') }}">
                                    </div>
                                    <div class="form-group col-6">
                                        <label for=graduateYear>Graduate Year* </label>
                                        <input type=string class=form-control id=graduateYear name=graduate_year value="{{ old('graduate_year') }}">
                                        <p>Note : If you haven't graduated, you can enter semester</p>
                                    </div>
                                    <div class="form-group col-6 select-border">
                                        <label for=studyLevel>Study Level* </label>
                                        <select class="form-control basic-select" name="study_level">
                                            <option value="" selected>Select Study Level</option>
                                            <option
                                                value="Program Sarjana"
                                                @if (old('study_level') === "Program Sarjana")
                                                    selected
                                                @endif
                                            >
                                                Program Sarjana
                                            </option>
                                            <option
                                                value="Program Profesi"
                                                @if (old('study_level') === "Program Profesi")
                                                    selected
                                                @endif
                                            >
                                                Program Profesi
                                            </optionvalue=>
                                            <option
                                                value="Program Magister"
                                                @if (old('study_level') === "Program Magister")
                                                    selected
                                                @endif
                                            >
                                                Program Magister
                                            </option>
                                            <option
                                                value="Program Doktor"
                                                @if (old('study_level') === "Program Doktor")
                                                    selected
                                                @endif
                                            >
                                                Program Doktor
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-12 select-border">
                                        <label>Departement *</label>
                                        <select class="form-control basic-select" name="departement">
                                            <option value="" selected>Select Departement</option>
                                            <option 
                                                value="Departemen Teknik Mesin"
                                                @if (old('departement') === 'Departemen Teknik Mesin')
                                                    selected
                                                @endif
                                            >
                                                Departemen Teknik Mesin
                                            </option>
                                            <option 
                                                value="Departemen Teknik Sipil"
                                                @if (old('departement') === 'Departemen Teknik Sipil')
                                                    selected
                                                @endif
                                            >
                                                Departemen Teknik Sipil
                                            </option>
                                            <option 
                                                value="Departemen Teknik Elektro"
                                                @if (old('departement') === 'Departemen Teknik Elektro')
                                                    selected
                                                @endif
                                            >
                                                Departemen Teknik Elektro
                                            </option>
                                            <option 
                                                value="Departemen Teknik Material"
                                                @if (old('departement') === 'Departemen Teknik Metalurgi dan Material')
                                                    selected
                                                @endif
                                            >
                                                Departemen Teknik Metalurgi 
                                                dan Material
                                            </option>
                                            <option 
                                                value="Departemen Teknik Kimia"
                                                @if (old('departement') === 'Departemen Teknik Kimia')
                                                    selected
                                                @endif
                                            >
                                                Departemen Teknik Kimia
                                            </option>
                                            <option 
                                                value="Departemen Teknik Industri"
                                                @if (old('departement') === 'Departemen Teknik Industri')
                                                    selected
                                                @endif
                                            >
                                                Departemen Teknik Industri
                                            </option>
                                            <option 
                                                value="Departemen Teknik Arsitektur"
                                                @if (old('departement') === 'Departemen Teknik Arsitektur')
                                                    selected
                                                @endif
                                            >
                                                Departemen Teknik Arsitektur
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group col-12">
                                        <label for=password>Password*</label>
                                        <input type=password class=form-control id=password name=password>
                                    </div>

                                    <div class="form-group col-md-12 my-4">
                                        <button class="btn btn-primary btn-block" type=submit style="color:white;">Register</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@push('addJs')
<script>
    const formRegister = document.getElementById('form-register');
    const urlVerify = "{{ route('verify.email') }}";
    const btnVerify = document.getElementById('btn-verify');

    btnVerify.addEventListener('click', function (e) {
        e.preventDefault();

        formRegister.setAttribute('action', urlVerify);
        formRegister.submit();
    });
</script>
@endpush