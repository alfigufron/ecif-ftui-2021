<header class="header bg-dark" style="padding: 0 !important;">
    <nav class="navbar navbar-static-top navbar-expand-lg">
      <div class="container-fluid">
        <button id="nav-icon4" type="button" class="navbar-toggler" data-toggle="collapse" data-target=".navbar-collapse">
            <span></span>
            <span></span>
            <span></span>
        </button>
        <a class="navbar-brand" href="{{ route('home') }}">
            <img class="img-fluid" src="{{ asset('assets/user/images/ecif.png') }}" alt="logo" style="width: 100x; height: 50px">
        </a>
        <div class="navbar-collapse collapse justify-content-center">
          <ul class="nav navbar-nav">
            <li class="nav-item {{ '/' == request()->path() ? 'active' : '' }}">
              <a class="nav-link" href="{{ route('home') }}">Home</a>
            </li>
            <li class="nav-item {{ Request::is('about') ? 'active' : '' }}">
              <a href="{{ route('about') }}" class="nav-link">About</a>
            </li>
            @if (!Auth::check())
              <li class="nav-item {{ !empty($menu) && $menu === 'register' ? 'active' : '' }}">
                  <a href="{{ route('view.register') }}" class="nav-link">Register</a>
              </li>
            @endif
            <li class="nav-item {{ Request::is('schedule') ? 'active' : '' }}">
              <a href="{{ route('schedule') }}" class="nav-link">Schedule</a>
            </li>
            <li class="nav-item {{ Request::is('map') ? 'active' : '' }}">
              <a href="{{ route('map') }}" class="nav-link">Main Hall</a>
            </li>
            <li class="nav-item {{ Request::is('faq') ? 'active' : '' }}">
              <a href="{{ route('faq') }}" class="nav-link">FAQ</a>
            </li>
            <li class="nav-item {{ Request::is('contact') ? 'active' : '' }}">
              <a href="{{ route('contact') }}" class="nav-link">Contact</a>
            </li>
          </ul>
          <div class="add-listing d-flex align-items-center justify-content-center" style="transform: translateX(100px)">
            @if (Auth::guard('web')->check())
              <div class="login d-inline-block mr-3">
                <a href="{{ route('profile') }}"><i class="far fa-user pr-2"></i>Profile</a>
              </div>
              <form action="{{ route('logout') }}" method="post">
                @csrf
  
                <button class="btn btn-white btn-md" type="submit">
                  <i class="fas fa-sign-out-alt"></i>
                  Logout
                </button>
              </form>
            @else
            <div class="login d-inline-block mr-4 ml-5">
              <a href="{{ route('view.login') }}"><i class="far fa-user pr-2"></i>Sign in</a>
            </div>
            @endif
          </div>
        </div>
      </div>
    </nav>
  </header>
  <!--=================================
  Header -->