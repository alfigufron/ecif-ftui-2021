<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Universitas Indonesia</title>

        <link rel="shortcut icon" href="{{ asset('assets/user/images/ecif.png') }}" type="image/x-icon">

        <link rel="stylesheet" href="{{ asset('assets/user/css/flaticon/flaticon.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/user/css/bootstrap/bootstrap.min.css') }}">

        <link rel="stylesheet" href="{{ asset('assets/user/css/owl-carousel/owl.carousel.min.css') }}">

        <!-- fontAW -->
            <script
            src="https://kit.fontawesome.com/5cc5353839.js"
            crossorigin="anonymous"
        ></script>

        <link rel="stylesheet" href="{{ asset('assets/user/css/style.css') }}">

        @stack('push-css')

        @stack('page-style')
    </head>
    <body>
        <!--=================================
        Header -->
        @include('user.layouts.header')
        <!--=================================
        End Header -->

        @yield('contentSec')

        <script src="{{ asset('assets/user/js/jquery-3.4.1.min.js') }}"></script>
        <script src="{{ asset('assets/user/js/popper/popper.min.js') }}"></script>
        <script src="{{ asset('assets/user/js/bootstrap/bootstrap.min.js') }}"></script>
        
        <script src="{{ asset('assets/user/js/owl-carousel/owl-carousel.min.js') }}"></script>
        <script src="{{ asset('assets/user/js/jarallax/jarallax.min.js') }}"></script>
        <script src="{{ asset('assets/user/js/jarallax/jarallax-video.min.js') }}"></script>

        <script src="{{ asset('assets/user/js/custom.js') }}"></script>

        @stack('addJs')
    </body>
</html>
