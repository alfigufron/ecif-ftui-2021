@extends('user.layouts.master')

@section('contentSec')
<!--=================================
inner banner -->
<div class="header-inner bg-light text-center">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="text-primary">Contact</h2>
                <ol class="breadcrumb mb-0 p-0">
                    <li class="breadcrumb-item"><a href="{{ route('home') }}"> Home </a></li>
                    <li class="breadcrumb-item active"> <i class="fas fa-chevron-right"></i> <span> Contact </span></li>
                </ol>
            </div>
        </div>
    </div>
</div>
<!--=================================
  inner banner -->

<!--=================================
Millions of jobs -->
<section class=space-ptb>
    <div class=container>
        <div class="row  ">
            <div class=col-lg-12>
                <div class="text-center">
                    <p class="">Bila Anda membutuhkan informasi lebih lanjut mengenai CDC FT UI atau hendak menyampaikan
                        kritik maupun saran dapat menghubungi kami melalui telepon, email, atau akun media sosial kami.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class=container>
        <div class=row>
            <div class="col-lg-6 col-md-6 mb-4 mb-lg-0">
                <div class="feature-info feature-info-border p-4 text-center">
                    <div class="feature-info-icon mb-3">
                        <i class="fas fa-map-marker-alt"></i>
                    </div>
                    <div class=feature-info-content>
                        <h5 class=text-black>Alamat</h5>
                        <span class=d-block>R. 204 Gedung Engineering Center lantai 2</span>
                        <span class=d-block>Fakultas Teknik Universitas Indonesia <br>
                            Kampus UI, Depok 16424</span>
                    </div>
                </div>
            </div>
            <br>
            <div class="col-lg-6 col-md-6 mb-4 mb-lg-0">
                <div class="feature-info feature-info-border p-4 text-center">
                    <div class="feature-info-icon mb-3">
                        <i class="fas fa-phone"></i>
                    </div>
                    <div class=feature-info-content>
                        <h5 class=text-black>Kontak</h5>
                        <span class=d-block>No. tlpn : 021 786 3506</span>
                        <span>Hp (wa) : 0857 76523987</span>
                    </div>
                </div>
            </div>
            <br>
            <div class="col-lg-6 col-md-6 mb-4 mb-md-0 my-4">
                <div class="feature-info feature-info-border p-4 text-center">
                    <div class="feature-info-icon mb-3">
                        <i class="fas fa-envelope"></i>
                    </div>
                    <div class=feature-info-content>
                        <h5 class=text-black>Email</h5>
                        <span class=d-block>Perusahaan: cdc.ft[at]ui.ac.id</span>
                        <span>Jobseekers: cdcftui[at]eng.ui.ac.id</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6">
                <div class="feature-info feature-info-border p-4 text-center">
                    <div class="feature-info-icon mb-3">
                        <i class="fas fa-share-alt"></i>
                    </div>
                    <div class=feature-info-content>
                        <h5 class=text-black>Media Sosial</h5>
                        <span class=d-block>IG : cdc_ftui</span>
                        <span>Twitter : @cdcftui</span><br>
                        <span>LinkedIn : <a href="https://linkedin.com/company/cdcftui" style="color: black">Buka
                                LinkedIn</a></span><br>
                        <span>Youtube : <a href="https://youtube.com/ftuicdc" style="color: black">Buka
                                Youtube</a></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('page-script')

@endpush