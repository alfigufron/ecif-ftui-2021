@extends('user.layouts.master')

@push('page-style')
    <style>
      .banner{
        min-height: 100vh;
        padding: 100px 0 100px 0 !important;
      }
    </style>
@endpush

@section('contentSec')
    <!--=================================
Banner -->
<section class="banner banner-bg-video bg-holder bg-overlay-white-40" style="" data-jarallax-video="{{ $data->url_home ? $data->url_home : "https://www.youtube.com/embed/7e90gBu4pas" }}">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-6 align-self-center text-center">
          <img class="img-fluid" src="{{ asset('assets/user/images/ecif.png') }}" alt="logo" style="width: 300px">
          <h1 class="text-white my-3" style="color: #001935 !important">Welcome to Engineering Career and Internship Fair 2021</h1>
          <a href="{{ route('map') }}" class="btn btn-primary btn-lg">Go to Main Hall</a>
          <h5 class="my-3">Sponsored by</h5>
          <div class="d-flex justify-content-center">
            <div class="mx-3 d-flex align-items-center">
            <img class="img-fluid" src="{{ asset('assets/user/images/paragon.png') }}" alt="logo" style="width: 320px; z-index:99;">
          </div>
          <div class="mx-3 d-flex align-items-center">
            <img class="img-fluid" src="{{ asset('assets/user/images/astra.png') }}" alt="logo" style="width: 300px; z-index:99;">
          </div>
          <div class="mx-3 d-flex align-items-center">
            <img class="img-fluid" src="{{ asset('assets/user/images/starborn.png') }}" alt="logo" style="width: 320px; z-index:99;">
          </div>
          <div class="mx-3 d-flex align-items-center">
            <img class="img-fluid" src="{{ asset('assets/user/images/philips.png') }}" alt="logo" style="width: 320px; z-index:99;">
          </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--=================================
  Banner -->
@endsection