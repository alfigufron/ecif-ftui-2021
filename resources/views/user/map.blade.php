@extends('user.layouts.master')

@push('page-style')
    <style>
      .job-list{
        border: 1px solid #cccccc !important;
      }
    </style>
@endpush

@section('contentSec')
<!--=================================
inner banner -->
<div class="header-inner bg-light text-center">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2 class="text-primary">Main Hall</h2>
          <ol class="breadcrumb mb-0 p-0">
            <li class="breadcrumb-item"><a href="{{ route('home') }}"> Home </a></li>
            <li class="breadcrumb-item active"> <i class="fas fa-chevron-right"></i> <span> Main Hall </span></li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <!--=================================
  inner banner -->

  <!--=================================
Millions of jobs -->
<section class="space-ptb">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <h5 class="my-3 text-center">Sponsored by</h5>
      <div class="d-flex justify-content-center align-items-center">
        <div class="mx-3 d-flex align-items-center">
        <img class="img-fluid" src="{{ asset('assets/user/images/paragon.png') }}" alt="logo" style="width: 200px; z-index:99;">
      </div>
      <div class="mx-3 d-flex align-items-center">
        <img class="img-fluid" src="{{ asset('assets/user/images/astra.png') }}" alt="logo" style="width: 300px; z-index:99;">
      </div>
      <div class="mx-3 d-flex align-items-center">
        <img class="img-fluid" src="{{ asset('assets/user/images/starborn.png') }}" alt="logo" style="width: 200px; z-index:99;">
      </div>
      <div class="mx-3 d-flex align-items-center">
        <img class="img-fluid" src="{{ asset('assets/user/images/philips.png') }}" alt="logo" style="width: 200px; z-index:99;">
      </div>
      </div>
    </div>
      <div class="col-md-12 mt-5">
        <div class="row justify-content-center align-items-stretch">
          @foreach ($data as $index => $datas)     
            @if ($datas->type == "GOLDEN") 
              <div class="col-md-4 mb-5">
                <div class="job-list job-grid h-100">
                  <div class="job-list-logo d-flex justify-content-center" style="height: 160px;">
                    <img class="w-100" src="{{ asset("company/$datas->logo") }}" alt="{{ $datas->name }}" alt="" style="object-fit: contain;">
                  </div>
                  
                  <div class="job-list-details">
                    <div class="job-list-info">
                      <div class="job-list-title">
                        <h3 class="mb-0">{{ $datas->name }}</h3>

                        @if ($datas->description)
                          <button type="button" class="btn btn-primary btn-sm mt-2" data-toggle="modal" data-target="{{"#modal-company-" . $index}}">
                            About
                          </button>
                        @endif
                            
                        <a class="btn btn-primary btn-sm mt-2" href="{{ route('map.detail', $datas->id) }}">Booth</a>
                      </div>
                    </div>
                  </div>
                </div>

                @if ($datas->description)
                  <div class="modal fade" id="{{"modal-company-" . $index}}" tabindex="-1" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                      <div class="modal-content p-3">
                        <div class="modal-body">
                          <div class="text-center">
                            <h4 class="mb-3">{{ $datas->name }}</h4>
                            {{ $datas->description }}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                @endif  
              </div>
            @endif 
          @endforeach
        </div>
      </div>
      
      <div class="col-md-12">
        <div class="row justify-content-center align-items-stretch">
          @foreach ($data as $index => $datas)
            @if ($datas->type == "SILVER") 
              <div class="col-md-3 mb-5">
                <div class="job-list job-grid h-100">
                  <div class="job-list-logo d-flex justify-content-center" style="height: 160px;">
                    <img class="w-100" src="{{ asset("company/$datas->logo") }}" alt="{{ $datas->name }}" alt="" style="object-fit: contain;">
                  </div>

                  <div class="job-list-details">
                    <div class="job-list-info">
                      <div class="job-list-title">
                        <h4 class="mb-0">{{ $datas->name }}</h4>

                        @if ($datas->description)
                          <button type="button" class="btn btn-primary btn-sm mt-2" data-toggle="modal" data-target="{{"#modal-company-" . $index}}">
                            About
                          </button>
                        @endif
                          
                        <a class="btn btn-primary btn-sm mt-2" href="{{ route('map.detail', $datas->id) }}">Booth</a>
                      </div>
                    </div>
                  </div>
                </div>
                @if ($datas->description)
                  <div class="modal fade" id="{{"modal-company-" . $index}}" tabindex="-1" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                      <div class="modal-content">
                        <div class="modal-body">
                          <div class="text-center">
                            <h4 class="mb-3">{{ $datas->name }}</h4>
                            {{ $datas->description }}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                @endif  
              </div>
            @endif
          @endforeach
        </div>
      </div>
      
      <div class="col-md-12">
        <div class="row justify-content-center align-items-stretch">
          @foreach ($data as $index => $datas)     
            @if ($datas->type == "BRONZE") 
              <div class="col-md-2 mb-5">
                <div class="job-list job-grid h-100">
                  <div class="job-list-logo d-flex justify-content-center" style="height: 100px;">
                    <img class="w-100" src="{{ asset("company/$datas->logo") }}" alt="{{ $datas->name }}" alt="" style="object-fit: contain;">
                  </div>
                  <div class="job-list-details">
                    <div class="job-list-info">
                      <div class="job-list-title">
                        <h5 class="mb-0">{{ $datas->name }}</h5>

                        @if ($datas->description)
                          <button type="button" class="btn btn-primary btn-sm mt-2" data-toggle="modal" data-target="{{"#modal-company-" . $index}}">
                            About
                          </button>
                        @endif

                        <a class="btn btn-primary btn-sm mt-2" href="{{ route('map.detail', $datas->id) }}">
                          Booth
                        </a>
                        
                      </div>
                    </div>
                  </div>
                </div>
                @if ($datas->description)
                  <div class="modal fade" id="{{"modal-company-" . $index}}" tabindex="-1" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered">
                      <div class="modal-content">
                        <div class="modal-body">
                          <div class="text-center">
                            <h4 class="mb-3">{{ $datas->name }}</h4>
                            {{ $datas->description }}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                @endif  
              </div>
            @endif 
          @endforeach
        </div>
    </div>
  </div>
</section>
<!--=================================
job-grid -->
@endsection

@push('page-script')
    
@endpush