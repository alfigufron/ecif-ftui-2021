<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <link rel="shortcut icon" href="{{ asset('assets/user/images/ecif.png') }}" type="image/x-icon">

  <link rel="stylesheet" href="{{ asset('assets/user/css/bootstrap/bootstrap.min.css') }}">

  <link rel="stylesheet" href="{{ asset('assets/user/css/booth.css') }}">
  <title>Universitas Indonesia</title>

</head>

<body>
  @if (Auth::guard('admin')->check() || Auth::guard('admin_company')->check())
  <div class="wrapper-booth">
    <div class="container-fluid">
      <div class="menu d-flex flex-column">
        @if (Auth::guard('web')->check() || Auth::guard('admin')->check() || Auth::guard('admin_company')->check())
          @if ($data->company_stand && $data->company_stand->career)
            <a href="{{ $data->company_stand->career }}" class="my-2">Career</a>
          @endif
          
          @if ($data->company_stand && $data->company_stand->about_us)
            <a href="{{ $data->company_stand->about_us }}" class="my-2">About Us</a>
          @endif
          {{-- @if ($data->type == "GOLDEN" || $data->type == "SILVER")
          @endif --}}

          @if ($data->type == "GOLDEN")
            @if ($data->company_stand && $data->company_stand->internship)
              <a href="{{ $data->company_stand->internship }}" class="my-2">Internship</a>
            @endif

            @if ($data->company_stand->apply || count($data->company_vacancy))
              <a href="{{ ($data->company_stand->apply ? $data->company_stand->apply : route('view.apply', $data->id)) }}" class="my-2">Apply</a>
            @endif
          @endif
          <a href="{{ route('map') }}" class="my-2">Main Hall</a>

          @if ($data->company_stand && $data->company_stand->join_webinar)
            <a href="{{ $data->company_stand->join_webinar }}" class="my-2">Join Webinar</a>
          @endif

        {{-- @elseif(Auth::guard('admin')->check())
          @if ($data->company_stand && $data->company_stand->career)
            <a href="{{ $data->company_stand->career }}" class="my-2">Career</a>
          @endif

          @if ($data->type == "GOLDEN")
          <a href="{{ $data->company_stand ? $data->company_stand->about_us : "#"}}" class="my-2">About Us</a>
          <a href="{{ $data->company_stand ? $data->company_stand->internship : "#"}}" class="my-2">Internship</a>
          @elseif($data->type == "SILVER")
          <a href="{{ $data->company_stand ? $data->company_stand->about_us : "#"}}" class="my-2">About Us</a>
          @endif
        <a href="#" class="my-2">Apply</a>
        <a href="{{ route('company.admin') }}" class="my-2">Back</a>
        
        @elseif(Auth::guard('admin_company')->check())
          @if ($data->company_stand && $data->company_stand->career)
            <a href="{{ $data->company_stand->career }}" class="my-2">Career</a>
          @endif

          @if ($data->type == "GOLDEN")
          <a href="{{ $data->company_stand ? $data->company_stand->about_us : "#"}}" class="my-2">About Us</a>
          <a href="{{ $data->company_stand ? $data->company_stand->internship : "#"}}" class="my-2">Internship</a>
          @elseif($data->type == "SILVER")
          <a href="{{ $data->company_stand ? $data->company_stand->about_us : "#"}}" class="my-2">About Us</a>
          @endif
        <a href="#" class="my-2">Apply</a>
        <a href="{{ route('detail.company') }}" class="my-2">Back</a> --}}
        
        @endif
      </div>
      <div class="row justify-content-center">
        <div class="col-md-12 d-flex justify-content-center mt-2">
          <iframe width="40%" class="my-3 video" height="300"
            {{ $data->company_stand ? "src=" . $data->company_stand->video : "src="}} title="YouTube video player"
            frameborder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowfullscreen></iframe>
        </div>
        {{-- @if ($data->type == "GOLDEN" || $data->type == "SILVER") --}}
        {{-- <div class="col-md-3">
          <div class="img left_side addition">
            @if ( $data->company_stand->left_side_addition)
            <img src={{ asset("assets/admin/fileCompanyStand/" . $data->company_stand->left_side_addition)}} alt="Left
        Side Addition">
        @else
        <img src="{{ asset('assets/admin/images/poster1.jpg') }}" alt="">
        @endif
      </div>
    </div> --}}
    {{-- @endif --}}
    <div class="col-md-3 mx-5">
      <div class="img poster wajib">
        @if ( $data->company_stand)
        <img src={{ asset("assets/admin/fileCompanyStand/" . $data->company_stand->recruitment_poster)}}
          alt="Recruitment Poster" id="poster">
        @else
        <img src="{{ asset('assets/admin/images/poster1.jpg') }}" alt="">
        @endif
      </div>
    </div>
    <div class="col-md-3 d-flex flex-column justify-content-center align-items-center">
      <div class="sec-center">
        <div class="d-flex justify-content-center">
          <img src="{{ asset("company/$data->logo")}}"
            alt="Logo" width="200px" height="200px" id="poster">
        </div>
      <div class="socialmedia my-3 d-flex justify-content-center">
        @if ($data->type == "GOLDEN")
          @if ($data->company_stand && $data->company_stand->twitter)
            <a href="{{ $data->company_stand->twitter }}" class="mx-2"><img src="{{ asset("assets/user/images/twitter.png")}}" alt="" width="35px"></a>
          @endif

          @if ($data->company_stand && $data->company_stand->youtube)
            <a href="{{ $data->company_stand->youtube }}" class="mx-2"><img src="{{ asset("assets/user/images/youtube.png")}}" alt="" width="35px"></a>
          @endif
        @endif
        @if ($data->company_stand && $data->company_stand->facebook)
          <a href="{{ $data->company_stand->facebook }}" class="mx-2"><img src="{{ asset("assets/user/images/facebook.png")}}" alt="" width="35px"></a>
        @endif

        @if ($data->company_stand && $data->company_stand->linkedin)
          <a href="{{ $data->company_stand->linkedin }}" class="mx-2"><img src="{{ asset("assets/user/images/linkedin.png")}}" alt="" width="35px"></a>
        @endif

        @if ($data->company_stand && $data->company_stand->instagram)
          <a href="{{ $data->company_stand->instagram }}" class="mx-2"><img src="{{ asset("assets/user/images/instagram.png")}}" alt="" width="35px"></a>
        @endif
      </div>
      </div>
    </div>
    <div class="col-md-3 mx-5">
      <div class="img poster wajib">
        @if ( $data->company_stand)
        <img src={{ asset("assets/admin/fileCompanyStand/" . $data->company_stand->webinar_poster)}}
          alt="Webinar Poster" id="poster">
        @else
        <img src="{{ asset('assets/admin/images/poster1.jpg') }}" alt="">
        @endif
      </div>
    </div>
    {{-- @if ($data->type == "GOLDEN" || $data->type == "SILVER") --}}
    {{-- <div class="col-md-3">
          <div class="img right_side addition">
            @if ( $data->company_stand->right_side_addition)
            <img src={{ asset("assets/admin/fileCompanyStand/" . $data->company_stand->right_side_addition)}}
    alt="Right Side Addition">
    @else
    <img src="{{ asset('assets/admin/images/poster1.jpg') }}" alt="">
    @endif
  </div>
  </div> --}}
  {{-- @endif --}}
  </div>
  </div>
  </div>
  @else
  @if ($data->company_stand->status == 1 && Auth::guard('web')->check())
  <div class="wrapper-booth">
    <div class="container-fluid">
      <div class="menu d-flex flex-column">
        @if ($data->company_stand && $data->company_stand->career)
          <a href="{{ $data->company_stand->career }}" class="my-2">Career</a>
        @endif
        
        @if ($data->company_stand && $data->company_stand->about_us)
          <a href="{{ $data->company_stand->about_us }}" class="my-2">About Us</a>
        @endif
        {{-- @if ($data->type == "GOLDEN" || $data->type == "SILVER")
        @endif --}}

        @if ($data->type == "GOLDEN")
          @if ($data->company_stand && $data->company_stand->internship)
          <a href="{{ $data->company_stand->internship }}" class="my-2">Internship</a>
          @endif
          
          @if ($data->company_stand->apply || count($data->company_vacancy))
            <a href="{{ ($data->company_stand->apply ? $data->company_stand->apply : route('view.apply', $data->id)) }}" class="my-2">Apply</a>
          @endif
        @endif  
        
        <a href="{{ route('map') }}" class="my-2">Main Hall</a>

        @if ($data->company_stand && $data->company_stand->join_webinar)
          <a href="{{ $data->company_stand->join_webinar }}" class="my-2">Join Webinar</a>
        @endif
      </div>
      <div class="row justify-content-center">
        <div class="col-md-12 d-flex justify-content-center mt-2">
          <iframe width="40%" class="my-3 video" height="300"
            {{ $data->company_stand ? "src=" . $data->company_stand->video : "src="}} title="YouTube video player"
            frameborder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowfullscreen></iframe>
        </div>
        {{-- @if ($data->type == "GOLDEN" || $data->type == "SILVER") --}}
        {{-- <div class="col-md-3">
          <div class="img left_side addition">
            @if ( $data->company_stand->left_side_addition)
            <img src={{ asset("assets/admin/fileCompanyStand/" . $data->company_stand->left_side_addition)}} alt="Left
        Side Addition">
        @else
        <img src="{{ asset('assets/admin/images/poster1.jpg') }}" alt="">
        @endif
      </div>
    </div> --}}
    {{-- @endif --}}
    <div class="col-md-3 mx-5">
      <div class="img poster wajib">
        @if ( $data->company_stand)
        <img src={{ asset("assets/admin/fileCompanyStand/" . $data->company_stand->recruitment_poster)}}
          alt="Recruitment Poster" id="poster">
        @else
        <img src="{{ asset('assets/admin/images/poster1.jpg') }}" alt="">
        @endif
      </div>
    </div>
    <div class="col-md-3 d-flex flex-column justify-content-center align-items-center">
      <div class="sec-center">
      <div class="d-flex justify-content-center">
      <img src="{{ asset("company/$data->logo")}}"
          alt="Recruitment Poster" width="200px" id="poster">
        </div>
      <div class="socialmedia my-3 d-flex justify-content-center">
        @if ($data->type == "GOLDEN")
          @if ($data->company_stand && $data->company_stand->twitter)
            <a href="{{ $data->company_stand->twitter }}" class="mx-2"><img src="{{ asset("assets/user/images/twitter.png")}}" alt="" width="35px"></a>
          @endif

          @if ($data->company_stand && $data->company_stand->youtube)
            <a href="{{ $data->company_stand->youtube }}" class="mx-2"><img src="{{ asset("assets/user/images/youtube.png")}}" alt="" width="35px"></a>
          @endif
        @endif
        @if ($data->company_stand && $data->company_stand->facebook)
          <a href="{{ $data->company_stand->facebook }}" class="mx-2"><img src="{{ asset("assets/user/images/facebook.png")}}" alt="" width="35px"></a>
        @endif

        @if ($data->company_stand && $data->company_stand->linkedin)
          <a href="{{ $data->company_stand->linkedin }}" class="mx-2"><img src="{{ asset("assets/user/images/linkedin.png")}}" alt="" width="35px"></a>
        @endif

        @if ($data->company_stand && $data->company_stand->instagram)
          <a href="{{ $data->company_stand->instagram }}" class="mx-2"><img src="{{ asset("assets/user/images/instagram.png")}}" alt="" width="35px"></a>
        @endif
      </div>
      </div>
    </div>
    <div class="col-md-3 mx-5">
      <div class="img poster wajib">
        @if ( $data->company_stand)
        <img src={{ asset("assets/admin/fileCompanyStand/" . $data->company_stand->webinar_poster)}}
          alt="Webinar Poster" id="poster">
        @else
        <img src="{{ asset('assets/admin/images/poster1.jpg') }}" alt="">
        @endif
      </div>
    </div>
    {{-- @if ($data->type == "GOLDEN" || $data->type == "SILVER") --}}
    {{-- <div class="col-md-3">
          <div class="img right_side addition">
            @if ( $data->company_stand->right_side_addition)
            <img src={{ asset("assets/admin/fileCompanyStand/" . $data->company_stand->right_side_addition)}}
    alt="Right Side Addition">
    @else
    <img src="{{ asset('assets/admin/images/poster1.jpg') }}" alt="">
    @endif
  </div>
  </div> --}}
  {{-- @endif --}}
  </div>
  </div>
  </div>
  @else
  <div class="wrapper-booth">
    <div class="container-fluid">
      <div class="menu d-flex flex-column">
        @if (Auth::guard('web')->check())
        <a href="#" class="my-2">Career</a>
        @if ($data->type == "GOLDEN")
        <a href="#" class="my-2">About Us</a>
        <a href="#" class="my-2">Internship</a>
        <a href="#" class="my-2">Join Webinar</a>
        <a href="#" class="my-2">Apply</a>
        @elseif($data->type == "SILVER")
        <a href="#" class="my-2">About Us</a>
        @endif

        @elseif(Auth::guard('admin')->check() || Auth::guard('admin_company')->check())
        <a href="#" class="my-2">Career</a>
        @if ($data->type == "GOLDEN")
        <a href="#" class="my-2">About Us</a>
        <a href="#" class="my-2">Internship</a>
        <a href="#" class="my-2">Join Webinar</a>
        <a href="#" class="my-2">Apply</a>
        @elseif($data->type == "SILVER")
        <a href="#" class="my-2">About Us</a>
        @endif
        @endif
      </div>
      <div class="row justify-content-center">
        <div class="col-md-12 d-flex justify-content-center mt-2">
          <iframe width="40%" class="my-3 video" height="300" src=title="YouTube video player" frameborder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowfullscreen></iframe>
        </div>
        {{-- @if ($data->type == "GOLDEN" || $data->type == "SILVER") --}}
        {{-- <div class="col-md-3">
              <div class="img left_side addition">
                @if ( $data->company_stand->left_side_addition)
                <img src={{ asset("assets/admin/fileCompanyStand/" . $data->company_stand->left_side_addition)}}
        alt="Left Side Addition">
        @else
        <img src="{{ asset('assets/admin/images/poster1.jpg') }}" alt="">
        @endif
      </div>
    </div> --}}
    {{-- @endif --}}
    <div class="col-md-3 mx-5">
      <div class="img poster wajib">
      </div>
    </div>
    <div class="col-md-3 mx-5">
      <div class="img poster wajib">
      </div>
    </div>
    {{-- @if ($data->type == "GOLDEN" || $data->type == "SILVER") --}}
    {{-- <div class="col-md-3">
              <div class="img right_side addition">
                @if ( $data->company_stand->right_side_addition)
                <img src={{ asset("assets/admin/fileCompanyStand/" . $data->company_stand->right_side_addition)}}
    alt="Right Side Addition">
    @else
    <img src="{{ asset('assets/admin/images/poster1.jpg') }}" alt="">
    @endif
  </div>
  </div> --}}
  {{-- @endif --}}
  </div>
  </div>
  </div>
  @endif
  @endif

  <!-- The Modal -->
<div id="myModal" class="modal">
  <span class="close">&times;</span>
  <img class="modal-content" id="img01">
</div>

  <script src="{{ asset('assets/user/js/jquery-3.4.1.min.js') }}"></script>
  <script src="{{ asset('assets/user/js/popper/popper.min.js') }}"></script>
  <script src="{{ asset('assets/user/js/bootstrap/bootstrap.min.js') }}"></script>
  <script>
    // Get the modal
    var modal = document.getElementById("myModal");

    // Get the image and insert it inside the modal - use its "alt" text as a caption
    var img = document.querySelectorAll("#poster");
    for(let x = 0; x < img.length; x++){
      var modalImg = document.getElementById("img01");
      img[x].onclick = function(){
        modal.style.display = "block";
        modalImg.src = this.src;
      }
  
      // Get the <span> element that closes the modal
      var span = document.getElementsByClassName("close")[0];
  
      // When the user clicks on <span> (x), close the modal
      span.onclick = function() { 
        modal.style.display = "none";
      }
    }
  </script>
</body>

</html>