@extends('user.layouts.master')

@section('contentSec')

<!--=================================
banner -->
<div class="candidate-banner bg-light">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="candidate-list bg-light">
          <div class="candidate-list-image">
            <img class="img-fluid" src="images/avatar/04.jpg" alt="" >
          </div>
          <div class="candidate-list-details">
            <div class="candidate-list-info">
              <div class="candidate-list-title">
                <h5 class="mb-0">{{ $data->name}}</h5>
              </div>
              <div class="candidate-list-option">
                <ul class="list-unstyled">
                  <li><i class="fas fa-filter pr-1"></i>{{ $data->user_faculty ? $data->user_faculty->departement : $data->user_non_faculty->faculty}}</li>
                  <li><i class="fas fa-map-marker-alt pr-1"></i>{{ $data->user_faculty ? "Universitas Indonesia" : $data->user_non_faculty->university }}</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--=================================
banner -->
<!--=================================
inner banner -->
<section class="space-pb">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="sticky-top secondary-menu-sticky-top">
          <div class="secondary-menu">
            <ul>
              <li><a href="#about"> About </a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-8 mb-4 mb-lg-0">
        <div class="jobber-candidate-detail">
          <div id="about">
            <h5 class="mb-3">About Me</h5>
            <div class="border p-3">
              <div class="row">
                <div class="col-md-4 col-sm-6 mb-4">
                  <div class="d-flex">
                    <i class="font-xll text-primary align-self-center flaticon-account"></i>
                    <div class="feature-info-content pl-3">
                      <label class="mb-0">Name:</label>
                      <span class="d-block font-weight-bold text-dark">{{ $data->name }}</span>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 col-sm-6 mb-4">
                  <div class="d-flex">
                    <i class="font-xll text-primary align-self-center flaticon-account"></i>
                    <div class="feature-info-content pl-3">
                      <label class="mb-0">Student Number:</label>
                      <span class="d-block font-weight-bold text-dark">{{ $data->student_number }}</span>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 col-sm-6 mb-4">
                  <div class="d-flex">
                    <i class="font-xll text-primary align-self-center flaticon-contact"></i>
                    <div class="feature-info-content pl-3">
                      <label class="mb-0">Phone :</label>
                      <span class="d-block font-weight-bold text-dark">{{ $data->phone_number }}</span>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 col-sm-6 mb-4">
                  <div class="d-flex">
                    <i class="font-xll text-primary align-self-center flaticon-appointment"></i>
                    <div class="feature-info-content pl-3">
                      <label class="mb-0">Date Of Birth :</label>
                      <span class="d-block font-weight-bold text-dark">{{ $data->birth }}</span>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 col-sm-6 mb-4">
                  <div class="d-flex">
                    <i class="font-xll text-primary align-self-center flaticon-map"></i>
                    <div class="feature-info-content pl-3">
                      <label class="mb-0">Address :</label>
                      <span class="d-block font-weight-bold text-dark">{{ $data->address }}</span>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 col-sm-6 mb-4">
                  <div class="d-flex">
                    <i class="font-xll text-primary align-self-center flaticon-man"></i>
                    <div class="feature-info-content pl-3">
                      <label class="mb-0">Sex :</label>
                      <span class="d-block font-weight-bold text-dark">{{ $data->gender }}</span>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="d-flex">
                    <i class="font-xll text-primary align-self-center flaticon-approval"></i>
                    <div class="feature-info-content pl-3">
                      <label class="mb-0">Email:</label>
                      <span class="d-block font-weight-bold text-dark">{{ $data->email }}</span>
                    </div>
                  </div>
                </div>
                @if ($data->user_non_faculty)    
                <div class="col-md-4">
                  <div class="d-flex">
                    <i class="font-xll text-primary align-self-center flaticon-approval"></i>
                    <div class="feature-info-content pl-3">
                      <label class="mb-0">Faculty Type:</label>
                      <span class="d-block font-weight-bold text-dark">{{ $data->user_non_faculty->faculty_type }}</span>
                    </div>
                  </div>
                </div>
                @endif
              </div>
            </div>
          </div>

          @if ($data->user_application)    
          <hr class="my-4 my-md-5">
          <div id="education">
            <h5 class="mb-3">Apply</h5>
            <div class="jobber-candidate-timeline">
              @foreach ($data->user_application as $datas)    
              <div class="jobber-timeline-icon">
                <img class="" src="{{ asset("company/". $datas->company()->first()->logo) }}" alt="{{ $datas->name }}" alt="" width=30px>
              </div>
              <div class="jobber-timeline-item">
                <div class="jobber-timeline-cricle">
                  <i class="far fa-circle"></i>
                </div>
                <div class="jobber-timeline-info">
                  <a href="{{ route('map.detail', $datas->company_id) }}"><h6 class="mb-2">{{ $datas->company()->first()->name}} | {{ $datas->company_vacancy->name }}</h6></a>
                </div>
              </div>
              @endforeach
            </div>
          </div>
          @endif
      </div>
    </div>

  </div>
</div>
</section>
@endsection

@push('page-script')
    
@endpush