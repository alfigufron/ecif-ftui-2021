@extends('user.layouts.master')

@section('contentSec')
<!--=================================
inner banner -->
<div class="header-inner bg-light text-center">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2 class="text-primary">About Us</h2>
          <ol class="breadcrumb mb-0 p-0">
            <li class="breadcrumb-item"><a href="{{ route('home') }}"> Home </a></li>
            <li class="breadcrumb-item active"> <i class="fas fa-chevron-right"></i> <span> About us </span></li>
          </ol>
        </div>
      </div>
    </div>
  </div>
  <!--=================================
  inner banner -->

  <!--=================================
Millions of jobs -->
<section class="mt-5">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-12">
          <div>
            <p class="my-3">{{ $data->about ? $data->about : "No data show" }}</p>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection

@push('page-script')
    
@endpush