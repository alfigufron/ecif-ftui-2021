@extends('admin.layouts.master')
@section('title', 'Universitas Indonesia')

@section('module')
Data Info
@endsection

@section('breadcump')
<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
  <li class="breadcrumb-item">Company</li>
  <li class="breadcrumb-item active">Detail {{ $data->name}}</li>
</ol>
@endsection

@section('contentSec')
<div class="row">
  <!-- Column -->
  <div class="col-lg-4 col-xlg-3 col-md-5">
    <div class="card">
      <div class="card-body">
        <center class="m-t-30"> <img src="{{ asset("company/$data->logo") }}" width="150" />
          <h4 class="card-title m-t-10">{{ $data->name }}</h4>

          <h6 class="card-subtitle mt-2"><span
              class="label {{$data->type == 'GOLDEN' ? 'label-warning' : ($data->type == 'SILVER' ? 'label-success' : 'label-inverse')}}">{{ $data->type }}</span>
          </h6>

        </center>
      </div>

      <div>
      </div>
      <div class="card-body"> <small class="text-muted">Email address </small>
        <h6>{{$data->admin->email}}</h6> <small class="text-muted p-t-30 db">Name</small>
        <h6>{{$data->admin->name}}</h6>
        <small class="text-muted p-t-30 db">Status Stand</small>
        @if ($data->company_stand->status == 0)
          <h6>Waiting for admin confirmation</h6>
          @if (Auth::guard('admin')->check())
          <form action="{{ route('updateStatus.company.admin', $data->id) }}" method="POST">
            @csrf
            @method('put')
            <button class="btn btn-primary my-3">Accept Change Stand</button>
          </form>
          @endif
        @else
        <h6>It's been confirmed by admin</h6>
        @endif
        @if (session('message'))
        <div class="alert alert-success mt-3">{{session('message')}}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      </div>
      @endif
      </div>
    </div>
  </div>
  <!-- Column -->
  <!-- Column -->
  <div class="col-lg-8 col-xlg-9 col-md-7">
    <div class="card">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs profile-tab" role="tablist">
        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home" role="tab">Company Stands</a>
        </li>
        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#poster" role="tab">Poster</a> </li>
        @if (Auth::guard('admin')->check())
        <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#settings" role="tab">Company Admin</a> </li>
        @endif
      </ul>
      <!-- Tab panes -->
      <div class="tab-content">
        <div class="tab-pane active" id="home" role="tabpanel">
          <div class="card-body">
            <form action="{{ Auth::guard('admin')->check() ? route('updateCompanyStand.admin', $data->id) : route('updateCompanyStand.company.admin', $data->id) }}" method="post"
              enctype="multipart/form-data">
              @csrf
              @method('put')
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="urlVideo" @error('video') class="text-danger" @enderror>URL Full Video @error('video')
                      | {{$message}}
                      @enderror:</label>
                    <input type="text" class="form-control" id="urlVideo" @if (old('video')) value="{{old('video')}}"
                      @else value="{{$data->company_stand->video}}" @endif name="video" placeholder="Input Key Video">
                    <iframe width="100%" class="my-3" height="315"
                      {{ $data->company_stand->video ? "src=" . $data->company_stand->video : "src="}}
                      title="YouTube video player" frameborder="0"
                      allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                      allowfullscreen></iframe>
                    {{-- <iframe width="560" height="315" src="https://www.youtube.com/embed/0IDI6zS5x1Q" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> --}}
                  </div>

                  <hr>

                  <div class="form-group">
                    <label for="career">Career</label>
                    <input type="text" class="form-control mb-4" id="career" @if (old('career'))
                      value="{{old('career')}}" @else value="{{$data->company_stand->career}}" @endif name="career"
                      placeholder="Input Website URL">
                  </div>
                  
                  <div class="form-group">
                    <label for="internship">Join Webinar</label>
                    <input type="text" class="form-control mb-4" id="join_webinar" @if (old('join_webinar'))
                      value="{{old('join_webinar')}}" @else value="{{$data->company_stand->join_webinar}}" @endif
                      name="join_webinar" placeholder="Input Website URL">
                  </div>

                  {{-- @if ($data->type == "GOLDEN" || $data->type == "SILVER") --}}
                  <div class="form-group">
                    <label for="about_us">About Us</label>
                    <input type="text" class="form-control mb-4" id="about_us" @if (old('about_us'))
                      value="{{old('about_us')}}" @else value="{{$data->company_stand->about_us}}" @endif
                      name="about_us" placeholder="Input Website URL">
                  </div>
                  {{-- @endif --}}

                  @if ($data->type == "GOLDEN")
                  <div class="form-group">
                    <label for="internship">Internship</label>
                    <input type="text" class="form-control mb-4" id="internship" @if (old('internship'))
                      value="{{old('internship')}}" @else value="{{$data->company_stand->internship}}" @endif
                      name="internship" placeholder="Input Website URL">
                  </div>
                  <div class="form-group">
                    <label for="internship">Apply</label>
                    <input type="text" class="form-control mb-4" id="apply" @if (old('apply'))
                      value="{{old('apply')}}" @else value="{{$data->company_stand->apply}}" @endif
                      name="apply" placeholder="Input Website URL">
                  </div>
                  <div class="form-group">
                    <label for="internship">Twitter</label>
                    <input type="text" class="form-control mb-4" id="twitter" @if (old('twitter'))
                      value="{{old('twitter')}}" @else value="{{$data->company_stand->twitter}}" @endif
                      name="twitter" placeholder="Input Website URL">
                  </div>
                  <div class="form-group">
                    <label for="internship">Youtube</label>
                    <input type="text" class="form-control mb-4" id="youtube" @if (old('youtube'))
                      value="{{old('youtube')}}" @else value="{{$data->company_stand->youtube}}" @endif
                      name="youtube" placeholder="Input Website URL">
                  </div>
                  @endif

                  <div class="form-group">
                    <label for="internship">Facebook</label>
                    <input type="text" class="form-control mb-4" id="facebook" @if (old('facebook'))
                      value="{{old('facebook')}}" @else value="{{$data->company_stand->facebook}}" @endif
                      name="facebook" placeholder="Input Website URL">
                  </div>
                  <div class="form-group">
                    <label for="internship">LinkedIn</label>
                    <input type="text" class="form-control mb-4" id="internship" @if (old('linkedin'))
                      value="{{old('linkedin')}}" @else value="{{$data->company_stand->linkedin}}" @endif
                      name="linkedin" placeholder="Input Website URL">
                  </div>
                  <div class="form-group">
                    <label for="internship">Instagram</label>
                    <input type="text" class="form-control mb-4" id="instagram" @if (old('instagram'))
                      value="{{old('instagram')}}" @else value="{{$data->company_stand->instagram}}" @endif
                      name="instagram" placeholder="Input Website URL">
                  </div>

                  <div class="modal-footer">
                    @if(Auth::guard('admin')->check())
                    <a href="{{ route('booth.company.admin', $data->id) }}" target="_blank"
                      class="btn btn-default">Preview Booth</a>
                    <button type="submit" class="btn btn-primary">Update</button>
                    @else
                    <a href="{{ route('booth.company') }}" target="_blank" class="btn btn-default">Preview Booth</a>
                    <button type="submit" class="btn btn-primary">Update</button>
                    @endif
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="tab-pane" id="poster" role="tabpanel">
          <div class="card-body">
            <form action="{{ Auth::guard('admin')->check() ? route('updateCompanyStandPoster.admin', $data->id) : route('updateCompanyStandPoster.company.admin', $data->id) }}" method="post"
              enctype="multipart/form-data">
              @csrf
              @method('put')
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="recuirementPoster">Recruitment Poster</label>
                    <input type="file" class="form-control mb-4" id="recuirementPoster" name="recruitment_poster">
                    @if ($data->company_stand->recruitment_poster)
                    <img src={{ asset("assets/admin/fileCompanyStand/" . $data->company_stand->recruitment_poster)}}
                      alt="" style="width: 120px">

                    @else
                    <img src="" alt="">
                    @endif
                  </div>

                  <hr>

                  <div class="form-group">
                    <label for="webinarPoster">Webinar Poster</label>
                    <input type="file" class="form-control mb-4" id="webinarPoster" name="webinar_poster">
                    @if ( $data->company_stand->webinar_poster)
                    <img src={{ asset("assets/admin/fileCompanyStand/" . $data->company_stand->webinar_poster)}} alt=""
                      style="width: 120px">

                    @else
                    <img src="" alt="">
                    @endif
                  </div>
                  <hr>

                  {{-- @if ($data->type == "SILVER" || $data->type == "GOLDEN")
                  <div class="form-group">
                    <label for="leftSide">Left Side</label>
                    <input type="file" class="form-control mb-4" id="leftSide" name="left_side_addition" required>
                    @if ( $data->company_stand->left_side_addition)
                    <img src={{ asset("assets/admin/fileCompanyStand/" . $data->company_stand->left_side_addition)}}
                      alt="" style="width: 120px">

                    @else
                    <img src="" alt="">
                    @endif
                  </div>
                  <hr>

                  <div class="form-group">
                    <label for="rightSide">Right Side</label>
                    <input type="file" class="form-control mb-4" id="rightSide" name="right_side_addition" required>
                    @if ( $data->company_stand->right_side_addition)
                    <img src={{ asset("assets/admin/fileCompanyStand/" . $data->company_stand->right_side_addition)}}
                      alt="" style="width: 120px">

                    @else
                    <img src="" alt="">
                    @endif
                  </div>
                  @endif --}}
                </div>
              </div>
              <button type="submit" class="btn btn-primary btn-block">Update</button>
            </form>

            <a href="{{ Auth::guard('admin')->check() ? route('booth.company.admin', $data->id) : route('booth.company') }}"
              target="_blank"
              class="btn btn-secondary btn-block mt-2"
            >
              Preview Booth
            </a>

            <div class="row">
              @if ($data->company_stand->recruitment_poster)
                <div class="{{ $data->company_stand->recruitment_poster && $data->company_stand->webinar_poster ? 'col-6' : 'col-12'  }}">
                  <form action="{{ route((Auth::guard('admin')->check() ? 'deleteCompanyStandPoster.admin' : 'deleteCompanyStandPoster.company.admin'), [$data->id, 'data' => 'recruitment_poster']) }}" method="post">
                    @csrf
    
                    <button type="submit" class="btn btn-danger btn-block mt-2">Delete Recruitment Poster</button>
                  </form>
                </div>
              @endif
              
              @if ($data->company_stand->webinar_poster)
                <div class="{{ $data->company_stand->recruitment_poster && $data->company_stand->webinar_poster ? 'col-6' : 'col-12'  }}">
                  <form action="{{ route((Auth::guard('admin')->check() ? 'deleteCompanyStandPoster.admin' : 'deleteCompanyStandPoster.company.admin'), [$data->id, 'data' => 'webinar_poster']) }}" method="post">
                    @csrf
    
                    <button type="submit" class="btn btn-danger btn-block mt-2">Delete Webinar Poster</button>
                  </form>
                </div>
              @endif
            </div>


          </div>
        </div>
        @if (Auth::guard('admin')->check())
        <div class="tab-pane" id="settings" role="tabpanel">
          <div class="card-body">
            <form action="{{ route('updateCompanyAdmin.company.admin', $data->id) }}" method="post"
              enctype="multipart/form-data">
              @csrf
              @method('put')
              <div class="form-group">
                <label for="example-email" class="col-md-12">Email</label>
                <div class="col-md-12">
                  <input type="text" class="form-control form-control-line" id="example-email" @if (old('email'))
                    value="{{old('email')}}" @else value="{{$data->admin->email}}" @endif name="email">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-12">Name</label>
                <div class="col-md-12">
                  <input type="text" class="form-control form-control-line" @if (old('name')) value="{{old('name')}}"
                    @else value="{{$data->admin->name}}" @endif name="name">
                </div>
              </div>
              <div class="form-group">
                <label class="col-md-12">Password</label>
                <div class="col-md-12">
                  <input type="text" class="form-control" id="password" name="password">
                </div>
              </div>
              <div class="modal-footer">
                <a href="{{ route('company.admin') }}" class="btn btn-default">Delete</a>
                <button type="submit" class="btn btn-primary">Update</button>
              </div>
            </form>
          </div>
        </div>
        @endif
      </div>
    </div>
  </div>
  <!-- Column -->
</div>
@endsection

@push('script-add')
<script>
$('.toast').toast('show');
</script>
@endpush