@extends('admin.layouts.master')
@section('title', 'Universitas Indonesia')

@section('module')
    Add Data
@endsection

@section('breadcump')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
    <li class="breadcrumb-item">Company</li>
    <li class="breadcrumb-item active">Add</li>
</ol>
@endsection

@section('contentSec')
<div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header bg-info">
          <h4 class="m-b-0 text-white">Add Company</h4>
        </div>
        <div class="card-body">
          <form action="{{ route('company.add.admin') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <h3 class="card-title">Admin Company</h3>
            <hr>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="company_name" @error('name_admin')
                  class="text-danger"
                  @enderror class="control-label">Admin Company Name  @error('name_admin')
                  | {{$message}}
                  @enderror:</label>
                  <input type="text" class="form-control" id="company_name" name="name_admin">
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group">
                  <label for="email" @error('name')
                  class="text-danger"
                  @enderror class="control-label">Email Address  @error('email')
                  | {{$message}}
                  @enderror:</label>
                  <input type="email" class="form-control" id="email" name="email">
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label for="password" @error('password') class="text-danger" @enderror class="control-label">Password @error('password')
                    | {{$message}}
                    @enderror:</label>
                    <input type="text" class="form-control" id="password" name="password">
                </div>
              </div>
            </div>
            
            <h3 class="card-title">Company Info</h3>
            <hr>
             <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="company_name" @error('name')
                    class="text-danger"
                    @enderror class="control-label">Company Name  @error('name')
                    | {{$message}}
                    @enderror:</label>
                    <input type="text" class="form-control" id="company_name" name="name">
                  </div>
                </div>
                
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="type" @error('type')
                    class="text-danger"
                    @enderror class="control-label">Type @error('type')
                    | {{$message}}
                    @enderror:</label>
                    <select class="form-control" id="type" name="type">
                      <option value="BRONZE">Bronze</option>
                      <option value="SILVER">Silver</option>
                      <option value="GOLDEN">Gold</option>
                    </select>
                  </div>
                </div>

                <div class="col-md-12">
                  <div class="form-group">
                    <label for="description" @error('description')
                    class="text-danger"
                    @enderror class="control-label">Description @error('description')
                    | {{$message}}
                    @enderror:</label>
                    <textarea name="description" id="description" cols="30" rows="3" class="form-control"></textarea>
                  </div>
                </div>

                <div class="col-md-12">
                  <div class="form-group">
                    <label for="logo" @error('logo')
                    class="text-danger"
                        @enderror class="control-label">Logo @error('logo')
                        | {{$message}}
                    @enderror:</label>
                    <input type="file" class="form-control" id="logo" name="logo">
                  </div>
                </div>
                
              </div>
            </div>
            <div class="modal-footer">
              <a href="{{ route('company.admin') }}" class="btn btn-default">Close</a>
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
         </form>
        </div>
      </div>
    </div>
    </div>
</div>
@endsection