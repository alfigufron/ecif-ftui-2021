<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  <link rel="stylesheet" href="{{ asset('assets/user/css/bootstrap/bootstrap.min.css') }}">

  <link rel="stylesheet" href="{{ asset('assets/user/css/booth.css') }}">
  <title>Document</title>
</head>

<body>
  <div class="wrapper-booth">
    <div class="container-fluid">
      <div class="menu d-flex flex-column">
        @if ($data->type == "GOLDEN")
        <a href="#">About Us</a>
        <a href="#" class="my-4">Career</a>
        <a href="#">Internship</a>
        @elseif($data->type == "SILVER")
        <a href="#">About Us</a>
        <a href="#" class="my-4">Career</a>
        @else
        <a href="#" class="my-4">Career</a>
        @endif
      </div>

      <div class="row justify-content-center">
        <div class="col-md-12 d-flex justify-content-center mt-2">
          <iframe width="40%" class="my-3 video" height="275"
            {{ $data->company_stand->video ? "src=https://www.youtube.com/embed/" . $data->company_stand->video : "src="}}
            title="YouTube video player" frameborder="0"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
            allowfullscreen></iframe>
        </div>

        @if ($data->type == "GOLDEN" || $data->type == "SILVER")
        <div class="col-md-3">
          <div class="img left_side addition">
            @if ( $data->company_stand->left_side_addition)
            <img src={{ asset("assets/admin/fileCompanyStand/" . $data->company_stand->left_side_addition)}}
              alt="Left Side Addition">
            @else
            <img src="{{ asset('assets/admin/images/poster1.jpg') }}" alt="">
            @endif
          </div>
        </div>
        @endif

        <div class="col-md-3">
          <div class="img poster wajib">
            @if ( $data->company_stand->recruitment_poster)
            <img src={{ asset("assets/admin/fileCompanyStand/" . $data->company_stand->recruitment_poster)}}
              alt="Recruitment Poster">
            @else
            <img src="{{ asset('assets/admin/images/poster1.jpg') }}" alt="">
            @endif
          </div>
        </div>

        <div class="col-md-3">
          <div class="img poster wajib">
            @if ( $data->company_stand->webinar_poster)
            <img src={{ asset("assets/admin/fileCompanyStand/" . $data->company_stand->webinar_poster)}}
              alt="Webinar Poster">
            @else
            <img src="{{ asset('assets/admin/images/poster1.jpg') }}" alt="">
            @endif
          </div>
        </div>

        @if ($data->type == "GOLDEN" || $data->type == "SILVER")
        <div class="col-md-3">
          <div class="img right_side addition">
            @if ( $data->company_stand->right_side_addition)
            <img src={{ asset("assets/admin/fileCompanyStand/" . $data->company_stand->right_side_addition)}}
              alt="Right Side Addition">
            @else
            <img src="{{ asset('assets/admin/images/poster1.jpg') }}" alt="">
            @endif
          </div>
        </div>
        @endif

      </div>
    </div>
  </div>

  <script src="{{ asset('assets/user/js/popper/popper.min.js') }}"></script>
  <script src="{{ asset('assets/user/js/bootstrap/bootstrap.min.js') }}"></script>
</body>

</html>