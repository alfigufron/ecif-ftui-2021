@extends('admin.layouts.master')
@section('title', 'Universitas Indonesia')

@section('module')
    Edit Data
@endsection

@section('breadcump')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
    <li class="breadcrumb-item">Company</li>
    <li class="breadcrumb-item active">Edit</li>
</ol>
@endsection

@section('contentSec')
<div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header bg-info">
          <h4 class="m-b-0 text-white">{{ $data->name}} Company</h4>
        </div>
        <div class="card-body">
            <form action="{{route('company.update.admin', $data->id)}}" method="post" enctype="multipart/form-data">
              @csrf
                  @method('put')
                <div class="form-body">
                    <h3 class="card-title">Data Info</h3>
                    <hr>
                    <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label class="control-label col-md-6">Company name</label>
                              <div class="col-md-12">
                                  <input type="text" class="form-control" @if (old('name'))
                                  value="{{old('name')}}" @else value="{{$data->name}}"
                                  @endif name="name">
                                </div>
                          </div>
                      </div>
                      <!--/span-->
                      <div class="col-md-6">
                          <div class="form-group">
                              <label class="control-label col-md-6">Type</label>
                              <div class="col-md-12">
                                <select class="form-control" id="type" name="type">
                                  <option value="BRONZE" @if ($data->type === 'BRONZE') selected @endif>Bronze</option>
                                  <option value="SILVER" @if ($data->type === 'SILVER') selected @endif>Silver</option>
                                  <option value="GOLDEN" @if ($data->type === 'GOLDEN') selected @endif>Gold</option>
                                </select>
                                </div>
                          </div>
                      </div>

                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="description" @error('description')
                          class="text-danger"
                          @enderror class="control-label col-md-12">Description @error('description')
                          | {{$message}}
                          @enderror:</label>
                          <div class="col-md-12">
                            <textarea name="description" id="description" cols="30" rows="3" class="form-control">@if (old('description')){{old('description')}}@else{{$data->description}}@endif</textarea>
                          </div>
                        </div>
                      </div>

                      <!--/span-->
                      <div class="col-md-12">
                          <div class="form-group">
                              <label class="control-label col-md-6">Logo</label>
                              <div class="col-md-12">
                                  <input type="file" class="form-control" @if (old('logo'))
                                  value="{{old('logo')}}" @else value="{{$data->logo}}"
                                    @endif name="logo">
                                </div>
                          </div>
                      </div>
                      <!--/span-->
                    </div>
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                    <a href="{{ route('company.admin') }}" class="btn btn-dark text-white"> <i class="fas fa-times"></i> Cancel</a>
                </div>
            </form>
        </div>
      </div>
    </div>
</div>
@endsection
@push('page-script')
    
@endpush