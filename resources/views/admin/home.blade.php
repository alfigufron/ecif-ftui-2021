@extends('admin.layouts.master')
@section('title', 'Universitas Indonesia')

@section('module')
    Dashboard
@endsection

@section('breadcump')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
    <li class="breadcrumb-item active">Dasboard</li>
</ol>
@endsection

@section('contentSec')
<div class="row">
    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">Howdy {{ Auth::guard('admin')->check() ? Auth::guard('admin')->user()->name : Auth::guard('admin_company')->user()->name}}</h5>
            <br> Welcome to the <strong>Indonesia University</strong> Website Administrator Page
            <br>You are on the PROTECTED page of the <strong>Indonesia University</strong> Website Content Management
            system.<br><br>
            <p> This page is specifically intended ONLY for website administrators who will manage content.<br>
              Before making arrangements, there are few things to note: <br><br>

              1. Save and secure your Email and Password to avoid things that are not desirable.<br>
              2. Prepare the material or content that will be updated first.<br>
              3. Also prepare photos, videos or other related material, to facilitate the updating process.<br>
              4. If there is any questions about using features in this backoffice, you can ask the web developer
              contact
              below.<br>
              5. Use the latest version of the browser to get the compatibility features in this backoffice.<br>
              <p>
                Web Developer Contact :<br>
                Email : hello@rundeglobe.com<br>
                Phone : +62-8577-9287-053
              </p><br />
          </div>
        </div>
      </div>
    </div>
</div>
@endsection
@push('page-script')
    
@endpush