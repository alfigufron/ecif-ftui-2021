@extends('admin.layouts.master')
@section('title', 'Universitas Indonesia')

@section('module')
  Log
@endsection

@section('breadcump')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="javascript:void(0)">Setting</a></li>
    <li class="breadcrumb-item active">Log</li>
</ol>
@endsection

@section('contentSec')
<div class="row">
    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
      <div class="card" style="height: 520px">
        <div class="card-body">
          <div class="card" style="background-color: #9ed5ff; height: 100%;padding: 10px 16px;overflow: scroll;">
            @if (app('request')->input('limit') <= $total)
              <div class="text-center mb-3">
                <a href="{{ route('view.log', ['limit' => app('request')->input('limit') + 10]) }}" class="btn btn-secondary" style="color: #004274;font-weight: 500;">Load More</a>
              </div>
            @endif

            @foreach ($data as $item)
              <h5 style="font-weight: 500;color: #004274">
                [{{ $item->created_at }}] {{ $item->log }}
              </h5>
            @endforeach
          </div>
        </div>
      </div>
    </div>
</div>
@endsection