@extends('admin.layouts.master')
@section('title', 'Universitas Indonesia')

@section('module')
    Data Info
@endsection

@section('breadcump')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
    <li class="breadcrumb-item">Web Setting App</li>
</ol>
@endsection

@section('contentSec')
<div class="row">
  <!-- Column -->
  <div class="col-lg-12 col-xlg-9 col-md-7">
      <div class="card">
        <div class="card-body">
          @if (session('message'))
        <div class="alert alert-success mt-3">{{session('message')}}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      </div>
      @endif
        <form action="{{ route('settingApp.update.admin') }}" method="post" enctype="multipart/form-data">
          @csrf
          @method('put')
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="">Url Video Homepage</label>
                  <input type="text" class="form-control" placeholder="Input URL" name="url" @if (old('url_home'))
                  value="{{old('url_home')}}" @else value="{{$data->url_home}}" @endif>
                </div>
              </div>
              <div class="col-md-6">
                <iframe width="100%" height="315" {{ $data->url_home ? "src=" . $data->url_home : "src="}} title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
              <hr>
              <div class="col-md-12 my-2">
                <label for="">About</label>
                  <input type="text" class="form-control" placeholder="Input About" name="about" @if (old('about'))
                  value="{{old('about')}}" @else value="{{$data->about}}" @endif>
              </div>
              <div class="col-md-12 my-2">
                <label for="">Schedule</label>
                  <input type="file" class="form-control" name="schedule">
              </div>
              <div class="col-md-12">
                @if ( $data->schedule)
                    <img src={{ asset("schedule/" . $data->schedule)}}
                      alt="" style="width: 500px">

                    @else
                    <img src="" alt="">
                    @endif
              </div>
              <div class="col-md-12 my-2">
              <button type="submit" class="btn btn-success float-right"> <i class="fa fa-check"></i> Save</button>
            </div>
            </div>
          </form>
        </div>
      </div>
  </div>
  <!-- Column -->
</div>
@endsection
