@extends('admin.layouts.master')
@section('title', 'Universitas Indonesia')

@section('module')
    Edit Data
@endsection

@section('breadcump')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
    <li class="breadcrumb-item">User</li>
    <li class="breadcrumb-item active">Edit</li>
</ol>
@endsection

@section('contentSec')
<div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header bg-info">
          <h4 class="m-b-0 text-white">{{ $data->name}} </h4>
        </div>
        <div class="card-body">
            <form action="{{route('users.update.admin', $data->id)}}" method="post" enctype="multipart/form-data">
              @csrf
                  @method('put')
                <div class="form-body">
                  <h3 class="card-title">Users info</h3>
                  <hr>
                  <form action="{{ route('users.add.admin') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="name" @error('name') class="text-danger" @enderror class="control-label">Name @error('name')
                            | {{$message}}
                            @enderror:</label>
                          <input type="text" class="form-control" id="name" name="name" @if (old('name'))
                          value="{{old('name')}}" @else value="{{$data->name}}"
                          @endif">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="email" @error('email') class="text-danger" @enderror class="control-label">Email address
                            @error('email')
                            | {{$message}}
                            @enderror:</label>
                          <input type="email" class="form-control" id="email" name="email" @if (old('email'))
                          value="{{old('email')}}" @else value="{{$data->email}}"
                          @endif">
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label for="gender" @error('gender') class="text-danger" @enderror class="control-label">Gender
                            @error('gender')
                            | {{$message}}
                            @enderror:</label>
                          <select class="form-control" id="gender" name="gender">
                            <option value="" selected>Select Gender</option>
                            <option value="M" @if (old('gender')==="M" ) selected @endif>
                              Male
                            </option>
                            <option value="F" @if (old('gender')==="F" ) selected @endif>
                              Female
                            </option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label for="study_level" @error('study_level') class="text-danger" @enderror class="control-label">Study
                            Level @error('study_level')
                            | {{$message}}
                            @enderror:</label>
                          <select class="form-control" id="study_level" name="study_level">
                            <option value="" selected>Select Study Level</option>
                            <option value="Program Diploma" @if (old('study_level')==="Program Diploma" ) selected @endif>
                              Program Diploma
                            </option>
                            <option value="Program Sarjana" @if (old('study_level')==="Program Sarjana" ) selected @endif>
                              Program Sarjana
                            </option>
                            <option value="Program Profesi" @if (old('study_level')==="Program Profesi" ) selected @endif>
                              Program Profesi
                            </option>
                            <option value="Program Magister" @if (old('study_level')==="Program Magister" ) selected @endif>
                              Program Magister
                            </option>
                            <option value="Program Doktor" @if (old('study_level')==="Program Doktor" ) selected @endif>
                              Program Doktor
                            </option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label for="type" @error('type') class="text-danger" @enderror class="control-label">Type @error('type')
                            | {{$message}}
                            @enderror:</label>
                          <select class="form-control" id="type" name="type">
                            <option value="" selected>Select Study Level</option>
                            <option value="FTUI" @if (old('type')==="FTUI" ) selected @endif>
                              FTUI
                            </option>
                            <option value="NON" @if (old('type')==="NON" ) selected @endif>
                              NON
                            </option>
                          </select>
                        </div>
                      </div>
                      <div class="col-md-3">
                        <div class="form-group">
                          <label for="graduate_year" @error('graduate_year') class="text-danger" @enderror
                            class="control-label">Graduate year
                            @error('graduate_year')
                            | {{$message}}
                            @enderror:</label>
                          <input type="number" class="form-control" id="graduate_year" name="graduate_year"
                          @if (old('graduate_year'))
                          value="{{old('graduate_year')}}" @else value="{{$data->graduate_year}}"
                          @endif">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="name" @error('student_number') class="text-danger" @enderror class="control-label">Student
                            number @error('student_number')
                            | {{$message}}
                            @enderror:</label>
                          <input type="number" class="form-control" id="student_number" name="student_number"
                          @if (old('student_number'))
                          value="{{old('student_number')}}" @else value="{{$data->student_number}}"
                          @endif">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="name" @error('birth') class="text-danger" @enderror class="control-label">Birth
                            @error('birth')
                            | {{$message}}
                            @enderror:</label>
                          <input type="date" class="form-control" id="birth" name="birth" @if (old('birth'))
                          value="{{old('birth')}}" @else value="{{$data->birth}}"
                          @endif">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="phone_number" @error('phone_number') class="text-danger" @enderror
                            class="control-label">Phone number @error('phone_number')
                            | {{$message}}
                            @enderror:</label>
                          <input type="text" class="form-control" id="phone_number" name="phone_number"
                          @if (old('phone_number'))
                          value="{{old('phone_number')}}" @else value="{{$data->phone_number}}"
                          @endif">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="name" @error('address') class="text-danger" @enderror class="control-label">Address
                            @error('address')
                            | {{$message}}
                            @enderror:</label>
                          <textarea class="form-control" id="address" rows="3" placeholder="Input Address"
                          @if (old('address'))
                          value="{{old('address')}}" @else value="{{$data->address}}"
                          @endif name="address"></textarea>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="password" @error('password') class="text-danger" @enderror class="control-label">Password
                            @error('password')
                            | {{$message}}
                            @enderror:</label>
                          <input type="text" class="form-control" id="password" name="password">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <h3 class="card-title">User Faculty</h3>
                        <hr>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="type" @error('departement') class="text-danger" @enderror
                                class="control-label">Departement @error('departement')
                                | {{$message}}
                                @enderror:</label>
                              <select class="form-control" id="departement" name="departement">
                                <option value="" selected>Select Departement</option>
                                <option value="Departemen Teknik Sipil" @if (old('departement')==='Departemen Teknik Sipil' )
                                  selected @endif>
                                  Departemen Teknik Sipil
                                </option>
                                <option value="Departemen Teknik Mesin" @if (old('departement')==='Departemen Teknik Mesin' )
                                  selected @endif>
                                  Departemen Teknik Mesin
                                </option>
                                <option value="Departemen Teknik Elektro" @if (old('departement')==='Departemen Teknik Elektro' )
                                  selected @endif>
                                  Departemen Teknik Elektro
                                </option>
                                <option value="Departemen Teknik Material" @if (old('departement') === 'Departemen Teknik Metalurgi dan Material') selected @endif>
                                  Departemen Teknik Metalurgi
                                  dan Material
                                </option>
                                <option value="Departemen Teknik Kimia" @if (old('departement')==='Departemen Teknik Kimia' )
                                  selected @endif>
                                  Departemen Teknik Kimia
                                </option>
                                <option value="Departemen Teknik Industri" @if (old('departement')==='Departemen Teknik Industri'
                                  ) selected @endif>
                                  Departemen Teknik Industri
                                </option>
                                <option value="Departemen Teknik Arsitektur" @if (old('departement')==='Departemen Teknik Arsitektur' ) selected @endif>
                                  Departemen Teknik Arsitektur
                                </option>
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <h3 class="card-title">User Non Faculty</h3>
                        <hr>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="type" @error('university') class="text-danger" @enderror
                                class="control-label">University @error('university')
                                | {{$message}}
                                @enderror:</label>
                                <input type=string class=form-control id=nameUniversity name=university @if (old('university'))
                                value="{{old('university')}}" @else value="{{$detail->university}}"
                                @endif">
                            </div>
                          </div>
          
                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="type" @error('faculty') class="text-danger" @enderror
                                class="control-label">Faculty @error('faculty')
                                | {{$message}}
                                @enderror:</label>
                              <select class="form-control" id="faculty" name="faculty">
                                <option value="" selected>Select Faculty</option>
                                                      <option
                                                          @if(old('faculty') === "Fakultas Kedokteran")
                                                              selected
                                                          @endif
                                                          value="Fakultas Kedokteran"
                                                      >
                                                          Fakultas Kedokteran
                                                      </option>
                                                      <option
                                                          @if(old('faculty') === "Fakultas Kedokteran Gigi")
                                                              selected
                                                          @endif
                                                          value="Fakultas Kedokteran Gigi"
                                                      >
                                                          Fakultas Kedokteran Gigi
                                                      </option>
                                                      <option
                                                          @if(old('faculty') === "Fakultas Matematika & IPAMIPA")
                                                              selected
                                                          @endif
                                                          value="Fakultas MIPA"
                                                      >
                                                          Fakultas Matematika & IPA
                                                      </option>
                                                      <option
                                                          @if(old('faculty') === "Fakultas Hukum")
                                                              selected
                                                          @endif
                                                          value="Fakultas Hukum"
                                                      >
                                                          Fakultas Hukum
                                                      </option>
                                                      <option
                                                          @if(old('faculty') === "Fakultas Ekonomi & Bisnis")
                                                              selected
                                                          @endif
                                                          value="Fakultas Ekonomi & Bisnis"
                                                      >
                                                          Fakultas Ekonomi & Bisnis
                                                      </option>
                                                      <option
                                                          @if(old('faculty') === "Fakultas Ilmu Pengetahuan dan Budaya")
                                                              selected
                                                          @endif
                                                          value="Fakultas Ilmu Pengetahuan dan Budaya"
                                                      >
                                                          Fakultas Ilmu Pengetahuan dan Budaya
                                                      </option>
                                                      <option
                                                          @if(old('faculty') === "Fakultas Psikologi")
                                                              selected
                                                          @endif
                                                          value="Fakultas Psikologi"
                                                      >
                                                          Fakultas Psikologi
                                                      </option>
                                                      <option
                                                          @if(old('faculty') === "Fakultas Ilmu Sosial dan Ilmu Politik")
                                                              selected
                                                          @endif
                                                          value="Fakultas Ilmu Sosial dan Ilmu Politik"
                                                      >
                                                          Fakultas Ilmu Sosial dan Ilmu Politik
                                                      </option>
                                                      <option
                                                          @if(old('faculty') === "Fakultas Kesehatan Masyarakat")
                                                              selected
                                                          @endif
                                                          value="Fakultas Kesehatan Masyarakat"
                                                      >
                                                          Fakultas Kesehatan Masyarakat
                                                      </option>
                                                      <option
                                                          @if(old('faculty') === "Fakultas Ilmu Komputer")
                                                              selected
                                                          @endif
                                                          value="Fakultas Ilmu Komputer"
                                                      >
                                                          Fakultas Ilmu Komputer
                                                      </option>
                                                      <option
                                                          @if(old('faculty') === "Fakultas Ilmu Keperawatan")
                                                              selected
                                                          @endif
                                                          value="Fakultas Ilmu Keperawatan"
                                                      >
                                                          Fakultas Ilmu Keperawatan
                                                      </option>
                                                      <option
                                                          @if(old('faculty') === "Fakultas Farmasi")
                                                              selected
                                                          @endif
                                                          value="Fakultas Farmasi"
                                                      >
                                                          Fakultas Farmasi
                                                      </option>
                                                      <option
                                                          @if(old('faculty') === "Fakultas Ilmu Administrasi")
                                                              selected
                                                          @endif
                                                          value="Fakultas Ilmu Administrasi"
                                                      >
                                                          Fakultas Ilmu Administrasi
                                                      </option>
                                                      <option
                                                          @if(old('faculty') === "Fakultas Ilmu Lingkungan")
                                                              selected
                                                          @endif
                                                          value="Fakultas Ilmu Lingkungan"
                                                      >
                                                          Fakultas Ilmu Lingkungan
                                                      </option>
                                                      <option
                                                          @if(old('faculty') === "Fakultas Kajian Stratejik & Global")
                                                              selected
                                                          @endif
                                                          value="Fakultas Kajian Stratejik & Global"
                                                      >
                                                          Fakultas Kajian Stratejik & Global
                                                      </option>
                                                      <option
                                                          @if(old('faculty') === "Program Vokasi")
                                                              selected
                                                          @endif
                                                          value="Program Vokasi"
                                                      >
                                                          Program Vokasi
                                                      </option>
                              </select>
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="type" @error('faculty_type') class="text-danger" @enderror class="control-label">Type @error('faculty_type')
                                | {{$message}}
                                @enderror:</label>
                              <select class="form-control" id="faculty_type" name="faculty_type">
                                <option value="" selected>Select Faculty Type</option>
                                <option value="FT" @if (old('faculty_type')==="FT" ) selected @endif>
                                  FT
                                </option>
                                <option value="NON" @if (old('faculty_type')==="NON" ) selected @endif>
                                  NON
                                </option>
                              </select>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                    <a href="{{ route('company.admin') }}" class="btn btn-dark text-white"> <i class="fas fa-times"></i> Cancel</a>
                </div>
            </form>
        </div>
      </div>
    </div>
</div>
@endsection