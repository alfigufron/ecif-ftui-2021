@extends('admin.layouts.master')
@section('title', 'Universitas Indonesia')

@section('module')
Users Management
@endsection

@section('breadcump')
<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
  <li class="breadcrumb-item active">Dasboard</li>
  <li class="breadcrumb-item active">Users</li>
</ol>
@endsection

@section('contentSec')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
        <div class="d-flex justify-content-between">
          <div>
            <a href="{{ route('users.view.add.admin') }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i>
              Create New
            </a>
          </div>

          <div class="d-flex">
            <div class="mr-3">
              <form action="{{ route('users.admin') }}" method="get" class="d-inline-flex">
                <input class="form-control mr-2" type="text" name="search" placeholder="Search by name" value="{{ !empty($params['search']) ? $params['search'] : '' }}">
                <button type="submit" class="btn btn-primary">
                  <i class="fas fa-search"></i>
                </button>
              </form>
            </div>
            <div>
              <a href="{{ route('users.admin.export') }}" class="btn btn-primary"><i class="fas fa-file-export"></i>
                Export Data
              </a>
            </div>
          </div>
        </div>
        @if (session('message'))
        <div class="alert alert-success mt-3">{{session('message')}}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      </div>
      @endif
        <div class="table-responsive mt-4">
          <table class="table">
            <thead>
              <tr>
                <th>Name</th>
                <th>Student Number</th>
                <th>Type</th>
                <th>Opsi</th>
              </tr>
            </thead>
            <tbody>
              @if (count($data) != 0)    
              @foreach ($data as $no => $datas)
              <tr>
                <td>{{ $datas->name }}</td>
                <td>{{ $datas->student_number }}</td>
                <td><span
                    class="label {{$datas->type == 'FTUI' ? 'label-warning' : 'label-success'}}">{{ $datas->type }}</span>
                </td>
                <td>
                  <a href="{{ route('users.detail.admin', $datas->id) }}" class="btn btn-icon btn-info"
                    data-toggle="tooltip" data-placement="top" title="Details"><i class="fas fa-info"></i></a>
                  <a href="#" data-id="{{$datas->id}}" class="btn btn-icon btn-danger confirmation"
                    data-toggle="tooltip" data-placement="top" title="Hapus">
                    <form style="display: none" action="{{route('users.delete.admin', $datas->id)}}" method="POST"
                      id="hapus{{$datas->id}}">
                      @csrf
                      @method('DELETE')
                    </form>
                    <i class="fas fa-times"></i>
                  </a>
                </td>
              </tr>
              @endforeach
              @else
              <tr>
                <td><h4>No data to show</h4></td>
              </tr>
              @endif

            </tbody>
          </table>

          {{$data->appends($params)->links()}}
          
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@push('script-add')
<script>
"use strict";
$(".confirmation").click(function(e) {
  var id = e.target.dataset.id;
  Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
    if (result.value) {
      Swal.fire(
        'Deleted!',
        'Your file has been deleted.',
        'success'
        )
      $(`#hapus${id}`).submit();
    }
  });
});
</script>
@endpush