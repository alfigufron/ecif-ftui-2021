@extends('admin.layouts.master')
@section('title', 'Universitas Indonesia')

@section('module')
    Data Info
@endsection

@section('breadcump')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
    <li class="breadcrumb-item">User</li>
    <li class="breadcrumb-item active">Detail {{ $data->name}}</li>
</ol>
@endsection

@section('contentSec')
<div class="row">
  <!-- Column -->
  <div class="col-lg-4 col-xlg-3 col-md-5">
      <div class="card">
          <div class="card-body">
              <center class="m-t-30">
                  <h4 class="card-title m-t-10">{{ $data->name }}</h4>
                  @if ($level == "userFaculty")
                      <h5 class="m-t-10">{{  $detail->departement }}</h5>
                  @else
                    <h5 class="m-t-10">{{  $detail->university }}</h5>
                  @endif
                  <h6 class="card-subtitle mt-2"><span class="label {{$data->type == 'FTUI' ? 'label-warning' : 'label-success'}}">{{ $data->type }}</span></h6>
              </center>
              @if (session('message'))
        <div class="alert alert-success mt-3">{{session('message')}}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      </div>
      @endif
          </div>
      </div>
  </div>
  <!-- Column -->
  <!-- Column -->
  <div class="col-lg-8 col-xlg-9 col-md-7">
      <div class="card">
          <!-- Nav tabs -->
          <ul class="nav nav-tabs profile-tab" role="tablist">
              <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#home" role="tab">About</a> </li>
              <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#profile" role="tab">Apply</a> </li>
          </ul>
          <!-- Tab panes -->
          <div class="tab-content">
              <div class="tab-pane active" id="home" role="tabpanel">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Student Number</strong>
                            <br>
                            <p class="text-muted">{{ $data->student_number }}</p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Full Name</strong>
                            <br>
                            <p class="text-muted">{{ $data->name }}</p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Mobile</strong>
                            <br>
                            <p class="text-muted">{{ $data->phone_number }}</p>
                        </div>
                        <div class="col-md-3 col-xs-6 b-r"> <strong>Email</strong>
                            <br>
                            <p class="text-muted">{{ $data->email }}</p>
                        </div>
                        <div class="col-md-3 col-xs-6"> <strong>Location</strong>
                            <br>
                            <p class="text-muted">{{ $data->address }}</p>
                        </div>
                        <div class="col-md-3 col-xs-6"> <strong>Gender</strong>
                            <br>
                            <p class="text-muted">{{ $data->gender }}</p>
                        </div>
                        <div class="col-md-3 col-xs-6"> <strong>Study Level</strong>
                            <br>
                            <p class="text-muted">{{ $data->study_level }}</p>
                        </div>
                        <div class="col-md-3 col-xs-6"> <strong>Type</strong>
                            <br>
                            <p class="text-muted">{{ $data->type }}</p>
                        </div>
                        <div class="col-md-3 col-xs-6"> <strong>Graduate Year</strong>
                            <br>
                            <p class="text-muted">{{ $data->graduate_year }}</p>
                        </div>
                        <div class="col-md-3 col-xs-6"> <strong>Location</strong>
                            <br>
                            <p class="text-muted">{{ $data->address }}</p>
                        </div>
                        <div class="col-md-3 col-xs-6"> <strong>Location</strong>
                            <br>
                            <p class="text-muted">{{ $data->address }}</p>
                        </div>
                        <div class="col-md-3 col-xs-6"> <strong>Birth</strong>
                            <br>
                            <p class="text-muted">{{ $data->birth }}</p>
                        </div>
                        @if ($level == "userFaculty")    
                        <div class="col-md-3 col-xs-6"> <strong>Departement</strong>
                            <br>
                            <p class="text-muted">{{ $detail->departement }}</p>
                        </div>
                        @else
                        <div class="col-md-3 col-xs-6"> <strong>University</strong>
                            <br>
                            <p class="text-muted">{{ $detail->university }}</p>
                        </div>
                        <div class="col-md-3 col-xs-6"> <strong>Faculty</strong>
                            <br>
                            <p class="text-muted">{{ $detail->faculty }}</p>
                        </div>
                        <div class="col-md-3 col-xs-6"> <strong>Faculty Type</strong>
                            <br>
                            <p class="text-muted">{{ $detail->faculty_type }}</p>
                        </div>
                        @endif
                    </div>
                    <hr>
                    <div class="row">
                        <form action="{{route('users.update.admin', $data->id)}}" method="post" enctype="multipart/form-data">
                            @csrf
                                @method('put')
                              <div class="form-body">
                                <h3 class="card-title">Edit Users Info</h3>
                                <hr>
                                <form action="{{ route('users.add.admin') }}" method="POST" enctype="multipart/form-data">
                                  @csrf
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label for="name" @error('name') class="text-danger" @enderror class="control-label">Name @error('name')
                                          | {{$message}}
                                          @enderror:</label>
                                        <input type="text" class="form-control" id="name" name="name" @if (old('name'))
                                        value="{{old('name')}}" @else value="{{$data->name}}"
                                        @endif">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label for="email" @error('email') class="text-danger" @enderror class="control-label">Email address
                                          @error('email')
                                          | {{$message}}
                                          @enderror:</label>
                                        <input type="email" class="form-control" id="email" name="email" @if (old('email'))
                                        value="{{old('email')}}" @else value="{{$data->email}}"
                                        @endif">
                                      </div>
                                    </div>
                                    <div class="col-md-3">
                                      <div class="form-group">
                                        <label for="gender" @error('gender') class="text-danger" @enderror class="control-label">Gender
                                          @error('gender')
                                          | {{$message}}
                                          @enderror:</label>
                                        <select class="form-control" id="gender" name="gender">
                                          <option value="" selected>Select Gender</option>
                                          <option value="M" @if ($data->gender === "M" ) selected @endif>
                                            Male
                                          </option>
                                          <option value="F" @if ($data->gender === "F" ) selected @endif>
                                            Female
                                          </option>
                                        </select>
                                      </div>
                                    </div>
                                    <div class="col-md-3">
                                      <div class="form-group">
                                        <label for="study_level" @error('study_level') class="text-danger" @enderror class="control-label">Study 
                                          Level @error('study_level')
                                          | {{$message}}
                                          @enderror:</label>
                                        <select class="form-control" id="study_level" name="study_level">
                                          <option value="" selected>Select Study Level</option>
                                          <option value="Program Diploma" @if ($data->study_level === "Program Diploma" ) selected @endif>
                                            Program Diploma
                                          </option>
                                          <option value="Program Sarjana" @if ($data->study_level === "Program Sarjana" ) selected @endif>
                                            Program Sarjana
                                          </option>
                                          <option value="Program Profesi" @if ($data->study_level === "Program Profesi" ) selected @endif>
                                            Program Profesi
                                          </option>
                                          <option value="Program Magister" @if ($data->study_level === "Program Magister" ) selected @endif>
                                            Program Magister
                                          </option>
                                          <option value="Program Doktor" @if ($data->study_level === "Program Doktor" ) selected @endif>
                                            Program Doktor
                                          </option>
                                        </select>
                                      </div>
                                    </div>
                                    <div class="col-md-3">
                                      <div class="form-group">
                                        <label for="type" @error('type') class="text-danger" @enderror class="control-label">Type @error('type')
                                          | {{$message}}
                                          @enderror:</label>
                                        <select class="form-control" id="type" name="type">
                                          <option value="" selected>Select Study Level</option>
                                          <option value="FTUI" @if ($data->type ==="FTUI" ) selected @endif>
                                            FTUI
                                          </option>
                                          <option value="NON" @if ($data->type ==="NON" ) selected @endif>
                                            NON
                                          </option>
                                        </select>
                                      </div>
                                    </div>
                                    <div class="col-md-3">
                                      <div class="form-group">
                                        <label for="graduate_year" @error('graduate_year') class="text-danger" @enderror
                                          class="control-label">Graduate year
                                          @error('graduate_year')
                                          | {{$message}}
                                          @enderror:</label>
                                        <input type="number" class="form-control" id="graduate_year" name="graduate_year"
                                        @if (old('graduate_year'))
                                        value="{{old('graduate_year')}}" @else value="{{$data->graduate_year}}"
                                        @endif">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label for="name" @error('student_number') class="text-danger" @enderror class="control-label">Student
                                          number @error('student_number')
                                          | {{$message}}
                                          @enderror:</label>
                                        <input type="number" class="form-control" id="student_number" name="student_number"
                                        @if (old('student_number'))
                                        value="{{old('student_number')}}" @else value="{{$data->student_number}}"
                                        @endif">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label for="name" @error('birth') class="text-danger" @enderror class="control-label">Birth
                                          @error('birth')
                                          | {{$message}}
                                          @enderror:</label>
                                        <input type="date" class="form-control" id="birth" name="birth" value="{{$data->birth}}">
                                      </div>
                                    </div>
                                    <div class="col-md-12">
                                      <div class="form-group">
                                        <label for="phone_number" @error('phone_number') class="text-danger" @enderror
                                          class="control-label">Phone number @error('phone_number')
                                          | {{$message}}
                                          @enderror:</label>
                                        <input type="text" class="form-control" id="phone_number" name="phone_number"
                                        @if (old('phone_number'))
                                        value="{{old('phone_number')}}" @else value="{{$data->phone_number}}"
                                        @endif">
                                      </div>
                                    </div>
                                    <div class="col-md-12">
                                      <div class="form-group">
                                        <label for="name" @error('address') class="text-danger" @enderror class="control-label">Address
                                          @error('address')
                                          | {{$message}}
                                          @enderror:</label>
                                        <textarea class="form-control" id="address" rows="3" placeholder="Input Address"
                                        @if (old('address'))
                                        value="{{old('address')}}" @else
                                        @endif name="address">{{$data->address}}
                                      </textarea>
                                      </div>
                                    </div>
                                    <div class="col-md-12">
                                      <div class="form-group">
                                        <label for="password" @error('password') class="text-danger" @enderror class="control-label">Password
                                          @error('password')
                                          | {{$message}}
                                          @enderror:</label>
                                        <input type="text" class="form-control" id="password" name="password">
                                      </div>
                                    </div>

                                    @if (!empty($detail->departement))
                                      <div class="col-md-6">
                                        <h3 class="card-title">User Faculty</h3>
                                        <hr>
                                        <div class="row">
                                          <div class="col-md-12">
                                            <div class="form-group">
                                              <label for="type" @error('departement') class="text-danger" @enderror
                                                class="control-label">Departement @error('departement')
                                                | {{$message}}
                                                @enderror:</label>
                                              <select class="form-control" id="departement" name="departement">
                                                <option value="" selected>Select Departement</option>
                                                <option value="Departemen Teknik Sipil" @if ($detail->departement === 'Departemen Teknik Sipil' )
                                                  selected @endif>
                                                  Departemen Teknik Sipil
                                                </option>
                                                <option value="Departemen Teknik Mesin" @if ($detail->departement === 'Departemen Teknik Mesin' )
                                                  selected @endif>
                                                  Departemen Teknik Mesin
                                                </option>
                                                <option value="Departemen Teknik Elektro" @if ($detail->departement === 'Departemen Teknik Elektro' )
                                                  selected @endif>
                                                  Departemen Teknik Elektro
                                                </option>
                                                <option value="Departemen Teknik Material" @if ($detail->departement ===  'Departemen Teknik Metalurgi dan Material') selected @endif>
                                                  Departemen Teknik Metalurgi
                                                  dan Material
                                                </option>
                                                <option value="Departemen Teknik Kimia" @if ($detail->departement === 'Departemen Teknik Kimia' )
                                                  selected @endif>
                                                  Departemen Teknik Kimia
                                                </option>
                                                <option value="Departemen Teknik Industri" @if ($detail->departement === 'Departemen Teknik Industri'
                                                  ) selected @endif>
                                                  Departemen Teknik Industri
                                                </option>
                                                <option value="Departemen Teknik Arsitektur" @if ($detail->departement === 'Departemen Teknik Arsitektur' ) selected @endif>
                                                  Departemen Teknik Arsitektur
                                                </option>
                                              </select>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    @else
                                      <div class="col-md-6">
                                        <h3 class="card-title">User Non Faculty</h3>
                                        <hr>
                                        <div class="row">
                                          <div class="col-md-12">
                                            <div class="form-group">
                                              <label for="type" @error('university') class="text-danger" @enderror
                                                class="control-label">University @error('university')
                                                | {{$message}}
                                                @enderror:</label>
                                                <input type=string class=form-control id=nameUniversity name=university @if (old('university'))
                                                value="{{old('university')}}" @else value="{{$detail->university}}"
                                                @endif">
                                            </div>
                                          </div>
                          
                                          <div class="col-md-12">
                                            <div class="form-group">
                                              <label for="type" @error('faculty') class="text-danger" @enderror
                                                class="control-label">Faculty @error('faculty')
                                                | {{$message}}
                                                @enderror:</label>
                                                <input type="text" name="faculty" id="" class="form-control" value="{{$detail->faculty}}">
                                            </div>
                                          </div>
                                          <div class="col-md-12">
                                            <div class="form-group">
                                              <label for="type" @error('faculty_type') class="text-danger" @enderror class="control-label">Type @error('faculty_type')
                                                | {{$message}}
                                                @enderror:</label>
                                              <select class="form-control" id="faculty_type" name="faculty_type">
                                                <option value="" selected>Select Faculty Type</option>
                                                <option value="FT" @if ($detail->faculty_type ==="FT" ) selected @endif>
                                                  FT
                                                </option>
                                                <option value="NON" @if ($detail->faculty_type ==="NON" ) selected @endif>
                                                  NON
                                                </option>
                                              </select>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    @endif
                                  </div>
                              </div>
                              <div class="form-actions">
                                  <button type="submit" class="btn btn-success float-right"> <i class="fa fa-check"></i> Save</button>
                              </div>
                          </form>
                    </div>
                    <hr>
                </div>
              </div>
              <!--second tab-->
              <div class="tab-pane" id="profile" role="tabpanel">
                  <div class="card-body">
                    @if ($data->user_application)    
                    <div class="profiletimeline">
                      @foreach ($data->user_application as $datas)
                      <div class="sl-item">
                          <div class="sl-left"> <img src="{{ asset("company/". $datas->company()->first()->logo) }}" alt="{{ $datas->name }}" alt="user" class="img-circle" /> </div>
                          <div class="sl-right">
                              <div><a href="#" class="link">{{ $data->name }}</a>
                                  <p>Apply to <a href="{{ route('map.detail', $datas->company_id) }}"> {{ $datas->company()->first()->name}}</a></p>
                              </div>
                          </div>
                      </div>
                      <hr>
                      @endforeach
                  </div>
                  @endif
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- Column -->
</div>
@endsection