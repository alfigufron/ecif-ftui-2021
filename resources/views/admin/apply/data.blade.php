@extends('admin.layouts.master')
@section('title', 'Universitas Indonesia')

@section('module')
Users Apply
@endsection

@section('breadcump')
<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
  <li class="breadcrumb-item">Dasboard</li>
  <li class="breadcrumb-item">Users</li>
  <li class="breadcrumb-item active">Apply</li>
</ol>
@endsection

@section('contentSec')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
        <div class="d-flex justify-content-between">

          <a href="{{ route('applyAllCompany.export.admin') }}" class="btn btn-primary"><i class="fas fa-file-export"></i>
            Export Data</a>
            
        </div>
        <div class="table-responsive mt-4">
          <table class="table">
            <thead>
              <tr>
                <th>Company</th>
                <th>Opsi</th>
              </tr>
            </thead>
            <tbody>

              @if (count($data) != 0)    
              @foreach ($data as $no => $datas)
              <tr>
                <td>{{ $datas->name }}</td>
                <td>
                  <a href="{{ route('usersApply.detail.admin', $datas->id) }}" class="btn btn-icon btn-info"
                    data-toggle="tooltip" data-placement="top" title="List Users Apply"><i class="fas fa-info"></i></a>
                </td>
              </tr>
              @endforeach
              @else
              <tr>
                <td><h4>No data to show</h4></td>
              </tr>
              @endif

            </tbody>
          </table>

          {{$data->links()}}
          
        </div>
      </div>
    </div>
  </div>
</div>
@endsection