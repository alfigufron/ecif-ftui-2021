@extends('admin.layouts.master')
@section('title', 'Universitas Indonesia')

@section('module')
Users Apply
@endsection

@section('breadcump')
<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
  <li class="breadcrumb-item">Dasboard</li>
  <li class="breadcrumb-item">Users</li>
  <li class="breadcrumb-item active">Apply</li>
</ol>
@endsection

@section('contentSec')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
        <div class="d-flex justify-content-between">

          @if (Auth::guard('admin')->check())
            
            <a href="{{ route('applyByCompany.export.admin', $id) }}" class="btn btn-primary"><i class="fas fa-file-export"></i>
            Export Data</a>

          @elseif(Auth::guard('admin_company')->check())

            <a href="{{ route('usersApply.export.company') }}" class="btn btn-primary"><i class="fas fa-file-export"></i>
            Export Data</a>

          @endif
        </div>
        <div class="table-responsive mt-4">
          <table class="table">
            <thead>
              <tr>
                <th>Student Number</th>
                <th>Email</th>
                <th>Name</th>
                <th>Vacancy</th>
              </tr>
            </thead>
            <tbody>

              @if (count($data) != 0)    
              @foreach ($data as $no => $datas)
              <tr>
                <td>{{ $datas->user->student_number }}</td>
                <td>{{ $datas->user->email }}</td>
                <td>{{ $datas->user->name }}</td>
                <td>{{ $datas->company_vacancy->name }}</td>
              </tr>
              @endforeach
              @else
              <tr>
                <td><h4>No data to show</h4></td>
              </tr>
              @endif

            </tbody>
          </table>

          {{$data->links()}}
          
        </div>
      </div>
    </div>
  </div>
</div>
@endsection