<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="user-pro"> <a class="waves-effect waves-dark">
                    <span class="hide-menu">{{ Auth::guard('admin')->check() ? Auth::guard('admin')->user()->name : Auth::guard('admin_company')->user()->name}}</span></a>
                </li>
                <li class="nav-small-cap">--- MENU</li>
                @if (Auth::guard('admin')->check())
                <li> <a class="waves-effect waves-dark" href="{{ route('dashboard.admin') }}"><i class="fas fa-fire"></i><span class="hide-menu">Dashboard</span></a>
                </li>
                <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="fas fa-building"></i><span class="hide-menu">Manage Company</span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{ route('company.admin') }}">Company</a></li>
                    </ul>
                </li>
                <li> <a class="waves-effect waves-dark" href="{{ route('users.admin')}}"><i class="fas fa-users"></i></i><span class="hide-menu">Manage Users</span></a>
                </li>
                <li> <a class="waves-effect waves-dark" href="{{ route('usersApply.admin')}}"><i class="fas fa-users-cog"></i><span class="hide-menu">Users Apply</span></a>
                </li>
                <li> <a class="waves-effect waves-dark" href="{{ route('settingApp.admin')}}"><i class="fas fa-edit"></i><span class="hide-menu">Web App</span></a>
                </li>
                <li> <a class="waves-effect waves-dark" href="{{ route('visitorReport.admin') }}"><i class="fas fa-file-export"></i><span class="hide-menu">Visitor Report</span></a>
                </li>
                <li> <a class="waves-effect waves-dark" href="{{ route('visitorCompanyReport.admin') }}"><i class="fas fa-file-export"></i><span class="hide-menu">Company Report</span></a>
                </li>
                <li class="nav-small-cap">--- SETTING</li>
                <li> <a class="waves-effect waves-dark" href="{{ route('view.log', ['limit' => 20]) }}"><i class="fas fa-cogs"></i><span class="hide-menu">Log</span></a>
                </li>
                @elseif(Auth::guard('admin_company')->check())
                <li> <a class="waves-effect waves-dark" href="{{ route('dashboard.company') }}"><i class="fas fa-fire"></i><span class="hide-menu">Dashboard</span></a>
                </li>
                <li> <a a class="waves-effect waves-dark" href="{{ route('detail.company') }}"><i class="fas fa-building"></i><span class="hide-menu">My Company</span></a>
                <li> <a class="waves-effect waves-dark" href="{{ route('usersApply.company')}}"><i class="fas fa-users-cog"></i><span class="hide-menu">Users Apply</span></a>
                    <li> <a class="waves-effect waves-dark" href="{{ route('vacancy.company')}}"><i class="fas fa-users"></i></i><span class="hide-menu">Vacancy</span></a>
                @endif
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>