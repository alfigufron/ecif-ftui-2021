<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link rel="shortcut icon" href="{{ asset('assets/user/images/ecif.png') }}" type="image/x-icon">

    <link rel="stylesheet" href="{{ asset('assets/admin/css/style.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/css/pages/dashboard1.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/admin/node_modules/sweetalert2/dist/sweetalert2.min.css') }}">

    @stack('link-rel')

     <!-- fontAW -->
     <script
     src="https://kit.fontawesome.com/5cc5353839.js"
     crossorigin="anonymous"
   ></script>

    <title>@yield('title')</title>
</head>
<body class="skin-blue fixed-layout">
    {{-- <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Universitas Indonesia</p>
        </div>
    </div> --}}

    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        @include('admin.layouts.header')
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        @include('admin.layouts.sidebar')
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h4 class="text-themecolor">@yield('module')</h4>
                    </div>
                    <div class="col-md-7 align-self-center text-right">
                        <div class="d-flex justify-content-end align-items-center">
                            @yield('breadcump')
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="contentSec">
                    @yield('contentSec')
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- footer -->
        <!-- ============================================================== -->
        @include('admin.layouts.footer')
        <!-- ============================================================== -->
        <!-- End footer -->
        <!-- ============================================================== -->
    </div>

    <script src="{{ asset('assets/admin/node_modules/jquery/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('assets/admin/node_modules/popper/popper.min.js') }}"></script>
    <script src="{{ asset('assets/admin/node_modules/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/admin/node_modules/sweetalert2/dist/sweetalert2.min.js') }}"></script>

    <script src="{{ asset('assets/admin/js/perfect-scrollbar.jquery.min.js') }}"></script>
    <script src="{{ asset('assets/admin/js/waves.js') }}"></script>
    <script src="{{ asset('assets/admin/js/sidebarmenu.js') }}"></script>
    <script src="{{ asset('assets/admin/js/custom.min.js') }}"></script>

    <script src="{{ asset('assets/admin/js/dashboard1.js') }}"></script>

    @stack('script-add')
</body>
</html>