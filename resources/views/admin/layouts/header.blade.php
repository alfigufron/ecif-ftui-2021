<header class="topbar">
    <nav class="navbar top-navbar navbar-expand-md navbar-dark">
        <!-- ============================================================== -->
        <!-- Logo -->
        <!-- ============================================================== -->
        <div class="navbar-header">
            <a class="navbar-brand" href="{{ route('home') }}">
                <!-- Logo icon -->

                <!--End Logo icon -->
                <span class="hidden-xs"><span class="font-bold">Indonesia</span>university</span>
            </a>
        </div>
        <!-- ============================================================== -->
        <!-- End Logo -->
        <!-- ============================================================== -->
        <div class="navbar-collapse">
            <!-- ============================================================== -->
            <!-- toggle and nav items -->
            <!-- ============================================================== -->
            <ul class="navbar-nav mr-auto">
                <!-- This is  -->
                <li class="nav-item"> <a class="nav-link nav-toggler d-block d-md-none waves-effect waves-dark"
                        href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                <li class="nav-item"> <a
                        class="nav-link sidebartoggler d-none d-lg-block d-md-block waves-effect waves-dark"
                        href="javascript:void(0)"><i class="icon-menu"></i></a> </li>
                <!-- ============================================================== -->
            </ul>
            <!-- ============================================================== -->
            <!-- User profile and search -->
            <!-- ============================================================== -->
            <ul class="navbar-nav my-lg-0">
                <!-- ============================================================== -->
                <!-- User Profile -->
                <!-- ============================================================== -->

                <li class="nav-item">
                    @if (Auth::guard('admin')->check())
                        <form action="{{ route('logout.admin') }}" method="post" class="nav-link">
                            @csrf
                            <button type="submit" class="btn text-light" style="font-size: 14px">
                                Logout
                            </button>
                        </form>
                    @elseif(Auth::guard('admin_company')->check())
                        <form action="{{ route('logout.company') }}" method="post" class="nav-link">
                            @csrf
                            <button type="submit" class="btn text-light" style="font-size: 14px">
                                Logout
                            </button>
                        </form>
                    @endif
                </li>
                <!-- ============================================================== -->
                <!-- End User Profile -->
                <!-- ============================================================== -->
            </ul>
        </div>
    </nav>
</header>