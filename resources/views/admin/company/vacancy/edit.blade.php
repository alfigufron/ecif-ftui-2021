@extends('admin.layouts.master')
@section('title', 'Universitas Indonesia')

@section('module')
    Edit Data
@endsection

@push('link-rel')
  <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@endpush

@section('breadcump')
<ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
    <li class="breadcrumb-item">Vacancy</li>
    <li class="breadcrumb-item active">Edit</li>
</ol>
@endsection

@section('contentSec')
<div class="row">
    <div class="col-12">
      <div class="card">
        <div class="card-header bg-info">
          <h4 class="m-b-0 text-white">Edit Vacancy</h4>
        </div>
        <div class="card-body">
            <form action="{{route('vacancy.update.company', $data->id)}}" method="post" enctype="multipart/form-data">
              @csrf
                  @method('put')
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="company_name" @error('name')
                        class="text-danger"
                        @enderror class="control-label">Name Vacancy  @error('name')
                        | {{$message}}
                        @enderror:</label>
                        <input type="text" class="form-control" id="company_name" @if (old('name'))
                        value="{{old('name')}}" @else value="{{$data->name}}"
                        @endif name="name">
                      </div>
                    </div>
      
                    <div class="col-md-12">
                      <div class="form-group">
                        <label for="description" @error('description')
                        class="text-danger"
                        @enderror class="control-label">Description  @error('description')
                        | {{$message}}
                        @enderror:</label>
                        <textarea name="description" id="description" cols="30" rows="10" class="form-control">{{ $data->description }}</textarea>
                        {{-- <p>Description : {!! $data->description !!}</p> --}}
                      </div>
                    </div>
      
                  </div>
                <div class="form-actions">
                    <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                    <a href="{{ route('vacancy.company') }}" class="btn btn-dark text-white"> <i class="fas fa-times"></i> Cancel</a>
                </div>
            </form>
        </div>
      </div>
    </div>
</div>
@endsection
@push('script-add')
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

<script>
  $(document).ready(function() {
    $('#description').summernote();
  });
</script>
@endpush