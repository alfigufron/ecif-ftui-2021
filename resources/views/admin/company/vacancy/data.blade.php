@extends('admin.layouts.master')
@section('title', 'Universitas Indonesia')

@section('module')
Vacancy Management
@endsection

@section('breadcump')
<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
  <li class="breadcrumb-item">Dasboard</li>
  <li class="breadcrumb-item active">Vacancy</li>
</ol>
@endsection

@section('contentSec')
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">All Vacancy</h4>
        <h6 class="card-subtitle">Create New Vacancy</h6>
        <div class="d-flex justify-content-between">
          <a href="{{ route('vacancy.view.add.company') }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i>
            Create New</a>

        </div>
        @if (session('message'))
        <div class="alert alert-success mt-3">{{session('message')}}
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
      </div>
      @endif
        <div class="table-responsive mt-4">
          <table class="table">
            <thead>
              <tr>
                <th>Name</th>
                <th>Deskripsi</th>
                <th>Opsi</th>
              </tr>
            </thead>
            <tbody>
              @if (count($data) != 0)    
              @foreach ($data as $no => $datas)
              <tr>
                <td>{{ $datas->name }}</td>
                <td>{!! $datas->description !!}</td>
                <td>

                  <a href="{{ route('vacancy.edit.company', $datas->id) }}" class="btn btn-icon btn-warning"
                    data-toggle="tooltip" data-placement="top" title="Edit"><i class="fas fa-edit"></i></a>

                  <a href="#" data-id="{{$datas->id}}" class="btn btn-icon btn-danger confirmation"
                    data-toggle="tooltip" data-placement="top" title="Hapus">
                    <form style="display: none" action="{{route('vacancy.delete.company', $datas->id)}}" method="POST"
                      id="hapus{{$datas->id}}">
                      @csrf
                      @method('DELETE')
                    </form>
                    <i class="fas fa-times"></i>
                  </a>
                </td>
              </tr>
              @endforeach
              @else
              <tr>
                <td><h4>No data to show</h4></td>
              </tr>
              @endif

            </tbody>
          </table>

          {{$data->links()}}

        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@push('script-add')
<script>
"use strict";
$(".confirmation").click(function(e) {
  var id = e.target.dataset.id;
  Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
    if (result.value) {
      Swal.fire(
        'Deleted!',
        'Your file has been deleted.',
        'success'
      )
      $(`#hapus${id}`).submit();
    }
  });
});
</script>
@endpush