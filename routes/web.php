<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * 
 * Users
 * 
 */
Route::get('/', 'BaseController@home')->name('home');
Route::get('/my-profile', 'BaseController@profile')->name('profile');
Route::get('/about', 'BaseController@about')->name('about');
Route::get('/schedule', 'BaseController@schedule')->name('schedule');
Route::get('/faq', 'BaseController@faq')->name('faq');
Route::get('/contact', 'BaseController@contact')->name('contact');

// Map
Route::prefix('map')->group(function () {
  Route::get('/', 'BaseController@map')->name('map');
  Route::get('/{id}', 'BaseController@mapDetail')->name('map.detail');
});

Route::middleware('auth')->group(function () {
  // Apply
  Route::prefix('apply')->group(function () {
    Route::get('/{id}', 'BaseController@viewApply')->name('view.apply');
    Route::post('/{id}', 'BaseController@apply')->name('apply');
    Route::delete('/{id}/delete', 'BaseController@applyDelete')->name('applyDelete');
  });
});

// Authentication
Route::prefix('register')->group(function () {
  Route::get('/member', 'User\AuthController@viewRegister')->name('view.register');
  Route::get('/general', 'User\AuthController@viewRegisterGeneral')->name('view.register.general');
  Route::post('/', 'User\AuthController@register')->name('register');
  Route::post('/verify', 'User\AuthController@verifyEmail')->name('verify.email');
});

Route::prefix('forgot-password')->group(function () {
  Route::get('/', 'User\AuthController@viewForgotPassword')->name('view.forgot.password');
  Route::post('/send-otp', 'User\AuthController@sendOtpForgotPassword')->name('forgot.password.send.otp');
  Route::post('/verification', 'User\AuthController@verificationForgotPassword')->name('forgot.password.verification');
  Route::post('/reset', 'User\AuthController@resetForgotPassword')->name('forgot.password.reset');
});

Route::get('/login', 'User\AuthController@viewLogin')->name('view.login');
Route::post('/login', 'User\AuthController@login')->name('login');
Route::post('/logout', 'User\AuthController@logout')->name('logout');



/**
 * 
 * Admin
 * 
 */
Route::prefix('admin')->group(function () {
  // Company
  Route::prefix('company')->group(function () {
    Route::get('/login', 'Admin\AuthController@viewLoginCompany')->name('view.login.company');
    Route::post('/login', 'Admin\AuthController@loginCompany')->name('login.company');
    
    Route::middleware('auth:admin_company')->group(function () {
      Route::post('/logout', 'Admin\AuthController@logoutCompany')->name('logout.company');

      // Feature
      Route::get('/dashboard', 'Admin\HomeController@dashboardCompany')->name('dashboard.company');
      Route::get('/profile', 'Company\ProfilController@detail')->name('detail.company');
      Route::get('/preview/booth', 'Company\ProfilController@booth')->name('booth.company');

      Route::prefix('users-apply')->group(function () {
        Route::get('/', 'Company\UserApplyController@index')->name('usersApply.company');
        Route::get('/export', 'Company\UserApplyController@export')->name('usersApply.export.company');
      });

      // Vacancy
      Route::prefix('vacancy')->group(function () {
        Route::get('/', 'Company\CompanyVacancyController@index')->name('vacancy.company');
        Route::get('/add', 'Company\CompanyVacancyController@add')->name('vacancy.view.add.company');
        Route::post('/', 'Company\CompanyVacancyController@processAdd')->name('vacancy.add.company');
        Route::get('/{id}', 'Company\CompanyVacancyController@edit')->name('vacancy.edit.company');
        Route::put('/{id}/update', 'Company\CompanyVacancyController@update')->name('vacancy.update.company');
        Route::get('/{id}/detail', 'Company\CompanyVacancyController@detail')->name('vacancy.detail.company');
        Route::delete('/{id}', 'Company\CompanyVacancyController@delete')->name('vacancy.delete.company');
      });

      // About Company
      Route::get('/{id}/detail', 'Admin\CompanyController@detail')->name('detail.company.admin');
      Route::put('/{id}/update', 'Admin\CompanyController@updateCompanyStand')->name('updateCompanyStand.company.admin');
      Route::put('/{id}/updatePoster', 'Admin\CompanyController@updateCompanyStandPoster')->name('updateCompanyStandPoster.company.admin');
      Route::post('/{id}/poster', 'Admin\CompanyController@deletePoster')->name('deleteCompanyStandPoster.company.admin');
    });
  });

  // Authentication
  Route::get('/login', 'Admin\AuthController@viewLogin')->name('view.login.admin');
  Route::post('/login', 'Admin\AuthController@login')->name('login.admin');

  Route::middleware('auth:admin')->group(function () {
    Route::post('/logout', 'Admin\AuthController@logout')->name('logout.admin');

    // Feature
    Route::get('/dashboard', 'Admin\HomeController@dashboard')->name('dashboard.admin');

    // Users
    Route::prefix('users')->group(function () {
      Route::get('/', 'Admin\UserController@index')->name('users.admin');
      Route::get('/export-excel', 'Admin\UserController@export')->name('users.admin.export');
      Route::get('/add', 'Admin\UserController@add')->name('users.view.add.admin');
      Route::post('/', 'Admin\UserController@processAdd')->name('users.add.admin')->middleware('log');
      Route::get('/{id}', 'Admin\UserController@edit')->name('users.edit.admin');
      Route::put('/{id}/update', 'Admin\UserController@update')->name('users.update.admin')->middleware('log');
      Route::get('/{id}/detail', 'Admin\UserController@detail')->name('users.detail.admin');
      Route::delete('/{id}', 'Admin\UserController@delete')->name('users.delete.admin')->middleware('log');
    });

    // Company
    Route::prefix('companies')->group(function () {
      Route::get('/', 'Admin\CompanyController@index')->name('company.admin');
      Route::get('/add', 'Admin\CompanyController@add')->name('company.view.add.admin');
      Route::post('/', 'Admin\CompanyController@processAdd')->name('company.add.admin')->middleware('log');
      Route::get('/{id}', 'Admin\CompanyController@edit')->name('company.edit.admin');
      Route::put('/{id}/update', 'Admin\CompanyController@update')->name('company.update.admin')->middleware('log');
      Route::put('/{id}/updateStatus', 'Admin\CompanyController@updateStatus')->name('updateStatus.company.admin')->middleware('log');
      Route::get('/{id}/detail', 'Admin\CompanyController@detail')->name('company.detail.admin');
      Route::delete('/{id}', 'Admin\CompanyController@delete')->name('company.delete.admin')->middleware('log');
      Route::get('/{id}/detail/booth', 'Admin\CompanyController@booth')->name('booth.company.admin');
      Route::put('/{id}/updateStand', 'Admin\CompanyController@updateCompanyStand')->name('updateCompanyStand.admin')->middleware('log');;
      Route::put('/{id}/updatePoster', 'Admin\CompanyController@updateCompanyStandPoster')->name('updateCompanyStandPoster.admin')->middleware('log');;
      Route::post('/{id}/poster', 'Admin\CompanyController@deletePoster')->name('deleteCompanyStandPoster.admin')->middleware('log');;
    });

    // Users Apply
    Route::prefix('user-apply')->group(function () {
      Route::get('/', 'Admin\UserApplyController@index')->name('usersApply.admin');
      Route::get('/export', 'Admin\UserApplyController@exportAllCompany')->name('applyAllCompany.export.admin');

      // By Company
      Route::get('/{id}', 'Admin\UserApplyController@detail')->name('usersApply.detail.admin');
      Route::get('/{id}/export', 'Admin\UserApplyController@exportByCompany')->name('applyByCompany.export.admin');
    });

    // Visitor Report
    Route::prefix('visitor-report')->group(function () {
      Route::get('/', 'Admin\VisitorReportController@exportVisitorReport')->name('visitorReport.admin');
      Route::get('/company', 'Admin\VisitorReportController@exportVisitorCompanyReport')->name('visitorCompanyReport.admin');
    });

    // Web Setting App
    Route::prefix('setting-app')->group(function () {
      Route::get('/', 'Admin\SettingAppController@index')->name('settingApp.admin');
      Route::put('/update', 'Admin\SettingAppController@update')->name('settingApp.update.admin')->middleware('log');
    });

    Route::prefix('log')->group(function () {
      Route::get('/', 'Admin\LogController@view')->name('view.log');
    });
  });

  Route::prefix('company')->group(function () {
    Route::put('/{id}/updateAdmin', 'Admin\CompanyController@updateCompanyAdmin')->name('updateCompanyAdmin.company.admin')->middleware('log');;
  });
});