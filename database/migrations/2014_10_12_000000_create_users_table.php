<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('email')->unique();
            $table->string('name');
            $table->string('password');
            $table->string('student_number', 20);
            $table->enum('gender', ['M', 'F']);
            $table->dateTime('birth');
            $table->text('address');
            $table->string('phone_number', 20);
            $table->string('graduate_year', 20);
            $table->string('study_level');
            $table->enum('type', ['FTUI', 'NON']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
