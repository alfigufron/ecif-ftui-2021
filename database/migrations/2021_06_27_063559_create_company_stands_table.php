<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyStandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_stands', function (Blueprint $table) {
            $table->id();
            $table->text('video')->nullable();;
            $table->text('webinar_poster')->nullable();;
            $table->text('recruitment_poster')->nullable();;
            $table->text('right_side_addition')
                ->nullable();
            $table->text('left_side_addition')
                ->nullable();
            $table->text('about_us')
                ->nullable();
            $table->text('career')
                ->nullable();
            $table->text('internship')
                ->nullable();
            $table->text('twitter')
                ->nullable();
            $table->text('facebook')
                ->nullable();
            $table->text('linkedin')
                ->nullable();
            $table->text('instagram')
                ->nullable();
            $table->text('youtube')
                ->nullable();
            $table->enum('status', ['0', '1'])->nullable();
            $table->foreignId('company_id')
                ->constrained()
                ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_stands');
    }
}