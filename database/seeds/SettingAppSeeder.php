<?php

use App\Models\SettingApp;
use Illuminate\Database\Seeder;

class SettingAppSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SettingApp::create([
            'url_home' => null,
            'about' => null,
            'schedule' => null,
        ]);
    }
}