<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

use App\Models\User;
use App\Models\UserFaculty;
use App\Models\UserNonFaculty;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $universities = [
            'Universitas Gadjah Mada',
            'Universitas Guna Darma',
            'Institut Teknologi Bandung',
        ];

        $study_levels = [
            'bachelor',
            'profession',
            'magister',
            'doctor',
        ];

        $faker = Faker::create();

        $user_faculty = 0;
        $user_non_faculty = 0;

        for ($i = 1; $i <= 10; $i++) {
            $type = $faker->randomElement(['FTUI', 'NON']);

            $user = User::create([
                'email' => "user_$i@mail.com",
                'name' => $faker->name,
                'password' => bcrypt('12345678'),
                'student_number' => "256820000$i",
                'gender' => $faker->randomElement(['M', 'F']),
                'birth' => '1999-02-12',
                'address' => $faker->streetAddress,
                'phone_number' => "08127788{$faker->randomNumber(4, true)}",
                'graduate_year' => $faker->year(),
                'study_level' => $faker->randomElement($study_levels),
                'type' => $type
            ]);

            if ($type === 'FTUI') {
                $user_faculty++;

                UserFaculty::create([
                    'departement' => "Departement $user_faculty",
                    'user_id' => $user->id
                ]);
            } else {
                $user_non_faculty++;

                UserNonFaculty::create([
                    'university' => $faker->randomElement($universities),
                    'faculty' => "Faculty $user_non_faculty",
                    'faculty_type' => $faker->randomElement(['FT', 'NON']),
                    'user_id' => $user->id
                ]);
            }
        }
    }
}
