<?php

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

use App\Models\Admin;
use App\Models\AdminCompany;
use App\Models\Company;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $faker = Faker::create();

        for ($i = 1; $i <= 3; $i++) {
            Admin::create([
                'email' => "admin_$i@mail.com",
                'name' => $faker->name(),
                'password' => bcrypt('12345678')
            ]);

            $company = Company::create([
                'name' => "Company $i",
                'logo' => 'http://www.webdesainid.net/wp-content/uploads/2015/09/3050613-inline-i-2-googles-new-logo-copy.png',
                'type' => 'SILVER'
            ]);
            
            AdminCompany::create([
                'email' => "admin_company_$i@mail.com",
                'name' => $faker->name(),
                'password' => bcrypt('12345678'),
                'company_id' => $company->id
            ]);
        }
    }
}
