<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;

class FacultyUserSheet implements FromCollection, WithHeadings, WithTitle, WithMapping {

  public function collection() {
    $user = User::with('user_faculty')
      ->where('type', 'FTUI',)
      ->get();

    return $user;
  }

  public function headings(): array {
    return [
      'Email',
      'Name',
      'Student Number',
      'Gender',
      'Birth',
      'Address',
      'Phone Number',
      'Graduate Year',
      'Study Level',
      'Departement',
    ];
  }

  public function map($data): array {
    return [
      $data->email,
      $data->name,
      $data->student_number,
      $data->gender,
      $data->birth,
      $data->address,
      $data->phone_number,
      $data->graduate_year,
      $data->study_level,
      $data->user_faculty->departement,
    ];
  }

  public function title(): string {
    return "User FTUI";
  }
}