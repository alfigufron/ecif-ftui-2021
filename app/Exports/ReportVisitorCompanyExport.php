<?php

namespace App\Exports;

use App\Models\Company;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ReportVisitorCompanyExport implements WithMultipleSheets {
    public function sheets(): array {
        $sheets = [];
        $companies = Company::with('visitors.user')
            ->get();

        foreach ($companies as $company) {
            array_push($sheets, new CompanyVisitor($company));
        }

        return $sheets;
    }
}
