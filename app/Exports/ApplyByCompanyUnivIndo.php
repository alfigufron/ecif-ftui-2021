<?php

namespace App\Exports;

use App\Models\UserApplication;
use App\Models\Company;
use App\Models\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithStyles;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithMapping;

class ApplyByCompanyUnivIndo implements FromCollection, WithHeadings, WithTitle
{
    protected $id, $companyName;

    public function __construct($id, $companyName)
    {
        $this->id = $id;
        $this->companyName = $companyName;
    }

    public function collection()
    {
        $data = UserApplication::with('user', 'company', 'user_faculty', 'user_application_document', 'company_vacancy')
            ->where('company_id', $this->id)
            ->whereHas('user', function ($query) {
                return $query->where('type', 'FTUI');
            })
            ->get();

        return $data;
    }

    public function headings(): array
    {
        return [
            'Student Number',
            'User Name',
            'Email',
            'Gender',
            'Birth',
            'Phone Number',
            'Address',
            'Graduate Year',
            'Study Level',
            'Type',
            'Departement',
            'Vacancy',
            'Document'
        ];
    }

    public function prepareRows($rows): array
    {
        return array_map(function ($data) {
            $fields = [
                $data->user->student_number,
                $data->user->name,
                $data->user->email,
                $data->user->gender,
                $data->user->birth,
                $data->user->phone_number,
                $data->user->address,
                $data->user->graduate_year,
                $data->user->study_level,
                $data->user->type,
                $data->user()->first()->user_faculty()->first()->departement,
                $data->company_vacancy->name,
                App::make('url')->to('/assets/user/documents') . '/' . $data->user_application_document->document
            ];
            // return $data;
            return $fields;
        }, $rows);
    }

    public function title(): string
    {
        return $this->companyName . ": FTUI";
    }
}