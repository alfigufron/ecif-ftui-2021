<?php

namespace App\Exports;

use App\Models\UserApplication;
use Maatwebsite\Excel\Concerns\FromCollection;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ApplyByCompany implements FromArray, WithMultipleSheets
{
    protected $sheets;

    public function __construct(array $sheets)
    {
        $this->sheets = $sheets;
    }

    public function array(): array
    {
        return $this->sheets;
    }

    public function sheets(): array
    {
        $sheets = [];

        foreach ($this->sheets as $sh) {
            $companyId = $sh->id;
            $companyName = $sh->name;
            array_push($sheets, new ApplyByCompanyUnivIndo($companyId, $companyName));
            array_push($sheets, new ApplyByCompanyUnivGeneral($companyId, $companyName));
        }

        return $sheets;
    }
}