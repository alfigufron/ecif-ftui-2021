<?php

namespace App\Exports;

use App\Models\Company;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ReportVisitorExport implements FromCollection, WithHeadings, WithMapping {
    public function collection() {
        $companies = Company::with('visitors')->get();

        return $companies;
    }

    public function headings(): array {
        return [
            "Company Name",
            "Total Visitors"
        ];
    }

    public function map($data): array {
        $total = ($data->visitors->count()) ? "{$data->visitors->count()}" : '0';

        return [
            $data->name,
            $total,
        ];
    }
}
