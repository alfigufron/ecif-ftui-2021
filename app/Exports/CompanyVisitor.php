<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;

class CompanyVisitor implements WithHeadings, WithTitle, FromCollection {
    protected $company;

    public function __construct($company) {
        $this->company = $company;
    } 

    public function collection() {
        $data = $this->company->visitors;

        return $data;
    }

    public function headings(): array {
        return [
            "Student Number",
            "User Name",
            "Email",
            "Gender",
            "Birth",
            "Phone Number",
            "Address",
        ];
    }

    public function prepareRows($rows): array {
        return array_map(function ($data) {
            $fields = [
                $data->user->student_number,
                $data->user->name,
                $data->user->email,
                $data->user->gender,
                $data->user->birth,
                $data->user->phone_number,
                $data->user->address,
            ];

            return $fields;
        }, $rows);
    }

    public function title(): string {
        return $this->company->name;
    }
}
