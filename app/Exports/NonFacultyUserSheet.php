<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;

class NonFacultyUserSheet implements FromCollection, WithHeadings, WithTitle, WithMapping {

  public function collection() {
    $user = User::with('user_non_faculty')
      ->where('type', 'NON',)
      ->get();

    return $user;
  }

  public function headings(): array {
    return [
      'Email',
      'Name',
      'Gender',
      'Birth',
      'Address',
      'Phone Number',
      'Graduate Year',
      'Study Level',
      'University',
      'Faculty',
      'Faculty Type',
    ];
  }

  public function map($data): array {
    return [
      $data->email,
      $data->name,
      $data->gender,
      $data->birth,
      $data->address,
      $data->phone_number,
      $data->graduate_year,
      $data->study_level,
      $data->user_non_faculty->university,
      $data->user_non_faculty->faculty,
      $data->user_non_faculty->faculty_type,
    ];
  }

  public function title(): string {
    return "User Non FTUI";
  }
}