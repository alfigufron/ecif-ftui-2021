<?php

namespace App\Utils;

use App\Models\Log;

class Logger {
  public static function getAll($limit = 20) {
    $data = Log::limit($limit)->latest()->get();
    
    return $data->reverse();
  }

  public static function create($message) {
    Log::create(['log' => $message]);
  }
}