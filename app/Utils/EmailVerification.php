<?php

namespace App\Utils;

use App\Helpers\Generate;
use App\Mail\ForgotPassword;
use App\Mail\Register;
use App\Models\EmailVerification as ModelsEmailVerification;
use App\Models\ResetPassword;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class EmailVerification {
  public static function otp_check($email, $otp, $auth = 'register') {
    if ($auth === 'register')
      $otp_data = ModelsEmailVerification::where([
        ['email', $email],
        ['otp', $otp]
      ])->first();

    if ($auth === 'forgot-password')
      $otp_data = ResetPassword::where([
        ['email', $email],
        ['otp', $otp]
      ])->first();

    if (!$otp_data)
      return 'OTP Invalid';

    $time_created = new Carbon($otp_data->created_at);
    $time_expired = $time_created->addMinute(5);
    $time_now = Carbon::now();

    $diff = $time_now->diffInMinutes($time_expired, false);

    if ($diff <= 0)
      return 'OTP Expired';

    $otp_data->delete();

    return 1;
  }

  public static function create_verification($email, $auth = 'register') {
    $otp = Generate::otp();

    $by_email = ['email' => $email];
    $data_update = [
        'email' => $email,
        'otp' => $otp,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
    ];

    if ($auth === 'register') {
      ModelsEmailVerification::updateOrInsert($by_email, $data_update);
      Mail::to($email)->send(new Register($otp));
    }
    
    if ($auth === 'forgot-password') {
      ResetPassword::updateOrInsert($by_email, $data_update);
      Mail::to($email)->send(new ForgotPassword($otp));
    }
  }
}