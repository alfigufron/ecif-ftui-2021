<?php

use Illuminate\Support\Facades\Route;

function set_active($route)
{
  if (Request::is($route)) {
    return 'active';
  }
}