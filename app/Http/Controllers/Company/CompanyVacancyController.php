<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use App\Models\CompanyVacancy;
use App\Models\Company;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

class CompanyVacancyController extends Controller
{
    // Validation
    private function _validation(Request $req)
    {
        $validation = $req->validate(
            [
                'name' => 'required|min:3',
                'description' => 'required|min:3',
            ],
            [
                'name.required' => 'Must be filled',
                'name.min' => 'Minimum 3 digit',
                'description.required' => 'Must be filled',
                'description.min' => 'Minimum 3 digit',
            ]
        );
    }

    public function index()
    {
        $id = Auth::guard('admin_company')->user()->company_id;

        $data = CompanyVacancy::where('company_id', $id)->paginate(10);

        return view("admin.company.vacancy.data", ["data" => $data]);
    }

    public function add()
    {
        return view('admin.company.vacancy.add');
    }

    public function processAdd(Request $req)
    {
        $id = Auth::guard('admin_company')->user()->company_id;

        $this->_validation($req);

        CompanyVacancy::create([
            'name' => $req->name,
            'description' => $req->description,
            'company_id' => $id,
        ]);

        return redirect()->route('vacancy.company')->with('message', 'Vacancy data saved!');
    }

    public function edit($id)
    {
        // $id = Auth::guard('admin_company')->user()->company_id;

        $data = CompanyVacancy::findOrFail($id);

        return view("admin.company.vacancy.edit", ["data" => $data]);
    }

    public function update(Request $req, $id)
    {
        $this->_validation($req);

        CompanyVacancy::findOrFail($id)->update([
            'name' => $req->name,
            'description' => $req->description,
        ]);

        return redirect()->route('vacancy.company')->with('message', 'Vacancy data saved!');
    }

    public function delete($id)
    {
        CompanyVacancy::findOrFail($id)->delete();
        return redirect()->route('vacancy.company')->with('message', 'Vacancy data deleted!');
    }
}