<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Exports\ApplyByCompany;
use App\Models\UserApplication;
use App\Models\Company;

use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

class UserApplyController extends Controller
{
    public function index()
    {
        $id = Auth::guard('admin_company')->user()->company_id;

        $data = UserApplication::with('user', 'company_vacancy')->where('company_id', $id)->paginate(15);

        return view('admin.apply.company.data', ['data' => $data]);
    }

    public function export()
    {
        $id = Auth::guard('admin_company')->user()->company_id;

        $data = Company::with('user_application')->where('id', $id)->get();

        $values = array();
        for ($x = 0; $x < count($data); $x++) {
            $values[] = $data[$x];
        }

        $export = new ApplyByCompany($values);

        return Excel::download($export, 'applyByCompany.xlsx');
    }
}