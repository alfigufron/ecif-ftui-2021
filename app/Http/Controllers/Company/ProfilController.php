<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\Admin;
use App\Models\AdminCompany;
use App\Models\Company;
use App\Models\CompanyStand;

class ProfilController extends Controller
{
    public function detail()
    {
        $elq = new Company;

        $id = Auth::guard('admin_company')->user()->company_id;

        $data = $elq->with('admin')->with('company_stand')->with('user_application')->findOrFail($id);

        // return $data;
        return view('admin.companies.detail', ['data' => $data]);
    }

    public function booth()
    {
        $id = Auth::guard('admin_company')->user()->company_id;

        $data = Company::findOrFail($id);

        return view('user.detailMap', ['data' => $data]);
    }
}