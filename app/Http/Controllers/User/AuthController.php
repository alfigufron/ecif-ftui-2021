<?php

namespace App\Http\Controllers\User;

use App\Helpers\Generate as HelpersGenerate;
use App\Http\Controllers\Controller;
use App\Mail\Register;
use App\Models\EmailVerification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Models\User;
use App\Models\UserFaculty;
use App\Models\UserNonFaculty;
use App\Utils\EmailVerification as UtilsEmailVerification;
use Carbon\Carbon;
use Generate;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller {
    /**
     * 
     * View FTUI User Register
     * 
     */
    public function viewRegister() {

        // Authentication Check
        if (Auth::check())
            return redirect()->route('home');

        return view('user.auth.register')->with(['menu' => 'register']);
    }

    /**
     * 
     * View General User Register
     * 
     */
    public function viewRegisterGeneral() {

        // Authentication Check
        if (Auth::check())
            return redirect()->route('home');

        return view('user.auth.registerGeneral')->with(['menu' => 'register']);
    }

    /**
     * 
     * Email Verification
     * 
     */
    public function verifyEmail(Request $request) {
        $request->validate([
            'email' => 'required|email|unique:users,email'
        ]);

        UtilsEmailVerification::create_verification($request->email);

        return redirect()
            ->back()
            ->withInput()
            ->with('success', "Send OTP Successfully, Please Check your Email");
    }

    /**
     * 
     * Process FTUI & General User Register
     * 
     */
    public function register(Request $request) {

        // Validation
        $validator = [];
        if ($request->type) {
            if ($request->type === 'ftui')
                $validator = [
                    'departement' => 'required|string',
                    'student_number' => 'required|min:10|max:10|unique:mysql.users,student_number'
                ];
            if ($request->type === 'non')
                $validator = [
                    'university' => 'required|string',
                    'faculty' => 'required|string',
                    'faculty_type' => 'required|string'
                ];
        }

        $validator = array_merge(
            [
                'type' => 'required|in:ftui,non',
                'full_name' => 'required|string',
                'address' => 'required|string',
                'email' => 'required|email|unique:mysql.users,email',
                'gender' => 'required|in:M,F',
                'birth' => 'required|date',
                'phone_number' => 'required',
                'graduate_year' => 'required',
                'study_level' => 'required|string',
                'password' => 'required|min:6',
                // 'otp' => 'required|min:6'
            ],
            $validator
        );

        $request->validate($validator);

        // $otp_check = UtilsEmailVerification::otp_check($request->email, $request->otp);

        // if ($otp_check !== 1)
        //     return ($request->type === 'ftui')
        //         ? redirect()->route('view.register')
        //             ->with('error', $otp_check)->withInput()
        //         : redirect()->route('view.register.general')
        //             ->with('error', $otp_check)->withInput();

        try {
            DB::beginTransaction();

            // Create Base User
            $user = new User();
            $user->email = $request->email;
            $user->name = $request->full_name;
            $user->student_number = $request->student_number;
            $user->gender = $request->gender;
            $user->birth = $request->birth;
            $user->address = $request->address;
            $user->phone_number = $request->phone_number;
            $user->graduate_year = $request->graduate_year;
            $user->study_level = $request->study_level;
            $user->type = strtoupper($request->type);
            $user->password = bcrypt($request->password);
            $user->save();

            if ($request->type === 'ftui') {

                // Create FTUI User
                $user_faculty = new UserFaculty();
                $user_faculty->departement = $request->departement;
                $user_faculty->user_id = $user->id;
                $user_faculty->save();

                DB::commit();

                return redirect()->route('view.register')
                    ->with('success', 'Register Successfully');
            } else {

                // Create General User
                $user_non_faculty = new UserNonFaculty();
                $user_non_faculty->university = $request->university;
                $user_non_faculty->faculty = $request->faculty;
                $user_non_faculty->faculty_type = $request->faculty_type;
                $user_non_faculty->user_id = $user->id;
                $user_non_faculty->save();

                DB::commit();
                
                return redirect()->route('view.register.general')
                    ->with('success', 'Register Successfully');
            }
        } catch (\Exception $err) {
            return $err;
            return redirect()->back();
        }
    }

    /**
     * 
     * View FTUI & General User Login
     * 
     */
    public function viewLogin() {

        // Authentication Check
        if (Auth::check())
            return redirect()->route('home');

        if (Auth::guard('admin')->check())
            return redirect()->route('dashboard.admin');

        if (Auth::guard('admin_company')->check())
            return redirect()->route('dashboard.company');

        return view('user.auth.login');
    }

    /**
     * 
     * Process FTUI & General User Login
     * 
     */
    public function login(Request $request) {

        // Validation
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|string'
        ]);

        // User Authentication
        $check = Auth::attempt([
            'email' => $request->email,
            'password' => $request->password
        ]);

        if (!$check)
            return redirect()
                ->back()
                ->withInput()
                ->with('error', 'Email or password does not match');
        
        return redirect()->route('home');
    }

    /**
     * 
     * Proccess FTUI & General User Logout
     * 
     */
    public function logout() {
        Auth::logout();
        
        return redirect()->route('home');
    }

    /**
     * 
     * View Forgot Password
     * 
     */
    public function viewForgotPassword() {
        return view('user.auth.forgotPassword');
    }

    /**
     * 
     * Send OTP Forgot Password
     * 
     */
    public function sendOtpForgotPassword(Request $request) {
        $request->validate([
            'email' => 'required|email|exists:users,email'
        ]);

        UtilsEmailVerification::create_verification($request->email, 'forgot-password');

        return redirect()
            ->back()
            ->withInput()
            ->with('success', "Send OTP Successfully, Please Check your Email");
    }

    /**
     * 
     * Verification OTP Forgot Password
     * 
     */
    public function verificationForgotPassword(Request $request) {
        $request->validate([
            'email' => 'required|email',
            'otp' => 'required|min:6'
        ]);

        $otp_check = UtilsEmailVerification::otp_check($request->email, $request->otp, 'forgot-password');

        if ($otp_check !== 1)
            return redirect()->route('view.forgot.password')
                ->with('error', $otp_check)
                ->withInput();

        return view('user.auth.resetPassword')->with('email', $request->email);
    }

    public function resetForgotPassword(Request $request) {
        $request->validate([
            'email' => 'required|email|exists:users,email',
            'password' => 'required|string|confirmed'
        ]);

        $user = User::where('email', $request->email)->first();
        $user->password = bcrypt($request->password);
        $user->save();

        return redirect()->route('view.login')->with('success', "Reset Password Successfully, Please Login");
    }
}
