<?php

namespace App\Http\Controllers;

use App\Models\BoothVisitor;
use App\Models\Company;
use App\Models\CompanyVacancy;
use App\Models\User;
use App\Models\SettingApp;
use App\Models\UserApplication;
use App\Models\UserApplicationDocument;
use App\Models\UserUI;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use File;

class BaseController extends Controller
{
    protected $data;

    // Validation
    private function _validation(Request $req)
    {
        $validation = $req->validate(
            [
                'vacancy' => 'required|exists:company_vacancy,id',
                'document' => 'required|mimes:pdf,docx,png',
            ],
            [
                'document.required' => 'Must be filled',
                'document.mimes' => 'Document Must be pdf, docx, or png',
            ]
        );
    }

    public function __construct()
    {
        $this->data = SettingApp::findOrFail(1)->first();
    }
    public function home()
    {
        return view('user.home', ['data' => $this->data]);
    }

    public function about()
    {
        return view('user.about', ['data' => $this->data]);
    }

    public function schedule()
    {
        return view('user.schedule', ['data' => $this->data]);
    }

    public function map()
    {
        $data = Company::whereHas('company_stand', function ($q) {
            return $q->where('status', '=', '1');
        })->get();

        return view('user.map', ['data' => $data]);
    }

    public function faq()
    {
        return view('user.faq');
    }

    public function contact()
    {
        return view('user.contact');
    }

    public function mapDetail($id)
    {
        $user = Auth::guard('web')->user();

        if ($user) {
            $data = Company::with('company_vacancy')->findOrFail($id);
            $visited = BoothVisitor::where([
                ['company_id', $id],
                ['user_id', $user->id],
            ])->first();

            if (empty($visited))
                BoothVisitor::create([
                    'company_id' => $id,
                    'user_id' => $user->id
                ]);

            return view('user.detailMap', ['data' => $data]);
        } else {
            return view('user.auth.login');
        }
    }

    public function profile()
    {
        $id = Auth::guard('web')->user()->id;

        $data = User::with('user_faculty')
            ->with('user_non_faculty', 'user_application.company_vacancy')
            ->findOrFail($id);

        // return $data;

        return view('user.profile', ['data' => $data]);

        // return $data;
    }

    public function viewApply($id)
    {
        if (Auth::guard('admin')->check())
            return redirect()->route('dashboard.admin');

        if (Auth::guard('admin_company')->check())
            return redirect()->route('dashboard.company');
        
        $user_id = Auth::guard('web')->user()->id;

        if ($user_id) {
            $data = Company::findOrFail($id);
            
            $elq = UserApplication::Where('company_id', $id);
            $dataUser = $elq->where('user_id', $user_id)->pluck('company_vacancy_id');

            $vacancy = CompanyVacancy::where('company_id', $id)
                ->whereNotIn('id', $dataUser)
                ->get();

            $data->company_vacancy = $vacancy;

            $user = User::find($user_id);
            $cdc_member = UserUI::where('email', $user->email)->first();

            // return response()->json([
            //     'data' => $data,
            //     'data_user' => $dataUser,
            //     'vacancy' => $vacancy,
            // ]);

            if (empty($cdc_member))
                return view('user.apply', ['data' => $data]);

            if ($elq) {
                return view('user.apply', ['data' => $data, 'dataUser' => $dataUser, 'cdc_member' => 1]);
            } else {
                return view('user.apply', ['data' => $data, 'cdc_member' => 1]);
            }
        } else {
            return redirect()->route('home');
        }
    }

    public function apply(Request $req, $id)
    {
        $user_id = Auth::guard('web')->user()->id;

        $this->_validation($req);

        $file = $req->file('document');
        $name_file = time() . "_" . $file->getClientOriginalName();
        $tujuan_upload = 'assets/user/documents';
        $file->move($tujuan_upload, $name_file);

        $user_applications = UserApplication::create([
            'company_id' => $id,
            'user_id' => $user_id,
            'company_vacancy_id' => $req->vacancy
        ]);

        if ($user_applications) {
            UserApplicationDocument::create([
                'document' => $name_file,
                'user_application_id' => $user_applications->id,
            ]);
        }
        return redirect()->route('view.apply', $id)->with('message', 'Apply succeed! Thank You');
    }

    public function applyDelete($id)
    {
        $data = UserApplicationDocument::where('user_application_id', $id)->first();
        File::delete('assets/user/documents/' . $data->document);

        $UserApplicationDocument = UserApplicationDocument::where('user_application_id', $id)->delete();
        if ($UserApplicationDocument) {
            UserApplication::findOrFail($id)->delete();
        }
        return redirect()->route('map')->with('message', 'Document deleted! Please upload your document for apply');
    }
}