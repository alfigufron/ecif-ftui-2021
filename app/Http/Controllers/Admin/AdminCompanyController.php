<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AdminCompany;
use App\Models\Company;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use File;

class CompanyController extends Controller
{
    // Validation
    private function _validation(Request $req)
    {
        $validation = $req->validate(
            [
                'name' => 'required|min:3',
                'type' => 'required',
                'logo' => 'required',
            ],
            [
                'name.required' => 'Must be filled',
                'name.min' => 'Minimum 3 digit',
                'type.required' => 'Must be filled',
                'logo.required' => 'Must be filled',
            ]
        );
    }

    public function index()
    {
        $eql = new Company();
        $data = $eql->paginate(10);
        return view('admin.companies.data', ['data' => $data]);
    }

    public function add()
    {
        $lastRecord = AdminCompany::latest('id')->first();

        // dd($lastRecord);
        return view('admin.companies.add', ['lastRecord' => $lastRecord]);
    }

    public function processAdd(Request $req)
    {
        $this->_validation($req);

        AdminCompany::insert([
            'email' => $req->email,
            'name' => $req->name_admin,
            'password' => Hash::make($req['password']),
        ]);

        return redirect()->route('company.view.add.admin')->with('message', 'Data Success!');
    }

    public function edit($id)
    {
        $elq = new Company;
        $data = $elq->findOrFail($id);
        return view('admin.companies.edit', ['data' => $data]);
    }

    public function update(Request $req, $id)
    {
        $eql = new Company;
        $data = $eql->where('id', $id)->first();
        File::delete('company/' . $data->logo);
        $file = $req->file('logo');
        $name_file = time() . "_" . $file->getClientOriginalName();
        $tujuan_upload = 'company';
        $file->move($tujuan_upload, $name_file);
        $eql->where('id', $id)->update([
            'name' => $req->name,
            'logo' => $name_file,
            'type' => $req->type,
        ]);
        return redirect()->route('company.admin')->with('message', 'Data Success!');
    }

    public function detail($id)
    {
        $elq = new Company;
        $data = $elq->findOrFail($id);
        return view('admin.companies.detail', ['data' => $data]);
    }

    public function delete($id)
    {
        $data = Company::findOrFail($id);
        File::delete('company/' . $data->logo);
        $data->delete();
        return redirect()->route('company.admin')->with('message', 'Data Deleted!');
    }
}