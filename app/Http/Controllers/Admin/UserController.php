<?php

namespace App\Http\Controllers\Admin;

use App\Exports\UserExport;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserFaculty;
use App\Models\UserNonFaculty;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{
    // Validation
    private function _validation(Request $req)
    {
        $validation = $req->validate(
            [
                'name' => 'required|min:3',
                'email' => 'required',
                'gender' => 'required',
                'study_level' => 'required',
                'type' => 'required',
                'graduate_year' => 'required|max:4',
                'student_number' => 'required',
                'birth' => 'required',
                'phone_number' => 'required',
                'address' => 'required',
                // 'departement' => 'required',
                // 'university' => 'required|min:5',
                // 'faculty' => 'required',
                // 'faculty_type' => 'required',
            ],
            [
                'name.required' => 'Must be filled',
                'name.min' => 'Minimum 3 digit',
                'email.required' => 'Must be filled',
                'gender.required' => 'Must be filled',
                'study_level.required' => 'Must be filled',
                'study_level.required' => 'Must be filled',
                'type.required' => 'Must be filled',
                'graduate_year.required' => 'Must be filled',
                'graduate_year.required' => 'Maximum 4 digit',
                'student_number.required' => 'Must be filled',
                'birth.required' => 'Must be filled',
                'phone_number.required' => 'Must be filled',
                'address.required' => 'Must be filled',
                // 'departement.required' => 'Must be filled',
                // 'university.required' => 'Must be filled',
                // 'university.min' => 'Minimum 3 digit',
                // 'faculty.required' => 'Must be filled',
                // 'faculty_type.required' => 'Must be filled',
            ]
        );
    }

    public function index(Request $req)
    {
        $elq = new User;
        $params = array();
        $search = "";

        if ($req->search) {
            $search = $req->search;
            $params['search'] = $search;
        }

        $data = User::where('name', 'like', "%$search%")->paginate(10);

        return view('admin.users.data', ['data' => $data, 'params' => $params]);
    }

    public function add()
    {
        return view('admin.users.add');
    }

    public function processAdd(Request $req)
    {
        $this->_validation($req);

        if ($req->departement && $req->university) {
            return redirect()->route('users.admin')->with('message', 'Add Failed! Choose one, between Faculty Or Non Faculty');
        } else if ($req->departement) {
            $userinfo = User::create([
                'email' => $req->email,
                'name' => $req->name,
                'gender' => $req->gender,
                'study_level' => $req->study_level,
                'type' => $req->type,
                'phone_number' => $req->phone_number,
                'graduate_year' => $req->graduate_year,
                'student_number' => $req->student_number,
                'birth' => $req->birth,
                'type' => $req->type,
                'address' => $req->address,
                'password' => Hash::make($req['password']),
            ]);

            UserFaculty::create([
                'departement' => $req->departement,
                'user_id' => $userinfo->id
            ]);
        } else if ($req->university) {
            $userinfo = User::create([
                'email' => $req->email,
                'name' => $req->name,
                'gender' => $req->gender,
                'study_level' => $req->study_level,
                'type' => $req->type,
                'phone_number' => $req->phone_number,
                'graduate_year' => $req->graduate_year,
                'student_number' => $req->student_number,
                'birth' => $req->birth,
                'type' => $req->type,
                'address' => $req->address,
                'password' => Hash::make($req['password']),
            ]);

            UserNonFaculty::create([
                'university' => $req->university,
                'faculty' => $req->faculty,
                'faculty_type' => $req->faculty_type,
                'user_id' => $userinfo->id
            ]);
        }

        return redirect()->route('users.admin')->with('message', 'User data saved!');
    }

    public function edit($id)
    {
        $elq = new User;
        $data = $elq->findOrFail($id);

        if (UserFaculty::where('user_id', $id)->first()) {
            $level = "userFaculty";
            $detail = UserFaculty::where('user_id', $id)->first();
        } else if (UserNonFaculty::where('user_id', $id)) {
            $level = "userNotFaculty";
            $detail = UserNonFaculty::where('user_id', $id)->first();
        }

        return view('admin.users.edit', ['data' => $data, 'level' => $level, 'detail' => $detail]);
    }

    public function update(Request $req, $id)
    {
        $elq = new User;

        $data = $elq->findOrFail($id);

        if (UserFaculty::where('user_id', $id)->first()) {
            $level = "userFaculty";
            $detail = UserFaculty::where('user_id', $id)->first();
        } else if (UserNonFaculty::where('user_id', $id)) {
            $level = "userNotFaculty";
            $detail = UserNonFaculty::where('user_id', $id)->first();
        }

        if ($req->departement && $req->university) {
            return redirect()->route('users.detail.admin', [$id, 'data' => $data, 'level' => $level, 'detail' => $detail])->with('message', 'Update Failed! Choose one, between Faculty Or Non Faculty');
        } else if ($req->departement) {
            $data = [
                'email' => $req->email,
                'name' => $req->name,
                'gender' => $req->gender,
                'study_level' => $req->study_level,
                'type' => $req->type,
                'phone_number' => $req->phone_number,
                'graduate_year' => $req->graduate_year,
                'student_number' => $req->student_number,
                'birth' => $req->birth,
                'type' => $req->type,
                'address' => $req->address,
            ];

            if ($req->password)
                $data['password'] = Hash::make($req->password);

            $elq->where('id', $id)->update($data);
            if (UserFaculty::where('user_id', $id)->first()) {
                UserFaculty::where('user_id', $id)->first()->update([
                    'departement' => $req->departement,
                ]);
            } else if (UserNonFaculty::where('user_id', $id)) {
                $userNotFacul = UserNonFaculty::where('user_id', $id)->first();
                $userNotFacul->delete();

                UserFaculty::create([
                    'departement' => $req->departement,
                    'user_id' => $id
                ]);
            }
        } else if ($req->university) {
            $data = [
                'email' => $req->email,
                'name' => $req->name,
                'gender' => $req->gender,
                'study_level' => $req->study_level,
                'type' => $req->type,
                'phone_number' => $req->phone_number,
                'graduate_year' => $req->graduate_year,
                'student_number' => $req->student_number,
                'birth' => $req->birth,
                'type' => $req->type,
                'address' => $req->address,
            ];

            if ($req->password)
                $data['password'] = Hash::make($req->password);

            $elq->where('id', $id)->update($data);
            if (UserFaculty::where('user_id', $id)->first()) {
                $userFacul = UserFaculty::where('user_id', $id)->first();
                $userFacul->delete();

                UserNonFaculty::create([
                    'university' => $req->university,
                    'faculty' => $req->faculty,
                    'faculty_type' => $req->faculty_type,
                    'user_id' => $id
                ]);
            } else if (UserNonFaculty::where('user_id', $id)) {
                $updated = UserNonFaculty::where('user_id', $id)->first();

                $updated->university = $req->university;
                $updated->faculty = $req->faculty;
                $updated->faculty_type = $req->faculty_type;
                $updated->save();
            }
        }

        return redirect()->route('users.detail.admin', [$id, 'data' => $data, 'level' => $level, 'detail' => $detail])->with('message', 'User data saved!');
    }

    public function detail($id)
    {
        $elq = new User;

        $data = $elq->with('user_application', 'user_faculty', 'user_non_faculty')->findOrFail($id);
        $data->birth = Carbon::parse($data->birth)->format('Y-m-d');

        if (UserFaculty::where('user_id', $id)->first()) {
            $level = "userFaculty";
            $detail = UserFaculty::where('user_id', $id)->first();
        } else if (UserNonFaculty::where('user_id', $id)) {
            $level = "userNotFaculty";
            $detail = UserNonFaculty::where('user_id', $id)->first();
        }

        return view('admin.users.detail', ['data' => $data, 'level' => $level, 'detail' => $detail]);
    }

    public function delete($id)
    {
        $data = User::findOrFail($id);

        if (UserFaculty::where('user_id', $id)->first()) {
            $userFacul = UserFaculty::where('user_id', $id)->first();
            $userFacul->delete();
        } else if (UserNonFaculty::where('user_id', $id)) {
            $userNotFacul = UserNonFaculty::where('user_id', $id)->first();
            $userNotFacul->delete();
        }

        $data->delete();

        return redirect()->route('users.admin')->with('message', 'User data deleted!');
    }

    public function export() {
        return Excel::download(new UserExport, 'users.xlsx');
    }
}