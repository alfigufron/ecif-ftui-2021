<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use App\Models\AdminCompany;
use App\Models\Company;
use App\Models\CompanyStand;
use App\Utils\Logger;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use File;
use Illuminate\Support\Facades\Auth;

class CompanyController extends Controller
{

    // Validation
    private function _validation(Request $req)
    {
        $validation = $req->validate(
            [
                'name' => 'required|min:3',
                'type' => 'required',
                'logo' => 'required',
            ],
            [
                'name.required' => 'Must be filled',
                'name.min' => 'Minimum 3 digit',
                'type.required' => 'Must be filled',
                'logo.required' => 'Must be filled',
            ]
        );
    }


    public function index()
    {
        $eql = new Company();
        $data = $eql->paginate(10);
        return view('admin.companies.data', ['data' => $data]);
    }

    public function add()
    {
        $lastRecord = AdminCompany::latest('id')->first();

        // dd($lastRecord);
        return view('admin.companies.add', ['lastRecord' => $lastRecord]);
    }

    public function processAdd(Request $req)
    {
        $this->_validation($req);

        $file = $req->file('logo');
        $name_file = time() . "_" . $file->getClientOriginalName();
        $tujuan_upload = 'company';
        $file->move($tujuan_upload, $name_file);

        $company = Company::create([
            'name' => $req->name,
            'description' => $req->description,
            'logo' => $name_file,
            'type' => $req->type,
        ]);;


        if ($req) {
            AdminCompany::create([
                'email' => $req->email,
                'name' => $req->name_admin,
                'password' => Hash::make($req['password']),
                'company_id' => $company->id
            ]);

            CompanyStand::create([
                'status' => '0',
                'company_id' => $company->id
            ]);
        }

        $user_log = Auth::guard('admin')->user()->name;

        return redirect()->route('company.admin')->with('message', 'Company data saved!');
    }

    public function edit($id)
    {
        $elq = new Company;
        $data = $elq->findOrFail($id);
        return view('admin.companies.edit', ['data' => $data]);
    }

    public function update(Request $req, $id)
    {
        $eql = new Company;
        $data = $eql->where('id', $id)->first();

        $update_data = [
            'name' => $req->name,
            'type' => $req->type,
            'description' => $req->description,
        ];

        if ($req->file('logo')) {
            File::delete('company/' . $data->logo);
            $file = $req->file('logo');
            $name_file = time() . "_" . $file->getClientOriginalName();
            $tujuan_upload = 'company';
            $file->move($tujuan_upload, $name_file);
            $update_data['logo'] = $name_file;
        }


        $eql->where('id', $id)->update($update_data);

        return redirect()->route('company.admin')->with('message', 'Company data saved!');
    }

    public function detail($id)
    {
        $elq = new Company;

        $data = $elq->with('admin')->with('company_stand')->with('user_application')->findOrFail($id);

        // return $data;
        return view('admin.companies.detail', ['data' => $data]);
    }

    public function updateCompanyStand(Request $req, $id)
    {
        $status = (Auth::guard('admin')->check())
            ? '1' : '0';

        CompanyStand::where('company_id', $id)->update([
            'video' => $req->video,
            'career' => $req->career,
            'about_us' => $req->about_us,
            'internship' => $req->internship,
            'apply' => $req->apply,
            'join_webinar' => $req->join_webinar,
            'twitter' => $req->twitter,
            'youtube' => $req->youtube,
            'facebook' => $req->facebook,
            'linkedin' => $req->linkedin,
            'instagram' => $req->instagram,
            'status' => $status,
        ]);

        return (Auth::guard('admin')->check())
            ? redirect()->route('company.detail.admin', $id)->with('message', 'Company Stand data saved!')
            : redirect()->route('detail.company.admin', $id)->with('message', 'Company Stand data saved!');
    }
    public function updateCompanyStandPoster(Request $req, $id)
    {
        function random($length = 10)
        {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }

        $posters = [];
        if ($req->recruitment_poster)
            array_push($posters, $req->recruitment_poster);

        if ($req->webinar_poster)
            array_push($posters, $req->webinar_poster);

        $arr = [];

        foreach ($posters as $key => $poster) {
            $random = random(8);
            $posterNew = $random . '.' . $poster->getClientOriginalExtension();
            $poster->move(public_path('/assets/admin/fileCompanyStand'), $posterNew);

            array_push($arr, $posterNew);
        };
        
        $poster = CompanyStand::where('company_id', $id)->first();

        if ($req->recruitment_poster) {
            File::delete('assets/admin/fileCompanyStand/' . $poster->recruitment_poster);
            $poster->recruitment_poster = $arr[0];
        }
        
        if ($req->webinar_poster) {
            File::delete('assets/admin/fileCompanyStand/' . $poster->webinar_poster);
            $poster->webinar_poster = (count($arr) === 2) ? $arr[1] : $arr[0];
        }

        $poster->status = (Auth::guard('admin')->check()) ? '1' : '0';

        $poster->save();

        return (Auth::guard('admin')->check())
            ? redirect()->route('company.detail.admin', $id)->with('message', 'Company Stand data saved!')
            : redirect()->route('detail.company.admin', $id)->with('message', 'Company Stand data saved!');
    }

    public function booth($id)
    {
        $data = Company::findOrFail($id);

        return view('user.detailMap', ['data' => $data]);
    }

    public function updateCompanyAdmin(Request $req, $id)
    {
        AdminCompany::where('company_id', $id)->update([
            'email' => $req->email,
            'name' => $req->name,
            'password' => Hash::make($req['password']),
        ]);

        return redirect()->route('company.detail.admin', $id)->with('message', 'Company admin data saved!');
    }

    public function updateStatus($id)
    {
        CompanyStand::where('company_id', $id)->update([
            'status' => '1'
        ]);

        return redirect()->route('company.detail.admin', $id)->with('message', 'Status stand changed!');
    }

    public function delete($id)
    {
        $adminCompany = AdminCompany::where('company_id', $id)->first();
        $adminCompany->delete();


        $companyStand = CompanyStand::where('company_id', $id)->first();
        if ($companyStand) {
            $data = Company::findOrFail($id);
            File::delete('company/' . $data->logo);
            File::delete('assets/admin/fileCompanyStand/' . $data->recruitment_poster);
            File::delete('assets/admin/fileCompanyStand/' . $data->webinar_poster);
            $companyStand->delete();
        }

        if ($adminCompany) {
            $data = Company::findOrFail($id);
            File::delete('company/' . $data->logo);
            $data->delete();
        }

        $user_log = Auth::guard('admin')->user()->name;
        Logger::create("[$user_log] Deleted Company");

        return redirect()->route('company.admin')->with('message', 'Company data deleted!');
    }

    public function deletePoster(Request $req, $id) {
        $company_stand = CompanyStand::where('company_id', $id)->first();

        if ($req->data === 'recruitment_poster') {
            File::delete('assets/admin/fileCompanyStand/' . $company_stand->recruitment_poster);
            $company_stand->recruitment_poster = null;
        }
        
        if ($req->data === 'webinar_poster') {
            File::delete('assets/admin/fileCompanyStand/' . $company_stand->webinar_poster);
            $company_stand->webinar_poster = null;
        }

        $company_stand->status = (Auth::guard('admin')->check()) ? '1' : '0';

        $company_stand->save();

        return (Auth::guard('admin')->check())
            ? redirect()->route('company.detail.admin', $id)->with('message', 'Company Stand data saved!')
            : redirect()->route('detail.company.admin', $id)->with('message', 'Company Stand data saved!');
    }
}