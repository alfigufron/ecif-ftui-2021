<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller {
    /**
     * 
     * View Admin Login
     * 
     */
    public function viewLogin() {

        // Authentication Check
        if (Auth::guard('admin')->check() || Auth::guard('admin_company')->check())
            return redirect()->route('dashboard.admin');

        if (Auth::check())
            return redirect()->route('home');

        return view('admin/auth/loginAdmin');
    }

    /**
     * 
     * Process Admin Login
     * 
     */
    public function login(Request $request) {

        // Validation
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|string'
        ]);

        // User Authentication
        $check = Auth::guard('admin')
            ->attempt([
                'email' => $request->email,
                'password' => $request->password
            ]);

        if (!$check)
            return redirect()
                ->back()
                ->withInput()
                ->with('error', 'Email or password does not match');

        return redirect()->route('dashboard.admin');
    }

    /**
     * 
     * Process Admin Logout
     * 
     */
    public function logout() {
        Auth::guard('admin')->logout();

        return redirect()->route('login.admin');
    }
    
    /**
     * 
     * View Admin Company Login
     * 
     */
    public function viewLoginCompany() {

        // Authentication Check
        if (Auth::guard('admin_company')->check() || Auth::guard('admin')->check())
            return redirect()->route('dashboard.company');

        if (Auth::check())
            return redirect()->route('home');
        
        return view('admin/auth/loginCompanies');
    }

    /**
     * 
     * Process Admin Company Login
     * 
     */
    public function loginCompany(Request $request) {

        // Validation
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|string'
        ]);

        // User Authentication
        $check = Auth::guard('admin_company')
            ->attempt([
                'email' => $request->email,
                'password' => $request->password
            ]);

        if (!$check)
            return redirect()
                ->back()
                ->withInput()
                ->with('error', 'Email or password does not match');

        return redirect()->route('dashboard.company');
    }

    /**
     * 
     * Process Admin Logout
     * 
     */
    public function logoutCompany() {
        Auth::guard('admin_company')->logout();

        return redirect()->route('login.company');
    }
}
