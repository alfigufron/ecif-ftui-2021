<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller {
    private function viewDashboard() {
        return view('admin.home');
    }

    public function dashboard() {
        
        // Authentication Check
        $check = Auth::guard('admin')->check();

        if (!$check)
            return redirect()->route('view.login.admin');

        return $this->viewDashboard();
    }

    public function dashboardCompany() {
        
        // Authentication Check
        $check = Auth::guard('admin_company')->check();

        if (!$check)
            return redirect()->route('view.login.company');

        return $this->viewDashboard();
    }
}
