<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Log;
use App\Utils\Logger;
use Illuminate\Http\Request;

class LogController extends Controller
{
    /**
     * 
     * View Log
     * 
     */
    public function view(Request $req) {
        $limit = 20;

        if ($req->limit)
            $limit = $req->limit;

        $data = Logger::getAll($limit);
        $total = Log::all()->count();

        return view('admin.log.view', [
            'data' => $data,
            'total' => $total
        ]);
    }
}
