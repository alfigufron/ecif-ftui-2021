<?php

namespace App\Http\Controllers\Admin;

use App\Exports\ApplyAllCompany;
use App\Exports\ApplyByCompany;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\UserApplication;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class UserApplyController extends Controller
{
    public function index()
    {
        $data = Company::paginate(15);

        return view('admin.apply.data', ['data' => $data]);
    }

    public function detail($id)
    {
        $data = UserApplication::with('user', 'company_vacancy', 'company')->where('company_id', $id)->paginate(15);

        return view('admin.apply.company.data', ['data' => $data, 'id' => $id]);
        // return count($data);
    }

    public function exportAllCompany()
    {
        $data = Company::with('user_application.user')->get();

        // return $data;

        $values = array();
        for ($x = 0; $x < count($data); $x++) {
            $values[] = $data[$x];
        }

        $export = new ApplyAllCompany($values);

        return Excel::download($export, 'applyAllCompany.xlsx');
    }

    public function exportByCompany($id)
    {
        $data = Company::with('user_application')->where('id', $id)->get();

        $values = array();
        for ($x = 0; $x < count($data); $x++) {
            $values[] = $data[$x];
        }

        $export = new ApplyByCompany($values);

        return Excel::download($export, 'applyByCompany.xlsx');
    }
}