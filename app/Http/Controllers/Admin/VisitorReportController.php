<?php

namespace App\Http\Controllers\Admin;

use App\Exports\ReportVisitorCompanyExport;
use App\Exports\ReportVisitorExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class VisitorReportController extends Controller
{
    public function exportVisitorReport() {
        return Excel::download(new ReportVisitorExport, 'Visitor Report.xlsx');
    }

    public function exportVisitorCompanyReport() {
        return Excel::download(new ReportVisitorCompanyExport, 'Visitor Company Report.xlsx');
    }
}
