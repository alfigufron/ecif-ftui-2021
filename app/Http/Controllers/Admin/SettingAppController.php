<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\SettingApp;
use Illuminate\Http\Request;

use File;

class SettingAppController extends Controller
{
    public function index()
    {
        $data = SettingApp::findOrFail(1);

        return view('admin.websetting.data', ['data' => $data]);
    }

    public function update(Request $req)
    {
        $data = SettingApp::findOrFail(1);

        $update = [
            'url_home' => $req->url,
            'about' => $req->about
        ];

        if ($req->file('schedule')) {
            File::delete('fileSchedule/' . $data->schedule);
    
            $file = $req->file('schedule');
            $name_file = time() . "_" . $file->getClientOriginalName();
            $tujuan_upload = 'fileSchedule';
            $file->move($tujuan_upload, $name_file);

            $update['schedule'] = $name_file;
        }

        SettingApp::findOrFail(1)->update($update);

        return redirect()->route('settingApp.admin')->with('message', 'Setting app data saved!');
    }
}