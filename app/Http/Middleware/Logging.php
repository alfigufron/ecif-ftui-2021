<?php

namespace App\Http\Middleware;

use App\Utils\Logger;
use Closure;
use Illuminate\Support\Facades\Auth;

class Logging
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $url = $request->url();
        $method = $request->method();

        $user_id = Auth::guard('admin')->user()->id;

        Logger::create("$user_id | $method | $url");

        return $next($request);
    }
}
