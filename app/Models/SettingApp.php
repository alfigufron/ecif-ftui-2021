<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SettingApp extends Model
{
    protected $connection = 'mysql';

    protected $table = 'setting_app';
    
    protected $fillable = [
        'url_home',
        'about',
        'schedule',
    ];
}