<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserFaculty extends Model
{
    protected $connection = 'mysql';
    
    protected $fillable = [
        'departement',
        'user_id'
    ];

    /**
     * Relation
     * 
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function user_application()
    {
        return $this->belongsTo(UserApplication::class, 'user_id');
    }
}