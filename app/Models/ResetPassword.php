<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResetPassword extends Model
{
    protected $connection = 'mysql';
    
    protected $fillable = [
        'email',
        'otp'
    ];
}
