<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class AdminCompany extends Authenticatable
{
    protected $connection = 'mysql';

    protected $fillable = [
        'email',
        'name',
        'password',
        'company_id',
    ];

    protected $hidden = [
        'password'
    ];

    /**
     * Relation
     * 
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}