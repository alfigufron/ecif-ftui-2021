<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable {
    protected $connection = 'mysql';

    protected $fillable = [
        'email',
        'name',
        'password',
    ];

    protected $hidden = [
        'password'
    ];
}
