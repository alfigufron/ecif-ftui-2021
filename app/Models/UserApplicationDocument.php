<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserApplicationDocument extends Model {
    protected $connection = 'mysql';
    
    protected $fillable = [
        'document',
        'user_application_id',
    ];

    /**
     * Relation
     * 
     */
    public function user_application() {
        return $this->belongsTo(UserApplication::class);
    }
}
