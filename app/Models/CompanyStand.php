<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyStand extends Model
{
    protected $connection = 'mysql';

    protected $fillable = [
        'video',
        'webinar_poster',
        'recruitment_poster',
        'right_side_addition',
        'left_side_addition',
        'about_us',
        'career',
        'internship',
        'apply',
        'join_webinar',
        'status',
        'twitter',
        'facebook',
        'linkedin',
        'instagram',
        'youtube',
        'company_id',
    ];

    /**
     * Relation
     * 
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}