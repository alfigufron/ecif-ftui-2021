<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserApplication extends Model
{
    protected $connection = 'mysql';
    
    protected $fillable = [
        'company_id',
        'user_id',
        'company_vacancy_id',
    ];

    /**
     * Relation
     * 
     */
    public function user_application_document()
    {
        return $this->hasOne(UserApplicationDocument::class);
    }

    // Foreign
    public function company()
    {
        return $this->belongsTo(Company::class);
    }
    public function company_vacancy()
    {
        return $this->belongsTo(CompanyVacancy::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function user_faculty()
    {
        return $this->hasOne(UserFaculty::class, 'user_id');
    }

    public function user_non_faculty()
    {
        return $this->hasOne(UserNonFaculty::class, 'user_id');
    }
}