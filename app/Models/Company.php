<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $connection = 'mysql';

    protected $fillable = [
        'name',
        'description',
        'logo',
        'type'
    ];

    /**
     * Relation
     * 
     */
    public function company_stand()
    {
        return $this->hasOne(CompanyStand::class);
    }

    public function company_vacancy()
    {
        return $this->hasMany(CompanyVacancy::class);
    }

    public function user_application()
    {
        return $this->hasMany(UserApplication::class);
    }

    public function visitors()
    {
        return $this->hasMany(BoothVisitor::class);
    }

    // Foreign
    public function admin()
    {
        return $this->hasOne(AdminCompany::class);
    }
}