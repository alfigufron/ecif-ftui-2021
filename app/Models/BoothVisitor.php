<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BoothVisitor extends Model
{
    protected $connection = 'mysql';

    protected $fillable = [
        'user_id',
        'company_id',
    ];

    public function company() {
        return $this->belongsTo(Company::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
}
