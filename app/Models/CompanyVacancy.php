<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyVacancy extends Model
{
    protected $connection = 'mysql';

    protected $table = 'company_vacancy';
    protected $fillable = [
        'name',
        'description',
        'company_id',
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}