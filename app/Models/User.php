<?php

namespace App\Models;

use App\Models\UserFaculty;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $connection = 'mysql';

    protected $table = 'users';
    
    protected $fillable = [
        'email',
        'name',
        'password',
        'student_number',
        'gender',
        'birth',
        'address',
        'phone_number',
        'graduate_year',
        'study_level',
        'type',
    ];

    protected $hidden = [
        'password',
    ];

    /**
     * Relation
     * 
     */
    public function user_faculty()
    {
        return $this->hasOne(UserFaculty::class);
    }

    public function user_non_faculty()
    {
        return $this->hasOne(UserNonFaculty::class);
    }

    public function user_application()
    {
        return $this->hasMany(UserApplication::class);
    }

    public function company_visit()
    {
        return $this->hasMany(BoothVisitor::class);
    }
}